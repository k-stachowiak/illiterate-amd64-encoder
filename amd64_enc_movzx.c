/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The MOVZX instruction
// =====================

bool amd64_enc_movzx(struct AMD64EncContext *ec,
                     struct AMD64Location src,
                     struct AMD64Location dst) {

    assert(ec);

    // Ensure mov to a directregister:
    if (dst.type != AMD64_LT_REGISTER_GP) {
        ec->error_message = "MOVZX destination must be a general purpose register";
        return false;
    }

    // Prevent using immediates:
    if (dst.type == AMD64_LT_IMMEDIATE) {
        ec->error_message = "Cannot MOVZX into an immediate";
        return false;
    }
    if (src.type == AMD64_LT_IMMEDIATE) {
        ec->error_message = "Cannot MOVZX from an immediate";
        return false;
    }

    // Prevent use of the "high" registers, when the REX prefix is in order
    if ((rex_prefix_required(&src) ||
         rex_prefix_required(&dst)) &&
        ((src.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(src.gpreg.gpreg)) ||
         (dst.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(dst.gpreg.gpreg)))) {
        ec->error_message = "High register not allowed when REX prefix used";
        return false;
    }

    // At this point the srouce is a r/m location and destination is a direct
    // register. We only have to dispatch the excution based on the operands' size.

    enc_start(ec);
    if (src.size == AMD64_LS_8_BIT) {

        if (dst.size == AMD64_LS_8_BIT) {
            ec->error_message = "MOVZX operand size mismatch";
            return false;
        }

        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &dst, &src);
        ec_put(ec, 0x0F);
        ec_put(ec, 0xB6);
        enc_modrm_2arg(ec, modrm_gpreg(dst.gpreg.gpreg), &src);
        enc_finish(ec);
        return true;

    } else if (src.size == AMD64_LS_16_BIT) {

        if (dst.size != AMD64_LS_32_BIT && dst.size != AMD64_LS_64_BIT) {
            ec->error_message = "MOVZX operand size mismatch";
            return false;
        }

        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &dst, &src);
        ec_put(ec, 0x0F);
        ec_put(ec, 0xB7);
        enc_modrm_2arg(ec, modrm_gpreg(dst.gpreg.gpreg), &src);
        enc_finish(ec);
        return true;

    } else {
        ec->error_message = "Illegal source size for MOVZX";
        return false;
    }
}

// Test code
// =========

static int test_movzx_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_movzx(&ec, AL,  WPTR32_EX(AX, 0x01));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX into a r/m location");

    amd64_enc_movzx(&ec, CX, IMM32(0x67452301));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX into an immediate location");

    amd64_enc_movzx(&ec, IMM16(0x2301), R15);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX from an immediate location");

    amd64_enc_movzx(&ec, BH, R15);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX to mix high register and REX prefix");

    amd64_enc_movzx(&ec, BH, AL);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX between two 8-bit loations");

    amd64_enc_movzx(&ec, CX, SPL);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX 16-bit -> 8bit");

    amd64_enc_movzx(&ec, WPTR32_EX(AX, 0x01), CX);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX 16-bit -> 16bit");

    amd64_enc_movzx(&ec, DWSIB32(2, CX, 12, 0), R15);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOVZX from a 32-bit location");

    return errors;
}

static int test_movzx_8bit(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_movzx(&ec, AL, CX);
    errors += expect_success_4(&ec, &sis, 0x66, 0x0F, 0xB6, 0xC8, "MOVZX AL -> CX");

    amd64_enc_movzx(&ec, BH, EBP);
    errors += expect_success_3(&ec, &sis, 0x0F, 0xB6, 0xEF, "MOVZX BH -> EBP");

    amd64_enc_movzx(&ec, SPL, R15);
    errors += expect_success_4(&ec, &sis, 0x4C, 0x0F, 0xB6, 0xFC, "MOVZX SPL -> R15");

    return errors;
}

static int test_movzx_16bit(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_movzx(&ec, WPTR32_EX(AX, 0x08), EAX);
    errors += expect_success_5(
        &ec, &sis,
        0x67, 0x0F, 0xB7, 0x40, 0x08,
        "MOVZX offset8(EAX) -> EAX");

    amd64_enc_movzx(&ec, WOFFSET32(0x67452301), EDI);
    errors += expect_success_8(
        &ec, &sis,
        0x0F, 0xB7, 0x3C, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOVZX (offset32) -> EDI");

    amd64_enc_movzx(&ec, WSIB64(2, CX, 12, 0), R8);
    errors += expect_success_5(
        &ec, &sis,
        0x4D, 0x0F, 0xB7, 0x04, 0x4C,
        "MOVZX (2*RCX+R12) -> R8");


    return errors;
}

int test_movzx(void) {
    return
        test_movzx_invalid() +
        test_movzx_8bit() +
        test_movzx_16bit();
}

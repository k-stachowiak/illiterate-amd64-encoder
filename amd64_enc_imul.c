/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>
#include <stdarg.h>

#define MAX_IMUL_ARGS 3

// The IMUL instruction
// ====================

static bool enc_imul_rm(struct AMD64EncContext *ec, struct AMD64Location divisor) {

    if (!amd64_loc_is_gp_rm(&divisor)) {
        ec->error_message = "Single argument IMUL requires a general purpose r/m operand";
        return false;
    }

    // IMUL r/m: F6/F7 /5
    enc_start(ec);
    enc_prefixes(ec, AMD64_LS_32_BIT, divisor.size, false, NULL, &divisor);
    enc_sized_opcode(ec, divisor.size, 0xF6, 0xF7);
    enc_modrm_1arg(ec, 5, &divisor);
    enc_finish(ec);
    return true;
}

static bool enc_imul_reg_rm(struct AMD64EncContext *ec,
                            struct AMD64Location reg,
                            struct AMD64Location rm) {
    enum AMD64LocationSize size;

    // Assert arguments' validity:

    if (reg.size != rm.size) {
        ec->error_message = "Size mismatch between two IMUL operands";
        return false;
    }
    size = reg.size;

    if (size == AMD64_LS_8_BIT) {
        ec->error_message = "Two argument IMUL cannot operate on 8-bit operands";
        return false;
    }

    if (reg.type != AMD64_LT_REGISTER_GP) {
        ec->error_message = "First argument to 2-arg IMUL must be a general purpose register";
        return false;
    }

    if (!amd64_loc_is_gp_rm(&rm)) {
        ec->error_message = "Second argument to 2-arg IMUL must be a general purpose r/m";
        return false;
    }

    // Generate the instruction:

    // IMUL reg/mem -> reg: 0x0F 0xAF /r
    enc_start(ec);
    enc_prefixes(ec, AMD64_LS_32_BIT, reg.size, false, &reg, &rm);
    ec_put(ec, 0x0F);
    enc_sized_opcode(ec, size, 0x00, 0xAF);
    enc_modrm_2arg(ec, modrm_gpreg(reg.gpreg.gpreg), &rm);
    enc_finish(ec);
    return true;
}

static bool enc_imul_reg_rm_imm(struct AMD64EncContext *ec,
                                struct AMD64Location reg,
                                struct AMD64Location rm,
                                struct AMD64Location imm) {
    enum AMD64LocationSize size;

    if (reg.size != rm.size) {
        ec->error_message =
            "Reg and r/m operands of 3-arg IMUL must be of the same sizes";
        return false;
    }
    size = reg.size;

    if (reg.type != AMD64_LT_REGISTER_GP) {
        ec->error_message = "First argument to 3-arg IMUL must be a general purpose register";
        return false;
    }

    if (!amd64_loc_is_gp_rm(&rm)) {
        ec->error_message = "Second argument to 3-arg IMUL must be a general purpose r/m";
        return false;
    }

    if (size == AMD64_LS_8_BIT) {
        ec->error_message =
            "Three argument IMUL cannot operate on 8-bit reg and r/m operands";
        return false;
    }

    if (imm.size != AMD64_LS_8_BIT &&
        imm.size != size &&
        (imm.size != AMD64_LS_32_BIT && size != AMD64_LS_64_BIT)) {
        ec->error_message =
            "Non 8-bit immediate operand to IMUL must be of the same size as reg and r/m operands, unless the reg and r/m operands are of 64-bit size, then the immediate should be of 32-bit size";
        return false;
    }

    enc_start(ec);

    if (imm.size == AMD64_LS_8_BIT) {
        enc_prefixes(ec, AMD64_LS_32_BIT, reg.size, false, &reg, &rm);
        enc_sized_opcode(ec, size, 0x00, 0x6B);
        enc_modrm_2arg(ec, modrm_gpreg(reg.gpreg.gpreg), &rm);
        ec_put_immediate_le(ec, AMD64_LS_8_BIT, &imm.imm);
    } else {
        enc_prefixes(ec, AMD64_LS_32_BIT, reg.size, false, &reg, &rm);
        enc_sized_opcode(ec, size, 0x00, 0x69);
        enc_modrm_2arg(ec, modrm_gpreg(reg.gpreg.gpreg), &rm);
        ec_put_immediate_le(ec, imm.size, &imm.imm);
    }

    enc_finish(ec);
    return true;
}

bool amd64_enc_imul(struct AMD64EncContext *ec, int argc, ...) {

    va_list ap;
    struct AMD64Location x, y, z;

    assert(ec);

    if (argc <= 0 || argc > MAX_IMUL_ARGS) {
        ec->error_message = "Incorrect operand count for IMUL";
        return false;
    }

    va_start(ap, argc);

    switch (argc) {
    case 1:
        x = va_arg(ap, struct AMD64Location);
        return enc_imul_rm(ec, x);

    case 2:
        x = va_arg(ap, struct AMD64Location);
        y = va_arg(ap, struct AMD64Location);
        return enc_imul_reg_rm(ec, x, y);

    case 3:
        x = va_arg(ap, struct AMD64Location);
        y = va_arg(ap, struct AMD64Location);
        z = va_arg(ap, struct AMD64Location);
        return enc_imul_reg_rm_imm(ec, x, y, z);
    }

    INTERNAL_ERROR;
}

// Test code
// =========

static int test_imul1_case_2(struct AMD64Location x,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_rm(&ec, x);
    errors += expect_success_2(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               message);
    amd64_enc_imul(&ec, 1, x);
    errors += expect_success_2(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               message);

    return errors;
}

static int test_imul1_case_4(struct AMD64Location x,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_rm(&ec, x);
    errors += expect_success_4(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               message);
    amd64_enc_imul(&ec, 1, x);
    errors += expect_success_4(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               message);

    return errors;
}

static int test_imul1_case_6(struct AMD64Location x,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             uint8_t expected_5,
                             uint8_t expected_6,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_rm(&ec, x);
    errors += expect_success_6(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               message);
    amd64_enc_imul(&ec, 1, x);
    errors += expect_success_6(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               message);

    return errors;
}

static int test_imul1_case_7(struct AMD64Location x,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             uint8_t expected_5,
                             uint8_t expected_6,
                             uint8_t expected_7,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_rm(&ec, x);
    errors += expect_success_7(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               message);
    amd64_enc_imul(&ec, 1, x);
    errors += expect_success_7(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               message);

    return errors;
}

static int test_imul2_case_5(struct AMD64Location x,
                             struct AMD64Location y,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             uint8_t expected_5,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_reg_rm(&ec, x, y);
    errors += expect_success_5(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               message);
    amd64_enc_imul(&ec, 2, x, y);
    errors += expect_success_5(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               message);

    return errors;
}

static int test_imul2_case_9(struct AMD64Location x,
                             struct AMD64Location y,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             uint8_t expected_5,
                             uint8_t expected_6,
                             uint8_t expected_7,
                             uint8_t expected_8,
                             uint8_t expected_9,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_reg_rm(&ec, x, y);
    errors += expect_success_9(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               expected_8,
                               expected_9,
                               message);
    amd64_enc_imul(&ec, 2, x, y);
    errors += expect_success_9(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               expected_8,
                               expected_9,
                               message);

    return errors;
}

static int test_imul3_case_5(struct AMD64Location x,
                             struct AMD64Location y,
                             struct AMD64Location z,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             uint8_t expected_5,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_reg_rm_imm(&ec, x, y, z);
    errors += expect_success_5(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               message);
    amd64_enc_imul(&ec, 3, x, y, z);
    errors += expect_success_5(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               message);

    return errors;
}

static int test_imul3_case_8(struct AMD64Location x,
                             struct AMD64Location y,
                             struct AMD64Location z,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             uint8_t expected_5,
                             uint8_t expected_6,
                             uint8_t expected_7,
                             uint8_t expected_8,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_reg_rm_imm(&ec, x, y, z);
    errors += expect_success_8(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               expected_8,
                               message);
    amd64_enc_imul(&ec, 3, x, y, z);
    errors += expect_success_8(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               expected_8,
                               message);

    return errors;
}

static int test_imul3_case_9(struct AMD64Location x,
                             struct AMD64Location y,
                             struct AMD64Location z,
                             uint8_t expected_1,
                             uint8_t expected_2,
                             uint8_t expected_3,
                             uint8_t expected_4,
                             uint8_t expected_5,
                             uint8_t expected_6,
                             uint8_t expected_7,
                             uint8_t expected_8,
                             uint8_t expected_9,
                             char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_reg_rm_imm(&ec, x, y, z);
    errors += expect_success_9(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               expected_8,
                               expected_9,
                               message);
    amd64_enc_imul(&ec, 3, x, y, z);
    errors += expect_success_9(&ec,
                               &sis,
                               expected_1,
                               expected_2,
                               expected_3,
                               expected_4,
                               expected_5,
                               expected_6,
                               expected_7,
                               expected_8,
                               expected_9,
                               message);

    return errors;
}

static int test_imul3_case_10(struct AMD64Location x,
                              struct AMD64Location y,
                              struct AMD64Location z,
                              uint8_t expected_1,
                              uint8_t expected_2,
                              uint8_t expected_3,
                              uint8_t expected_4,
                              uint8_t expected_5,
                              uint8_t expected_6,
                              uint8_t expected_7,
                              uint8_t expected_8,
                              uint8_t expected_9,
                              uint8_t expected_10,
                              char *message) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_imul_reg_rm_imm(&ec, x, y, z);
    errors += expect_success_10(&ec,
                                &sis,
                                expected_1,
                                expected_2,
                                expected_3,
                                expected_4,
                                expected_5,
                                expected_6,
                                expected_7,
                                expected_8,
                                expected_9,
                                expected_10,
                                message);
    amd64_enc_imul(&ec, 3, x, y, z);
    errors += expect_success_10(&ec,
                                &sis,
                                expected_1,
                                expected_2,
                                expected_3,
                                expected_4,
                                expected_5,
                                expected_6,
                                expected_7,
                                expected_8,
                                expected_9,
                                expected_10,
                                message);

    return errors;
}

static int test_imul_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // Operand counts:

    amd64_enc_imul(&ec, -1);
    errors += expect_failure(&ec, "IMUL Should fail for negative arguments count");

    amd64_enc_imul(&ec, 4);
    errors += expect_failure(&ec, "IMUL Should fail for too many arguments");

    amd64_enc_imul(&ec, 400);
    errors += expect_failure(&ec, "IMUL Should fail for too many arguments");

    // 1-arg operand failures:

    amd64_enc_imul(&ec, 1, IMM8(1));
    errors += expect_failure(&ec, "1-arg IMUL can only operate on r/m arguments");

    // 2-arg operand failures:

    amd64_enc_imul(&ec, 2, DX, R15D);
    errors += expect_failure(
        &ec,
        "2-arg IMUL should only accept args of same sizes");

    amd64_enc_imul(&ec, 2, AH, BL);
    errors += expect_failure(&ec, "2-arg IMUL should not accept 8-bit operands");

    amd64_enc_imul(&ec, 2, DWPTR32(8), R15D);
    errors += expect_failure(&ec, "First argument to 2-arg IMUL must be direct");

    amd64_enc_imul(&ec, 2, R15D, IMM32(1));
    errors += expect_failure(&ec, "Second argument to 2-arg IMUL must be an r/m");

    // 3-arg operand failures:

    amd64_enc_imul(&ec, 3, DX, R15D, IMM32(1));
    errors += expect_failure(
        &ec,
        "3-arg IMUL should only accept reg and r/m args of same sizes");

    amd64_enc_imul(&ec, 3, DWPTR32(8), R15D, IMM32(1));
    errors += expect_failure(&ec, "First argument to 3-arg IMUL must be direct");

    amd64_enc_imul(&ec, 3, R15D, IMM32(1), IMM32(1));
    errors += expect_failure(&ec, "Second argument to 3-arg IMUL must be an r/m");

    amd64_enc_imul(&ec, 3, AH, R9B, IMM8(1));
    errors += expect_failure(
        &ec,
        "3-arg IMUL should not allow 8-bit reg and r/m operands");

    return errors;
}

static int test_imul_1arg(void) {

    int errors = 0;

    errors += test_imul1_case_2(CH, 0xF6, 0xED, "IMUL CH");
    errors += test_imul1_case_2(DL, 0xF6, 0xEA, "IMUL DL");
    errors += test_imul1_case_4(WPTR32(BX), 0x67, 0x66, 0xF7, 0x2B, "IMUL [EBX]");

    errors += test_imul1_case_7(
        DWPTR32_EX(AX, 0x67452301),
        0x67, 0xF7, 0xA8, 0x01, 0x23, 0x45, 0x67,
        "IMUL [EAX]+disp32");

    errors += test_imul1_case_6(
        QWSIB32(1, 9, BX, 0x01),
        0x67, 0x4A, 0xF7, 0x6C, 0x0b, 0x01,
        "IMUL [1*R9D+EBX]+disp8");

    return errors;
}

static int test_imul_2arg(void) {

    int errors = 0;

    errors += test_imul2_case_9(
        AX, WPTR32_EX(DX, 0x67452301),
        0x67, 0x66, 0x0F, 0xAF, 0x82, 0x01, 0x23, 0x45, 0x67,
        "IMUL AX [EDX]+disp32");

    errors += test_imul2_case_5(
        EBX, DWPTR32_EX(SI, 0x01),
        0x67, 0x0F, 0xAF, 0x5E, 0x01,
        "IMUL EBX [ESI]+disp8");

    errors += test_imul2_case_5(
        RCX, QWSIB64(2, 10, 11, 0),
        0x4B, 0x0F, 0xAF, 0x0C, 0x53,
        "IMUL RCX [2*R10+R11]");

    return errors;
}

static int test_imul_3arg(void) {

    int errors = 0;

    errors += test_imul3_case_9(
        DX, WPTR32_EX(AX, 0x67452301), IMM8(0x01),
        0x67, 0x66, 0x6B, 0x90, 0x01, 0x23, 0x45, 0x67, 0x01,
        "IMUL 0x01 0x67452301[EAX] %DX");

    errors += test_imul3_case_5(
        ESI, DWPTR32_EX(BX, 0x01), IMM8(0x01),
        0x67, 0x6B, 0x73, 0x01, 0x01,
        "IMUL 0x01 0x01[EBX] %ESI");

    errors += test_imul3_case_5(
        RDI, QWSIB64(2, 14, 15, 0), IMM8(0x01),
        0x4B, 0x6B, 0x3C, 0x77, 0x01,
        "IMUL 0x01 [2*R15+R14] RDI");

    errors += test_imul3_case_10(
        DX, WPTR32_EX(AX, 0x67452301), IMM16(0x2301),
        0x67, 0x66, 0x69, 0x90, 0x01, 0x23, 0x45, 0x67, 0x01, 0x23,
        "IMUL 0x2301 0x67452301[EAX] DX");

    errors += test_imul3_case_8(
        ESI, DWPTR32_EX(BX, 0x01), IMM32(0x67452301),
        0x67, 0x69, 0x73, 0x01, 0x01, 0x23, 0x45, 0x67,
        "IMUL 0x67452301 0x01[EBX] ESI");

    errors += test_imul3_case_8(
        RDI, QWSIB64(2, 14, 15, 0), IMM32(0x67452301),
        0x4B, 0x69, 0x3C, 0x77, 0x01, 0x23, 0x45, 0x67,
        "IMUL 0x67452301 [2*R15+R14] RDI");

    return errors;
}

int test_imul(void) {
    return
        test_imul_invalid() +
        test_imul_1arg() +
        test_imul_2arg() +
        test_imul_3arg();
}

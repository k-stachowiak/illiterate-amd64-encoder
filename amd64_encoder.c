/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

// AMD64 architectural semantics
// =============================

// Helper make function for the strongly typed displacement
// --------------------------------------------------------

struct AMD64Displacement amd64_displacement_make(int32_t value) {
    struct AMD64Displacement result = {
        .value = value
    };
    return result;
}

// Encoding of the location objects
// --------------------------------

// This section gives a few helper functions that ease in-place creation of
// non-trivial location structures.

bool amd64_loc_try_make_register_gp(enum AMD64LocationSize size,
                                    enum AMD64GPRegister gpreg,
                                    struct AMD64Location *result) {
    assert(result);
    if (amd64_gpreg_is_high(gpreg) && size != AMD64_LS_8_BIT)
        return false;

    result->type = AMD64_LT_REGISTER_GP;
    result->size = size;
    result->gpreg.gpreg = gpreg;
    return true;
}

bool amd64_loc_try_make_memory_sib(enum AMD64LocationSize size,
                                   enum AMD64SIBScale scale,
                                   enum AMD64SIBRegister index,
                                   enum AMD64SIBRegister base,
                                   enum AMD64IndirectionAddressSize ias,
                                   struct AMD64Displacement disp,
                                   struct AMD64Location *result) {

    assert(result);

    // And rSP won't work for index, as it is a special escape value meaning
    // no index register.
    if (index == AMD64_SREG_SP)
        return false;

    result->type = AMD64_LT_MEMORY_SIB;
    result->size = size;
    result->msib.scale = scale;
    result->msib.index = index;
    result->msib.base = base;
    result->msib.addr_size = ias;
    result->msib.disp = disp;
    return true;
}

struct AMD64Location amd64_loc_make_immediate(enum AMD64LocationSize loc_size,
                                              int64_t value) {
    struct AMD64Location result = {
        .type = AMD64_LT_IMMEDIATE,
        .size = loc_size,
        .imm = {
            .value = value
        }
    };
    return result;
}

struct AMD64Location amd64_loc_make_register_gp(enum AMD64LocationSize size,
                                                enum AMD64GPRegister gpreg) {
    struct AMD64Location result;
    if (!amd64_loc_try_make_register_gp(size, gpreg, &result))
        INTERNAL_ERROR;
    return result;
}

struct AMD64Location amd64_loc_make_register_sse(enum AMD64SSERegister ssereg) {
    struct AMD64Location result = {
        .type = AMD64_LT_REGISTER_SSE,
        .size = AMD64_LS_64_BIT,
        .ssereg = {
            .ssereg = ssereg
        }
    };
    return result;
}

struct AMD64Location amd64_loc_make_register_indirect(
        enum AMD64LocationSize size,
        enum AMD64IndirectRegister ireg,
        enum AMD64IndirectionAddressSize ias,
        struct AMD64Displacement disp) {
    struct AMD64Location result = {
        .type = AMD64_LT_REGISTER_INDIRECT,
        .size = size,
        .ireg = {
            .ireg = ireg,
            .addr_size = ias,
            .disp = disp
        }
    };
    return result;
}

struct AMD64Location amd64_loc_make_memory_offset_small(enum AMD64LocationSize size,
                                                        int32_t offset) {
    struct AMD64Location result = {
        .type = AMD64_LT_MEMORY_OFFSET_SMALL,
        .size = size,
        .moffset_small = {
            .offset = offset
        }
    };
    return result;
}

struct AMD64Location amd64_loc_make_memory_offset_big(enum AMD64LocationSize size,
                                                      int64_t offset) {
    struct AMD64Location result = {
        .type = AMD64_LT_MEMORY_OFFSET_BIG,
        .size = size,
        .moffset_big = {
            .offset = offset
        }
    };
    return result;
}

struct AMD64Location amd64_loc_make_memory_rip_offset(enum AMD64LocationSize size,
                                                      int32_t offset) {
    struct AMD64Location result = {
        .type = AMD64_LT_MEMORY_RIP_OFFSET,
        .size = size,
        .mrip = {
            .offset = offset
        }
    };
    return result;
}

struct AMD64Location amd64_loc_make_memory_sib(enum AMD64LocationSize size,
                                               enum AMD64SIBScale scale,
                                               enum AMD64SIBRegister index,
                                               enum AMD64SIBRegister base,
                                               enum AMD64IndirectionAddressSize ias,
                                               struct AMD64Displacement disp) {
    struct AMD64Location result;
    if (!amd64_loc_try_make_memory_sib(
            size, scale, index, base, ias, disp, &result))
        INTERNAL_ERROR;
    return result;
}

struct AMD64Location amd64_loc_make_deferred_memory_rip_offset(
        enum AMD64LocationSize size,
        struct AMD64RipOffsetRecord *loc_rec) {

    struct AMD64Location result = {
        .type = AMD64_LT_DEFERRED_MEMORY_RIP_OFFSET,
        .size = size,
        .def_mrip = {
            .rec = loc_rec
        }
    };
    return result;
}

// Location operations
// -------------------

bool amd64_loc_equal(struct AMD64Location *x, struct AMD64Location *y) {

    if (x->type != y->type || x->size != y->size)
        return false;

    switch (x->type) {
    case AMD64_LT_IMMEDIATE:
        return x->imm.value == y->imm.value;

    case AMD64_LT_REGISTER_GP:
        return x->gpreg.gpreg == y->gpreg.gpreg;

    case AMD64_LT_REGISTER_SSE:
        return x->ssereg.ssereg == y->ssereg.ssereg;

    case AMD64_LT_REGISTER_INDIRECT:
        return
            x->ireg.ireg == y->ireg.ireg &&
            x->ireg.addr_size == y->ireg.addr_size &&
            x->ireg.disp.value == y->ireg.disp.value;

    case AMD64_LT_MEMORY_OFFSET_SMALL:
        return x->moffset_small.offset == y->moffset_small.offset;

    case AMD64_LT_MEMORY_OFFSET_BIG:
        return x->moffset_big.offset == y->moffset_big.offset;

    case AMD64_LT_MEMORY_RIP_OFFSET:
        return x->mrip.offset == y->mrip.offset;

    case AMD64_LT_MEMORY_SIB:
        return
            x->msib.scale == y->msib.scale &&
            x->msib.index == y->msib.index &&
            x->msib.base == y->msib.base &&
            x->msib.addr_size == y->msib.addr_size &&
            x->msib.disp.value == y->msib.disp.value;

    case AMD64_LT_DEFERRED_MEMORY_RIP_OFFSET:
        return
            x->def_mrip.rec == y->def_mrip.rec || (
                x->def_mrip.rec->offset == y->def_mrip.rec->offset &&
                x->def_mrip.rec->size == y->def_mrip.rec->size &&
                x->def_mrip.rec->base == y->def_mrip.rec->base);
    }

    INTERNAL_ERROR;
}

void amd64_loc_swap(struct AMD64Location *x, struct AMD64Location *y) {
    struct AMD64Location temp = *y;
    *y = *x;
    *x = temp;
}

enum AMD64LocationSize amd64_loc_size_from_bytes(uint8_t bytes) {
    switch (bytes) {
    case 1:
        return AMD64_LS_8_BIT;
    case 2:
        return AMD64_LS_16_BIT;
    case 4:
        return AMD64_LS_32_BIT;
    case 8:
        return AMD64_LS_64_BIT;
    }
    INTERNAL_ERROR;
}

bool amd64_try_conv_gpreg_to_ireg(enum AMD64GPRegister gpreg,
                                  enum AMD64IndirectRegister *ireg) {
    switch (gpreg) {
    case AMD64_GPREG_AH:
    case AMD64_GPREG_BH:
    case AMD64_GPREG_CH:
    case AMD64_GPREG_DH:
        return false;

    case AMD64_GPREG_AX:
        *ireg = AMD64_IREG_AX;
        return true;
    case AMD64_GPREG_BX:
        *ireg = AMD64_IREG_BX;
        return true;
    case AMD64_GPREG_CX:
        *ireg = AMD64_IREG_CX;
        return true;
    case AMD64_GPREG_DX:
        *ireg = AMD64_IREG_DX;
        return true;
    case AMD64_GPREG_SP:
        *ireg = AMD64_IREG_SP;
        return true;
    case AMD64_GPREG_BP:
        *ireg = AMD64_IREG_BP;
        return true;
    case AMD64_GPREG_SI:
        *ireg = AMD64_IREG_SI;
        return true;
    case AMD64_GPREG_DI:
        *ireg = AMD64_IREG_DI;
        return true;

    case AMD64_GPREG_8:
        *ireg = AMD64_IREG_8;
        return true;
    case AMD64_GPREG_9:
        *ireg = AMD64_IREG_9;
        return true;
    case AMD64_GPREG_10:
        *ireg = AMD64_IREG_10;
        return true;
    case AMD64_GPREG_11:
        *ireg = AMD64_IREG_11;
        return true;
    case AMD64_GPREG_12:
        *ireg = AMD64_IREG_12;
        return true;
    case AMD64_GPREG_13:
        *ireg = AMD64_IREG_13;
        return true;
    case AMD64_GPREG_14:
        *ireg = AMD64_IREG_14;
        return true;
    case AMD64_GPREG_15:
        *ireg = AMD64_IREG_15;
        return true;
    }

    INTERNAL_ERROR;
}

// Location predicates
// -------------------

bool loc_in_funny_register(struct AMD64Location *loc) {
    return
        (loc->size == AMD64_LS_8_BIT && loc->type == AMD64_LT_REGISTER_GP && (
            loc->gpreg.gpreg == AMD64_GPREG_SP ||
            loc->gpreg.gpreg == AMD64_GPREG_BP ||
            loc->gpreg.gpreg == AMD64_GPREG_SI ||
            loc->gpreg.gpreg == AMD64_GPREG_DI));
}

bool amd64_gpreg_is_high(enum AMD64GPRegister gpreg) {
    return gpreg >= AMD64_GPREG_AH && gpreg <= AMD64_GPREG_DH;
}

bool amd64_gpreg_is_extended(enum AMD64GPRegister gpreg) {
    return gpreg >= AMD64_GPREG_8 && gpreg <= AMD64_GPREG_15;
}

bool amd64_disp_is_byte(struct AMD64Displacement *disp) {
    return
        disp->value <= INT8_MAX &&
        disp->value >= INT8_MIN;
}

bool amd64_ireg_is_extended(enum AMD64IndirectRegister ireg) {
    return ireg >= AMD64_IREG_8 && ireg <= AMD64_IREG_15;
}

bool amd64_sreg_is_extended(enum AMD64SIBRegister sreg) {
    return sreg >= AMD64_SREG_8 && sreg <= AMD64_SREG_15;
}

bool amd64_loc_is_memory(struct AMD64Location *loc) {
    assert(loc);
    return
        loc->type == AMD64_LT_REGISTER_INDIRECT ||
        loc->type == AMD64_LT_MEMORY_OFFSET_SMALL ||
        loc->type == AMD64_LT_MEMORY_RIP_OFFSET ||
        loc->type == AMD64_LT_MEMORY_SIB ||
        loc->type == AMD64_LT_DEFERRED_MEMORY_RIP_OFFSET;
}

bool amd64_loc_is_gp_rm(struct AMD64Location *loc) {
    assert(loc);
    return
        loc->type == AMD64_LT_REGISTER_GP ||
        amd64_loc_is_memory(loc);
}

bool amd64_loc_is_sse_rm(struct AMD64Location *loc) {
    assert(loc);
    return
        loc->type == AMD64_LT_REGISTER_SSE ||
        (loc->size == AMD64_LS_64_BIT && amd64_loc_is_memory(loc));
}

// AMD64 instruction encoder
// =========================

// This module is implemented mostly based on the reference manual:
//
// "AMD64 Architecture Programmer’s Manual
//  Volume 3: General-Purpose and System Instructions"
//
// The manual is refered to as [AMD:<location>], where "<location>" gives the
// precise place in the document.
//
// The general approach to the implementation is, that the module exposes
// abstract functions that represent semantics that could appear in the assembly
// language text, and then translate those commands into the particular byte
// strings.

// Special bytes encoding
// ----------------------
//
// Many instructions require inclusion of specific byte prefixes or suffixes.
// The following functions enable easy and clear construction of such bytes.

static uint8_t rex_prefix(bool w, bool r, bool x, bool b) {

    // REX prefix enables use of the AMD64 register and operand size extensions.
    // It's an 8-bit number constructed by optional setting 4 least significant
    // bits in the value value of 0x40. This results in the following structure:
    //
    // +---------------+---+---+---+---+
    // |      0x4      | W | R | X | B |
    // +---------------+---+---+---+---+
    //
    // The purposes of the particular fields are:
    // - W: set to enable 64-bit operand size,
    // - R: set to reference an extended register in the ModRM.reg field,
    // - X: set to reference an extended register in the SIB.index field,
    // - B: set to reference an extended register in:
    //      - ModRM.r/m field,
    //      - SIB.base field or
    //      - opcode increment register.
    //
    // See [AMD:Section 1.2.7]
    // See [AMD:Section 1.4.4]

    return 0x40 | (w << 3) | (r << 2) | (x << 1) | (b << 0);
}

static uint8_t modrm(char mod, char reg, char rm) {

    // ModRM is a register/memory modification byte, that can optionally be used
    // in some instructions. It consists of (starting from the most significant
    // bits):
    // - 2 bits of the modifier value,
    // - 3 bits identifying a register,
    // - 3 bits identifying a register or a memory location.
    //
    // "mod" selects between register direct (11b) or indirect (<11b)
    // interpretation of the "r/m" field, indirect meaning that the register
    // stores the operand's address rather than the operand itself.
    //
    // "reg" selects a register, except for some instructions, where it is used
    // to expand the opcode value.
    //
    // "r/m" selects a register or a memory location together with "mod".
    //
    // See [AMD:Section 1.4.1]

    return (mod & 3) << 6 | (reg & 7) << 3 | (rm & 7) << 0;
}

static uint8_t sib(char scale, char index, char base) {

    // SIB is an optional suffix used to expand the ways of defining address
    // displacement.
    //
    // See [AMD:Section 1.4.2]

    return (scale & 3) << 6 | (index & 7) << 3 | (base & 7) << 0;
}

static uint8_t sib_scale_code(enum AMD64SIBScale scale_code) {
    switch (scale_code) {
    case AMD64_SS_1:
        return 0;
    case AMD64_SS_2:
        return 1;
    case AMD64_SS_4:
        return 2;
    case AMD64_SS_8:
        return 3;
    }
    INTERNAL_ERROR;
}

uint8_t modrm_gpreg(enum AMD64GPRegister gpreg)
{
    // When identifying a register in the ModRM byte, we must use the following
    // mapping. Note, how multiple registers are assiciated with every potential
    // value. The actual register is derived from additional information
    // provided with the encoded instruction.
    //
    // See [AMD:Table 1-10]
    // See [AMD:Table 2-2]

    switch (gpreg) {
    case AMD64_GPREG_AX:
    case AMD64_GPREG_8:
        return 0;

    case AMD64_GPREG_CX:
    case AMD64_GPREG_9:
        return 1;

    case AMD64_GPREG_DX:
    case AMD64_GPREG_10:
        return 2;

    case AMD64_GPREG_BX:
    case AMD64_GPREG_11:
        return 3;

    case AMD64_GPREG_AH:
    case AMD64_GPREG_SP:
    case AMD64_GPREG_12:
        return 4;

    case AMD64_GPREG_CH:
    case AMD64_GPREG_BP:
    case AMD64_GPREG_13:
        return 5;

    case AMD64_GPREG_DH:
    case AMD64_GPREG_SI:
    case AMD64_GPREG_14:
        return 6;

    case AMD64_GPREG_BH:
    case AMD64_GPREG_DI:
    case AMD64_GPREG_15:
        return 7;
    }
    INTERNAL_ERROR;
}

uint8_t modrm_ssereg(enum AMD64SSERegister ssereg)
{
    // When identifying a register in the ModRM byte, we must use the following
    // mapping. Note, how multiple registers are assiciated with every potential
    // value. The actual register is derived from additional information
    // provided with the encoded instruction.
    //
    // See [AMD:Table 1-10]
    // See [AMD:Table 2-2]

    switch (ssereg) {
    case AMD64_SSEREG_XMM0:
        return 0;

    case AMD64_SSEREG_XMM1:
        return 1;

    case AMD64_SSEREG_XMM2:
        return 2;

    case AMD64_SSEREG_XMM3:
        return 3;

    case AMD64_SSEREG_XMM4:
        return 4;

    case AMD64_SSEREG_XMM5:
        return 5;

    case AMD64_SSEREG_XMM6:
        return 6;

    case AMD64_SSEREG_XMM7:
        return 7;
    }
    INTERNAL_ERROR;
}

uint8_t modrm_ireg(enum AMD64IndirectRegister ireg) {

    // The mapping of the indirect register codes is the same as for the
    // direct ones, but the range usable in this context is narrower, so
    // a separate semantic mapping has been defined here.

    switch (ireg) {
    case AMD64_IREG_AX:
    case AMD64_IREG_8:
        return 0;

    case AMD64_IREG_CX:
    case AMD64_IREG_9:
        return 1;

    case AMD64_IREG_DX:
    case AMD64_IREG_10:
        return 2;

    case AMD64_IREG_BX:
    case AMD64_IREG_11:
        return 3;

    case AMD64_IREG_SP:
    case AMD64_IREG_12:
        return 4;

    case AMD64_IREG_BP:
    case AMD64_IREG_13:
        return 5;

    case AMD64_IREG_SI:
    case AMD64_IREG_14:
        return 6;

    case AMD64_IREG_DI:
    case AMD64_IREG_15:
        return 7;
    }
    INTERNAL_ERROR;
}

uint8_t modrm_sreg(enum AMD64SIBRegister sreg) {

    // The mapping of the SIB register codes is the same as for the direct ones
    // but the range usable in this context is narrower, so a separate semantic
    // mapping has been defined here.

    switch (sreg) {
    case AMD64_SREG_AX:
    case AMD64_SREG_8:
        return 0;

    case AMD64_SREG_CX:
    case AMD64_SREG_9:
        return 1;

    case AMD64_SREG_DX:
    case AMD64_SREG_10:
        return 2;

    case AMD64_SREG_BX:
    case AMD64_SREG_11:
        return 3;

    case AMD64_SREG_SP:
    case AMD64_SREG_12:
        return 4;

    case AMD64_SREG_BP:
    case AMD64_SREG_13:
        return 5;

    case AMD64_SREG_SI:
    case AMD64_SREG_14:
        return 6;

    case AMD64_SREG_DI:
    case AMD64_SREG_15:
        return 7;
    }
    INTERNAL_ERROR;
}

// Construction of the binary strings
// ----------------------------------
//
// In order to conveniently build the byte strings an internal API has been
// provided to be used while encoding the instructions.

static void ec_put_le(struct AMD64EncContext *ec, uint8_t *buffer, uint8_t size) {

    // Append a given buffer to the result buffer in the Little Endian encoding.
    // This function will append the given bytes to the result buffer in the
    // reverse order. It uses the `ec_put()` function, therefore it inherits
    // its correctness guarantees.

    int i;
    for (i = 0; i != size; ++i)
        ec_put(ec, buffer[i]);
}

int ec_offset(struct AMD64EncContext *ctx) {
    assert(ctx);
    return ctx->sink_offset(ctx->sink_ctx);
}

void ec_put(struct AMD64EncContext *ctx, uint8_t x) {
    assert(ctx);
    ctx->sink_put(ctx->sink_ctx, x);
}

void ec_put_rel_offest_le(struct AMD64EncContext *ec,
                          struct AMD64RelOffset *offset) {

    // Store the offset of encoded address:
    ec->last_address_entry.offset = ec_offset(ec);

    if (offset->size == AMD64_ROS_8)
        ec_put(ec, offset->value8);
    else
        ec_put_le(ec, (uint8_t*)&offset->value32, 4);

    // Store the size of encoded address:
    ec->last_address_entry.size = ec_offset(ec) - ec->last_address_entry.offset;
}

void ec_put_32disp_le(struct AMD64EncContext *ec, int32_t disp) {

    // Store the offset of encoded address:
    ec->last_address_entry.offset = ec_offset(ec);

    // Encode displacement of 8 or 32 bits stored in a 32-bit variable..
    ec_put_le(ec, (uint8_t*)&disp, 4);

    // Store the size of encoded address:
    ec->last_address_entry.size = ec_offset(ec) - ec->last_address_entry.offset;
}

void ec_put_disp_le(struct AMD64EncContext *ec,
                    struct AMD64Displacement *disp) {

    if (amd64_disp_is_byte(disp)) {
        // Store the offset of encoded address:
        ec->last_address_entry.offset = ec_offset(ec);

        // Encode displacement of 8 or 32 bits stored in a 32-bit variable..
        ec_put(ec, disp->value);

        // Store the size of encoded address:
        ec->last_address_entry.size = ec_offset(ec) - ec->last_address_entry.offset;

    } else {
        ec_put_32disp_le(ec, disp->value);

    }
}

void ec_put_immediate_le(struct AMD64EncContext *ec,
                         enum AMD64LocationSize loc_size,
                         struct AMD64ImmediateLocation *imm) {

    // Encode immediate value stored in a 64bit variable as 1-, 3-, 4- or 8-byte
    // buffer.

    int bytes = -1;
    switch (loc_size) {
    case AMD64_LS_8_BIT:
        bytes = 1;
        break;
    case AMD64_LS_16_BIT:
        bytes = 2;
        break;
    case AMD64_LS_32_BIT:
        bytes = 4;
        break;
    case AMD64_LS_64_BIT:
        bytes = 8;
        break;
    }
    assert(bytes != -1);
    ec_put_le(ec, (uint8_t*)&imm->value, bytes);
}

void ec_put_offset_big_le(struct AMD64EncContext *ec, int64_t offset) {
    // Store the offset of encoded address:
    ec->last_address_entry.offset = ec_offset(ec);

    // Encode 64-bit offset.
    ec_put_le(ec, (uint8_t*)&offset, 8);

    // Store the size of encoded address:
    ec->last_address_entry.size = ec_offset(ec) - ec->last_address_entry.offset;
}

void ec_put_rex_prefix(struct AMD64EncContext *ec,
                       bool rex_w,
                       bool rex_r,
                       bool rex_x,
                       bool rex_b) {
    ec_put(ec, rex_prefix(rex_w, rex_r, rex_x, rex_b));
}

void ec_maybe_put_rex_prefix(struct AMD64EncContext *ec,
                             bool rex_w,
                             bool rex_r,
                             bool rex_x,
                             bool rex_b) {
    if (rex_w || rex_r || rex_x || rex_b)
        ec_put_rex_prefix(ec, rex_w, rex_r, rex_x, rex_b);
}

// Common instruction encoding algorithms
// --------------------------------------

// There are several commonalities between the way particular instructions are
// encoded. This section gathers such common algorithms, shared between the
// instruction encoding code.

static void enc_modrm(struct AMD64EncContext *ec,
                      char mod,
                      char reg,
                      char rm) {
    // Since it's not trivial to encode the ModRM byte using the current API,
    // a helper function has been defined to do just that.
    ec_put(ec, modrm(mod, reg, rm));
}

static void enc_sib(struct AMD64EncContext *ec,
                    char scale,
                    char index,
                    char base) {
    // A helper to encode a SIB byte into the result buffer.
    ec_put(ec, sib(scale, index, base));
}

static void enc_modrm_reg_gp(struct AMD64EncContext *ec,
                             char reg_code,
                             enum AMD64GPRegister gpreg) {

    // This is a case that can be encoded simply.
    enc_modrm(
        ec,
        3,                 // mod = 11b
        reg_code,          // reg = first operand
        modrm_gpreg(gpreg) // r/m = second operand
        );
}

static void enc_modrm_reg_sse(struct AMD64EncContext *ec,
                              char reg_code,
                              enum AMD64SSERegister ssereg) {

    // This is a case that can be encoded simply.
    enc_modrm(
        ec,
        3,                   // mod = 11b
        reg_code,            // reg = first operand
        modrm_ssereg(ssereg) // r/m = second operand
        );
}

static void enc_modrm_reg_indirect(struct AMD64EncContext *ec,
                                   char reg_code,
                                   struct AMD64IndirectRegisterLocation *loc) {

    if (loc->ireg == AMD64_IREG_SP || loc->ireg == AMD64_IREG_12) {

        // rSP and r12 are not usable for indirection, as their code in the
        // ModRM byte is taken by the SIB byte enabling. However the SIB byte
        // can encode rSP/r12 based indirection, so we'll be using it here.

        char modrm_mod =
            loc->disp.value == 0 ? 0 :
            amd64_disp_is_byte(&loc->disp) ? 1 :
            2;

        enc_modrm(
            ec,
            modrm_mod, // mod = designate displacement
            reg_code,  // reg = first operand
            4          // r/m = 100b: SIB based
            );
        enc_sib(
            ec,
            0, // scale = 1
            4, // index = 100b: null index
            4  // base = 100b: for rSP/r12
            );
        if (loc->disp.value != 0)
            ec_put_disp_le(ec, &loc->disp);

    } else if (loc->disp.value == 0 &&
               loc->ireg != AMD64_IREG_BP &&
               loc->ireg != AMD64_IREG_13) {

        // Certain cases can be encoded with no displacement bytes AND no SIB
        // byte.
        enc_modrm(
            ec,
            0,                    // mod = 00b: indirect no displacement
            reg_code,             // reg = first operand
            modrm_ireg(loc->ireg) // r/m = second operand
            );

    } else {

        // For the remaining cases we use the ModRM byte to enable the
        // displacement, set its size and point at the register holding the base
        // address. Then we just give 1 or 4 bytes of the displacement.

        bool one_byte = amd64_disp_is_byte(&loc->disp);

        enc_modrm(
            ec,
            one_byte ? 1 : 2,     // mod = 01b or 10b: 8- or 32-bit displacement
            reg_code,             // reg = first operand
            modrm_ireg(loc->ireg) // r/m = offset base
            );
        ec_put_disp_le(ec, &loc->disp);
    }
}

static void enc_modrm_disp(struct AMD64EncContext *ec,
                           char reg_code,
                           int32_t disp) {
    // Here we encode a simple case of a base zero 32-bit displacement. It
    // doesn't look useful, but is easy to implement, so why not have it?
    // In the legacy mode, we could get away with using 101b r/m field and just
    // give the 32-bit displacement, but in the 64-bit mode that is used for the
    // RIP-relative addressing. Therefore, in this mode we need to use a SIB
    // byte for that.
    enc_modrm(
        ec,
        0,        // mod = 00b: enable special case for r/m
        reg_code, // reg = first operand
        4         // r/m = 100b: SIB based
        );
    enc_sib(
        ec,
        0, // scale = don't care
        4, // index = 100b: null index
        5  // base = 101b: together with mod = 0, this means 32-bit offset
        );
    ec_put_32disp_le(ec, disp);
}

static void enc_modrm_rip(struct AMD64EncContext *ec,
                          char reg_code,
                          int32_t disp) {
    // Here we encode a simple case of a RIP based 32-bit displacement.
    enc_modrm(
        ec,
        0,        // mod = 00b: enable special case for r/m
        reg_code, // reg = first operand
        5         // r/m = 101b: RIP based
        );
    ec_put_32disp_le(ec, disp);
}

static void enc_modrm_sib(struct AMD64EncContext *ec,
                          char reg_code,
                          struct AMD64SIBMemoryLocation *sib) {

    // This is a baroque addressing scheme, that enables having a challenge
    // while performing one of the most basic actions - giving an address.
    // I suppose this may be useful to implement array indexing, so let's
    // implement it.
    //
    // The SIB addressing defines the address as:
    // scale * index + base + (optional)offset
    //
    // index, and base come from registers

    // Cannot use rSP as a SIB index
    assert(sib->index != AMD64_SREG_SP);

    // ModRM for a SIB displacement. This comes in three variations:
    if (sib->disp.value == 0) {

        // No displacement:
        if (sib->base == AMD64_SREG_BP) {
            // BP cannot be used directly as a displacementless base for SIB,
            // so if the client doesn't want a displacement, we encode the SIB
            // as one with the 8-bit displacement of value 0:
            enc_modrm(
                ec,
                1,        // mod = 10b for 8-bit displacement
                reg_code, // reg = first operand
                4         // r/m = 100b: SIB-based
                );
            enc_sib(ec,
                    sib_scale_code(sib->scale),
                    modrm_sreg(sib->index),
                    modrm_sreg(sib->base));
            ec_put(ec, 0x00);

        } else {
            // For the other registers we can encode a displacementless base
            // in a straightforward way:
            enc_modrm(
                ec,
                0,        // mod = 00b for no displacement
                reg_code, // reg = first operand
                4         // r/m = 100b: SIB-based
                );
            enc_sib(ec,
                    sib_scale_code(sib->scale),
                    modrm_sreg(sib->index),
                    modrm_sreg(sib->base));
        }

    } else if (amd64_disp_is_byte(&sib->disp)) {

        // 8-bit displacement:
        enc_modrm(
            ec,
            1,        // mod = 10b for 8-bit displacement
            reg_code, // reg = first operand
            4         // r/m = 100b: SIB-based
            );
        enc_sib(ec,
                sib_scale_code(sib->scale),
                modrm_sreg(sib->index),
                modrm_sreg(sib->base));
        ec_put_disp_le(ec, &sib->disp);

    } else {

        // 32-bit displacement:
        enc_modrm(
            ec,
            2,        // mod = 01b for 32-bit displacement
            reg_code, // reg = first operand
            4         // r/m = 100b: SIB-based
            );
        enc_sib(ec,
                sib_scale_code(sib->scale),
                modrm_sreg(sib->index),
                modrm_sreg(sib->base));
        ec_put_disp_le(ec, &sib->disp);
    }
}

// Internal API
// ============

bool rex_prefix_required(struct AMD64Location *loc) {

    // We need the REX prefix, when:
    return
        // * we refer to a 64-bit operand,
        loc->size == AMD64_LS_64_BIT ||

        // * we directly or indirectly refer to an expteded register,
        (loc->type == AMD64_LT_REGISTER_GP &&
         amd64_gpreg_is_extended(loc->gpreg.gpreg)) ||
        (loc->type == AMD64_LT_REGISTER_INDIRECT &&
         amd64_ireg_is_extended(loc->ireg.ireg)) ||

        // * we use an extended register as the SIB byte's base or index.
        (loc->type == AMD64_LT_MEMORY_SIB &&
         (amd64_sreg_is_extended(loc->msib.base) ||
          amd64_sreg_is_extended(loc->msib.index)));
}

// Prefix encoding
// ---------------

void enc_legacy_prefixes(struct AMD64EncContext *ec,
                         enum AMD64LocationSize default_operand_size,
                         enum AMD64LocationSize operand_size,
                         struct AMD64Location *rm_location) {

    // There are two prefixes that are supported here, the operand size and
    // the address size prefix.
    // If there is no r/m operand, rm_location may be NULL.
    //
    // See [AMD:Section 1.2.2], [AMD:Section 1.2.3]

    // 1. Override a 64-bit address size with a 32-bit size:
    if (rm_location && (
        (rm_location->type == AMD64_LT_REGISTER_INDIRECT &&
         rm_location->ireg.addr_size == AMD64_IAS_32) ||
        (rm_location->type == AMD64_LT_MEMORY_SIB &&
         rm_location->msib.addr_size == AMD64_IAS_32)))
        ec_put(ec, 0x67);

    // 2. Override a 32- or 64-bit operand size with a 16-bit size:
    if ((default_operand_size == AMD64_LS_32_BIT ||
         default_operand_size == AMD64_LS_64_BIT) &&
        operand_size == AMD64_LS_16_BIT)
        ec_put(ec, 0x66);
}

void enc_rex_prefix(struct AMD64EncContext *ec,
                    enum AMD64LocationSize default_operand_size,
                    enum AMD64LocationSize operand_size,
                    bool reg_opcode,
                    struct AMD64Location *reg_loc,
                    struct AMD64Location *rm_loc) {

    // This function encodes REX prefix for an instruction with reg and r/m
    // operands.
    // If there is no register operand, `reg_loc` may be NULL.
    // If there is no r/m operand, rm_loc may be NULL.
    //
    // See [AMD:Section 1.4.4], [AMD:Section 1.2.3]

    bool rex_w = false;
    bool rex_r = false;
    bool rex_x = false;
    bool rex_b = false;

    assert(!reg_loc ||
           reg_loc->type == AMD64_LT_REGISTER_GP ||
           reg_loc->type == AMD64_LT_REGISTER_SSE);
    assert(!rm_loc || amd64_loc_is_gp_rm(rm_loc) || amd64_loc_is_sse_rm(rm_loc));

    // REX.w overrides operand size from 32- to 64-bits:
    if (default_operand_size == AMD64_LS_32_BIT &&
        operand_size == AMD64_LS_64_BIT)
        rex_w = true;

    // REX.r extends the ModRM.reg field:
    if (reg_loc &&
        !reg_opcode &&
        reg_loc->type == AMD64_LT_REGISTER_GP &&
        amd64_gpreg_is_extended(reg_loc->gpreg.gpreg))
        rex_r = true;

    // REX.x extends the SIB.index field:
    if (rm_loc && rm_loc->type == AMD64_LT_MEMORY_SIB &&
        amd64_sreg_is_extended(rm_loc->msib.index))
        rex_x = true;

    // REX.b extends the ModRM.r/m of SIB.base field:
    if (rm_loc && (
        (rm_loc->type == AMD64_LT_REGISTER_GP &&
         amd64_gpreg_is_extended(rm_loc->gpreg.gpreg)) ||
        (rm_loc->type == AMD64_LT_REGISTER_INDIRECT &&
         amd64_ireg_is_extended(rm_loc->ireg.ireg)) ||
        (rm_loc->type == AMD64_LT_MEMORY_SIB &&
         amd64_sreg_is_extended(rm_loc->msib.base))))
        rex_b = true;

    // REX.b also extends the reg location if it is the part of the opcode:
    if (reg_loc &&
        reg_opcode &&
        reg_loc->type == AMD64_LT_REGISTER_GP &&
        amd64_gpreg_is_extended(reg_loc->gpreg.gpreg))
        rex_b = true;

    // If no bit is to be set in the REX prefix, then in general we could avoid
    // it. However, there's a catch! Encoding of SI, DI, BP and SP is a bit
    // silly. We can only refer to them when the REX prefix is present.
    // Otherwise, their respective encoding will be interpreted as AH, BH, CH
    // and DH, accordingly. Therefore, even if we don't really need any of the
    // REX prefix bits set, but want to address SI, DI BP or SP (refered to as
    // "funny" registers), we still need the prefix, even if empty. Hence...
    if ((rm_loc && loc_in_funny_register(rm_loc)) ||
        (reg_loc && loc_in_funny_register(reg_loc)))
        ec_put_rex_prefix(ec, rex_w, rex_r, rex_x, rex_b);
    else
        ec_maybe_put_rex_prefix(ec, rex_w, rex_r, rex_x, rex_b);
}

void enc_prefixes(struct AMD64EncContext *ec,
                  enum AMD64LocationSize default_operand_size,
                  enum AMD64LocationSize operand_size,
                  bool reg_opcode,
                  struct AMD64Location *reg_loc,
                  struct AMD64Location *rm_loc) {
    enc_legacy_prefixes(ec, default_operand_size, operand_size, rm_loc);
    enc_rex_prefix(ec, default_operand_size, operand_size, reg_opcode, reg_loc, rm_loc);
}

// Opcode encoding
// ---------------

void enc_sized_opcode(struct AMD64EncContext *ec,
                      enum AMD64LocationSize size,
                      uint8_t opcode_8bit,
                      uint8_t opcode_default) {
    switch (size) {
    case AMD64_LS_8_BIT:
        ec_put(ec, opcode_8bit);
        break;
    case AMD64_LS_16_BIT:
    case AMD64_LS_32_BIT:
    case AMD64_LS_64_BIT:
        ec_put(ec, opcode_default);
        break;
    }
}

void enc_incr_sized_opcode(struct AMD64EncContext *ec,
                           enum AMD64LocationSize size,
                           uint8_t opcode_8bit,
                           uint8_t opcode_default,
                           enum AMD64GPRegister incr_gpreg) {
    switch (size) {
    case AMD64_LS_8_BIT:
        ec_put(ec, opcode_8bit + modrm_gpreg(incr_gpreg));
        break;
    case AMD64_LS_16_BIT:
    case AMD64_LS_32_BIT:
    case AMD64_LS_64_BIT:
        ec_put(ec, opcode_default + modrm_gpreg(incr_gpreg));
        break;
    }
}

// ModRM and SIB bytes encoding
// ----------------------------

void enc_modrm_1arg(struct AMD64EncContext *ec,
                    uint8_t reg_code,
                    struct AMD64Location *rm_loc) {

    // First let's make sure, nobody tries to get a r/m encoding for an immediate.
    assert(rm_loc->type != AMD64_LT_IMMEDIATE);
    assert(rm_loc->type != AMD64_LT_MEMORY_OFFSET_BIG);

    // Now check the remaining possibilities
    if (rm_loc->type == AMD64_LT_REGISTER_GP) {
        enc_modrm_reg_gp(ec, reg_code, rm_loc->gpreg.gpreg);

    } else if(rm_loc->type == AMD64_LT_REGISTER_SSE) {
        enc_modrm_reg_sse(ec, reg_code, rm_loc->ssereg.ssereg);

    } else if (rm_loc->type == AMD64_LT_REGISTER_INDIRECT) {
        enc_modrm_reg_indirect(ec,reg_code, &rm_loc->ireg);

    } else if (rm_loc->type == AMD64_LT_MEMORY_OFFSET_SMALL) {
        enc_modrm_disp(ec, reg_code, rm_loc->moffset_small.offset);

    } else if (rm_loc->type == AMD64_LT_MEMORY_RIP_OFFSET) {
        enc_modrm_rip(ec, reg_code, rm_loc->mrip.offset);

    } else if (rm_loc->type == AMD64_LT_DEFERRED_MEMORY_RIP_OFFSET) {
        enc_modrm_rip(ec, reg_code, 0xdeadbeef);
        rm_loc->def_mrip.rec->offset = ec->last_address_entry.offset;
        rm_loc->def_mrip.rec->size = ec->last_address_entry.size;
        ec->base_set_request = &rm_loc->def_mrip.rec->base;

    } else {
        enc_modrm_sib(ec, reg_code, &rm_loc->msib);

    }
}

void enc_modrm_2arg(struct AMD64EncContext *ec,
                    uint8_t reg_code,
                    struct AMD64Location *rm_loc) {
    enc_modrm_1arg(ec, reg_code, rm_loc);
}

// Public API
// ==========

// Single instruction sink related
// -------------------------------

void amd64_sis_init(struct AMD64SingleInstrSink *sis) {
    memset(sis->buffer, 0xFF, AMD64_MAX_INSTRUCTION_SIZE);
    sis->bufsize = 0;
}

void amd64_sis_reset(struct AMD64SingleInstrSink *sis) {
    amd64_sis_init(sis);
}

static int sis_offset(void *sisv) {
    struct AMD64SingleInstrSink *sis = (struct AMD64SingleInstrSink *)sisv;
    return sis->bufsize;
}

static void sis_put(void *sisv, uint8_t x) {
    struct AMD64SingleInstrSink *sis = (struct AMD64SingleInstrSink *)sisv;
    assert(sis->bufsize < AMD64_MAX_INSTRUCTION_SIZE);
    sis->buffer[sis->bufsize++] = x;
}

void amd64_ec_init_generic(struct AMD64EncContext *ec,
                           void *ctx,
                           int (*offset_func)(void *),
                           void (*put_func)(void *, uint8_t)) {
    assert(ec);
    assert(ctx);
    ec->error_message = NULL;
    ec->sink_ctx = ctx;
    ec->sink_offset = offset_func;
    ec->sink_put = put_func;
}

void amd64_ec_init_single_instr(struct AMD64EncContext *ec,
                                struct AMD64SingleInstrSink *sis) {
    assert(ec);
    assert(sis);
    ec->error_message = NULL;
    ec->sink_ctx = sis;
    ec->sink_offset = sis_offset;
    ec->sink_put = sis_put;
}

// Instruction encoding
// --------------------

bool amd64_enc_ret(struct AMD64EncContext *ec) {
    enc_start(ec);
    ec_put(ec, 0xC3);
    enc_finish(ec);
    return true;
}

bool amd64_enc_ret_pop(struct AMD64EncContext *ec, uint16_t pop) {
    enc_start(ec);
    ec_put(ec, 0xC2);
    ec_put_le(ec, (uint8_t*)&pop, 2);
    enc_finish(ec);
    return true;
}

bool amd64_enc_syscall(struct AMD64EncContext *ec) {
    enc_start(ec);
    ec_put(ec, 0x0F);
    ec_put(ec, 0x05);
    enc_finish(ec);
    return true;
}

void enc_start(struct AMD64EncContext *ec) {
    // Reset request for setting the base address of a potential defered
    // location.
    ec->base_set_request = NULL;
}

void enc_finish(struct AMD64EncContext *ec) {
    // If a location request was set, it points to the variable where the base
    // offset is expected. Let's set it then.
    if (ec->base_set_request)
        *ec->base_set_request = ec_offset(ec);
}

// Tests
// =====

// Common test code
// ----------------

void print_hex_buffer(uint8_t *buffer, int length) {
    while (length--)
        printf("%02X ", *buffer++);
}

int expect_failure(struct AMD64EncContext *ec, char *fail_msg) {
    if (ec->error_message) {
        return 0;
    } else {
        printf("Unexpected success:\n");
        printf("\t%s\n", fail_msg);
        return 1;
    }
}

int expect_success(struct AMD64EncContext *ec,
                   struct AMD64SingleInstrSink *sis,
                   uint8_t *expected,
                   int expected_len,
                   char *fail_msg) {
    if (ec->error_message) {
        printf("Unexpected failure:\n");
        printf("\t%s\n", fail_msg);
        amd64_sis_reset(sis);
        return 1;
    }
    if (expected_len != sis->bufsize) {

        printf("Unexpected result length:\n");

        printf("\tExpected: ");
        print_hex_buffer(expected, expected_len);
        printf("\n");

        printf("\tActual: ");
        print_hex_buffer(sis->buffer, sis->bufsize);
        printf("\n");

        printf("\t%s\n", fail_msg);

        amd64_sis_reset(sis);
        return 1;
    }

    if (memcmp(expected, sis->buffer, expected_len) != 0) {
        printf("Unexpected result contents:\n");

        printf("\tExpected: ");
        print_hex_buffer(expected, expected_len);
        printf("\n");

        printf("\tActual: ");
        print_hex_buffer(sis->buffer, sis->bufsize);
        printf("\n");

        printf("\t%s\n", fail_msg);

        amd64_sis_reset(sis);
        return 1;
    }

    amd64_sis_reset(sis);
    return 0;
}

int expect_success_2(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2
    };
    return expect_success(ec, sis, expected, 2, fail_msg);
}

int expect_success_3(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3
    };
    return expect_success(ec, sis, expected, 3, fail_msg);
}

int expect_success_4(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4
    };
    return expect_success(ec, sis, expected, 4, fail_msg);
}

int expect_success_5(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4,
        expected_5
    };
    return expect_success(ec, sis, expected, 5, fail_msg);
}

int expect_success_6(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4,
        expected_5,
        expected_6
    };
    return expect_success(ec, sis, expected, 6, fail_msg);
}

int expect_success_7(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     uint8_t expected_7,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4,
        expected_5,
        expected_6,
        expected_7
    };
    return expect_success(ec, sis, expected, 7, fail_msg);
}

int expect_success_8(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     uint8_t expected_7,
                     uint8_t expected_8,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4,
        expected_5,
        expected_6,
        expected_7,
        expected_8
    };
    return expect_success(ec, sis, expected, 8, fail_msg);
}

int expect_success_9(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     uint8_t expected_7,
                     uint8_t expected_8,
                     uint8_t expected_9,
                     char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4,
        expected_5,
        expected_6,
        expected_7,
        expected_8,
        expected_9
    };
    return expect_success(ec, sis, expected, 9, fail_msg);
}

int expect_success_10(struct AMD64EncContext *ec,
                      struct AMD64SingleInstrSink *sis,
                      uint8_t expected_1,
                      uint8_t expected_2,
                      uint8_t expected_3,
                      uint8_t expected_4,
                      uint8_t expected_5,
                      uint8_t expected_6,
                      uint8_t expected_7,
                      uint8_t expected_8,
                      uint8_t expected_9,
                      uint8_t expected_10,
                      char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4,
        expected_5,
        expected_6,
        expected_7,
        expected_8,
        expected_9,
        expected_10
    };
    return expect_success(ec, sis, expected, 10, fail_msg);
}

int expect_success_11(struct AMD64EncContext *ec,
                      struct AMD64SingleInstrSink *sis,
                      uint8_t expected_1,
                      uint8_t expected_2,
                      uint8_t expected_3,
                      uint8_t expected_4,
                      uint8_t expected_5,
                      uint8_t expected_6,
                      uint8_t expected_7,
                      uint8_t expected_8,
                      uint8_t expected_9,
                      uint8_t expected_10,
                      uint8_t expected_11,
                      char *fail_msg) {
    uint8_t expected[] = {
        expected_1,
        expected_2,
        expected_3,
        expected_4,
        expected_5,
        expected_6,
        expected_7,
        expected_8,
        expected_9,
        expected_10,
        expected_11
    };
    return expect_success(ec, sis, expected, 11, fail_msg);
}

int expect_last_offset(struct AMD64EncContext *ec,
                       struct AMD64EncContext *ctx,
                       struct AMD64SingleInstrSink *sis,
                       int expected_offset,
                       int expected_size) {
    if (!ec) {
        printf("Unexpected failure while checking last address\n");
        return 1;
    }

    if (ctx->last_address_entry.offset != expected_offset) {
        printf("Unexpected last addresses offset. Expected %d, actual: %d\n",
               expected_offset,
               ctx->last_address_entry.offset);
        amd64_sis_reset(sis);
        return 1;
    }

    if (ctx->last_address_entry.size != expected_size) {
        printf("Unexpected last addresses size. Expected %d, actual: %d\n",
               expected_size,
               ctx->last_address_entry.size);
        amd64_sis_reset(sis);
        return 1;
    }

    amd64_sis_reset(sis);
    return 0;
}

// Common tests
// ------------

static int test_loc_validation(void) {

    struct AMD64Location dummy_loc;
    struct AMD64Displacement dummy_displacement = { .value = 1 };

    int errors = 0;

    // Try invalid direct register location:
    if (amd64_loc_try_make_register_gp(AMD64_LS_16_BIT,
                                       AMD64_GPREG_AH,
                                       &dummy_loc) ||
        amd64_loc_try_make_register_gp(AMD64_LS_32_BIT,
                                       AMD64_GPREG_BH,
                                       &dummy_loc) ||
        amd64_loc_try_make_register_gp(AMD64_LS_64_BIT,
                                       AMD64_GPREG_CH,
                                       &dummy_loc) ||
        amd64_loc_try_make_register_gp(AMD64_LS_16_BIT,
                                       AMD64_GPREG_DH,
                                       &dummy_loc)) {
        printf("Failed rejecting a \"high\" register location with non-8-bit size");
        ++errors;
    }

    // Try invalid SIB index and base locations.
    if (amd64_loc_try_make_memory_sib(AMD64_LS_8_BIT,
                                      AMD64_SS_1,         // valid
                                      AMD64_SREG_SP,      // invalid
                                      AMD64_SREG_AX,      // valid
                                      AMD64_IAS_32,       // valid
                                      dummy_displacement, // valid
                                      &dummy_loc)) {
        printf("Failed to reject rSP as an index for a SIB byte");
        ++errors;
    }

    return errors;
}

static int test_address_offset_encoding(void) {

    struct AMD64RipOffsetRecord ror = {
        .offset = -1,
        .size = -1,
        .base = -1
    };

    struct AMD64Displacement disp8 = { .value = 0x01 };
    struct AMD64Displacement disp32 = { .value = 0x67452301 };

    struct AMD64RelOffset offset8 = { .size = AMD64_ROS_8, .value8 = 0x01 };
    struct AMD64RelOffset offset32 = { .size = AMD64_ROS_32, .value32 = 0x67452301 };

    struct AMD64Location imm32 = amd64_loc_make_immediate(AMD64_LS_32_BIT, 0x67452301);

    struct AMD64Location ebx = amd64_loc_make_register_gp(
        AMD64_LS_32_BIT, AMD64_GPREG_BX);

    struct AMD64Location esi32_8 = amd64_loc_make_register_indirect(
        AMD64_LS_32_BIT, AMD64_IREG_SI, AMD64_IAS_32, disp8);

    struct AMD64Location eax32_32 = amd64_loc_make_register_indirect(
        AMD64_LS_32_BIT, AMD64_IREG_AX, AMD64_IAS_32, disp32);

    struct AMD64Location rip_32_32 = amd64_loc_make_memory_rip_offset(
        AMD64_LS_32_BIT, offset32.value32);

    struct AMD64Location drip_32_32 = amd64_loc_make_deferred_memory_rip_offset(
        AMD64_LS_32_BIT, &ror);

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // Relative offset encoding:

    amd64_enc_jmp_rel(&ec, offset8);
    errors += expect_last_offset(&ec, &ec, &sis, 1, 1);

    amd64_enc_jmp_rel(&ec, offset32);
    errors += expect_last_offset(&ec, &ec, &sis, 1, 4);

    // RIP relative offset:

    // Regular...:
    amd64_enc_xor(&ec, imm32, rip_32_32);
    errors += expect_last_offset(&ec, &ec, &sis, 2, 4);

    // ...and deferred:
    amd64_enc_xor(&ec, imm32, drip_32_32);
    if (ror.offset == -1 ||
        ror.size == -1 ||
        ror.base == -1) {
        printf("Deferred location attributes not set\n");
        ++errors;
    }
    if (ror.offset != 2) {
        printf("Deferred address offset: %d, expected: %d\n", ror.offset, 2);
        ++errors;
    }
    if (ror.size != 4) {
        printf("Deferred address size: %d, expected: %d\n", ror.size, 4);
        ++errors;
    }
    if (ror.base != 10) {
        printf("Deferred address base: %d, expected: %d\n", ror.base, 10);
        ++errors;
    }
    amd64_sis_reset(&sis);

    // Indirect register address:

    amd64_enc_imul(&ec, 2, ebx, esi32_8);
    errors += expect_last_offset(&ec, &ec, &sis, 4, 1);

    amd64_enc_imul(&ec, 1, eax32_32);
    errors += expect_last_offset(&ec, &ec, &sis, 3, 4);

    return errors;
}

static int test_mod_rm_encoding(void) {

    struct AMD64Displacement disp0 = { .value = 0 };
    struct AMD64Displacement disp8 = { .value = 0x01 };
    struct AMD64Displacement disp32 = { .value = 0x67452301 };

    struct AMD64Location AH = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_AH);
    struct AMD64Location AL = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_AX);

    struct AMD64Location BH = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_BH);
    struct AMD64Location BL = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_BX);

    struct AMD64Location CH = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_CH);
    struct AMD64Location CL = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_CX);

    struct AMD64Location DH = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_DH);
    struct AMD64Location DL = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_DX);

    struct AMD64Location SP = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_SP);
    struct AMD64Location BP = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_BP);
    struct AMD64Location SI = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_SI);
    struct AMD64Location DI = amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_DI);

    struct AMD64Location EAX_0 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_AX, AMD64_IAS_32, disp0);
    struct AMD64Location EAX_8 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_AX, AMD64_IAS_32, disp8);
    struct AMD64Location EAX_32 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_AX, AMD64_IAS_32, disp32);

    struct AMD64Location ESP_0 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_SP, AMD64_IAS_32, disp0);
    struct AMD64Location ESP_8 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_SP, AMD64_IAS_32, disp8);
    struct AMD64Location ESP_32 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_SP, AMD64_IAS_32, disp32);

    struct AMD64Location EBP_0 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_BP, AMD64_IAS_32, disp0);
    struct AMD64Location EBP_8 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_BP, AMD64_IAS_32, disp8);
    struct AMD64Location EBP_32 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_BP, AMD64_IAS_32, disp32);

    struct AMD64Location R12D_0 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_12, AMD64_IAS_32, disp0);
    struct AMD64Location R12D_8 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_12, AMD64_IAS_32, disp8);
    struct AMD64Location R12D_32 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_12, AMD64_IAS_32, disp32);

    struct AMD64Location R13D_0 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_13, AMD64_IAS_32, disp0);
    struct AMD64Location R13D_8 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_13, AMD64_IAS_32, disp8);
    struct AMD64Location R13D_32 = amd64_loc_make_register_indirect(
        AMD64_LS_8_BIT, AMD64_IREG_13, AMD64_IAS_32, disp32);

    struct AMD64Location offset32 = amd64_loc_make_memory_offset_small(
        AMD64_LS_8_BIT, 0x67452301);

    struct AMD64Location rip32 = amd64_loc_make_memory_rip_offset(
        AMD64_LS_8_BIT, 0x67452301);

    struct AMD64Location sib_1_a_sp_0 = amd64_loc_make_memory_sib(
        AMD64_LS_8_BIT, AMD64_SS_1, AMD64_SREG_AX, AMD64_SREG_SP, AMD64_IAS_64, disp0);
    struct AMD64Location sib_2_a_sp_8 = amd64_loc_make_memory_sib(
        AMD64_LS_8_BIT, AMD64_SS_2, AMD64_SREG_AX, AMD64_SREG_SP, AMD64_IAS_64, disp8);
    struct AMD64Location sib_4_a_sp_32 = amd64_loc_make_memory_sib(
        AMD64_LS_8_BIT, AMD64_SS_4, AMD64_SREG_AX, AMD64_SREG_SP, AMD64_IAS_64, disp32);

    struct AMD64Location sib_8_a_bp_0 = amd64_loc_make_memory_sib(
        AMD64_LS_8_BIT, AMD64_SS_8, AMD64_SREG_AX, AMD64_SREG_BP, AMD64_IAS_64, disp0);
    struct AMD64Location sib_1_a_bp_8 = amd64_loc_make_memory_sib(
        AMD64_LS_8_BIT, AMD64_SS_1, AMD64_SREG_AX, AMD64_SREG_BP, AMD64_IAS_64, disp8);
    struct AMD64Location sib_2_a_bp_32 = amd64_loc_make_memory_sib(
        AMD64_LS_8_BIT, AMD64_SS_2, AMD64_SREG_AX, AMD64_SREG_BP, AMD64_IAS_64, disp32);

    struct {
        char *name;
        struct AMD64Location loc;
        uint8_t expected_output[6];
        int expected_size;
    } cases[] = {

        // Direct registers
        // ----------------

        // ### Direct register cases:
        { "AH", AH, { 0xC4 }, 1 },
        { "AL", AL, { 0xC0 }, 1 },
        { "BH", BH, { 0xC7 }, 1 },
        { "BL", BL, { 0xC3 }, 1 },
        { "CH", CH, { 0xC5 }, 1 },
        { "CL", CL, { 0xC1 }, 1 },
        { "DH", DH, { 0xC6 }, 1 },
        { "DL", DL, { 0xC2 }, 1 },

        { "SPL", SP, { 0xC4 }, 1 },
        { "BPL", BP, { 0xC5 }, 1 },
        { "SIL", SI, { 0xC6 }, 1 },
        { "DIL", DI, { 0xC7 }, 1 },

        // ### Indirect register cases:
        { "(EAX)", EAX_0, { 0x00 }, 1 },
        { "disp8(EAX)", EAX_8, { 0x40, 0x01 }, 2 },
        { "disp32(EAX)", EAX_32, { 0x80, 0x01, 0x23, 0x45, 0x67 }, 5 },

        { "(ESP)", ESP_0, { 0x04, 0x24 }, 2 },
        { "disp8(ESP)", ESP_8, { 0x44, 0x24, 0x01 }, 3 },
        { "disp32(ESP)", ESP_32, { 0x84, 0x24, 0x01, 0x23, 0x45, 0x67 }, 6 },

        { "(EBP)", EBP_0, { 0x45, 0x00 }, 2 },
        { "disp8(EBP)", EBP_8, { 0x45, 0x01 }, 2 },
        { "disp32(EBP)", EBP_32, { 0x85, 0x01, 0x23, 0x45, 0x67 }, 5 },

        { "(R12D)", R12D_0, { 0x04, 0x24 }, 2 },
        { "disp8(R12D)", R12D_8, { 0x44, 0x24, 0x01 }, 3 },
        { "disp32(R12D)", R12D_32, { 0x84, 0x24, 0x01, 0x23, 0x45, 0x67 }, 6 },

        { "(R13D)", R13D_0, { 0x45, 0x00 }, 2 },
        { "disp8(R13D)", R13D_8, { 0x45, 0x01 }, 2 },
        { "disp32(R13D)", R13D_32, { 0x85, 0x01, 0x23, 0x45, 0x67 }, 5 },

        // ### Plain memory displacement:
        { "(disp32)", offset32, { 0x04, 0x25, 0x01, 0x23, 0x45, 0x67 }, 6 },

        // ### RIP relative memory displacement:
        { "disp32(RIP)", rip32, { 0x05, 0x01, 0x23, 0x45, 0x67 }, 5 },

        // ### SIB based addressing:
        { "(1*RAX+RSP)", sib_1_a_sp_0, { 0x04, 0x04 }, 2 },
        { "disp8(2*RAX+RSP)", sib_2_a_sp_8, { 0x44, 0x44, 0x01 }, 3 },
        { "disp32(4*RAX+RSP)", sib_4_a_sp_32, { 0x84, 0x84, 0x01, 0x23, 0x45, 0x67 }, 6 },

        { "(8*RAX+RBP)", sib_8_a_bp_0, { 0x44, 0xC5, 0x00 }, 3 },
        { "disp8(1*RAX+RBP)", sib_1_a_bp_8, { 0x44, 0x05, 0x01 }, 3 },
        { "disp32(2*RAX+RBP)", sib_2_a_bp_32, { 0x84, 0x45, 0x01, 0x23, 0x45, 0x67 }, 6 },

        // ### End of cases:
        { NULL, {}, {}, 0 }

    }, *current_case;

    int errors = 0;
    for (current_case = cases; current_case->name; ++current_case) {

        struct AMD64SingleInstrSink sis;
        struct AMD64EncContext ec;

        amd64_sis_init(&sis);
        amd64_ec_init_single_instr(&ec, &sis);

        enc_modrm_1arg(&ec, 0, &current_case->loc);

        if (ec.error_message) {
            printf("Failed ModRM encoding: %s\n", current_case->name);
            ++errors;
            continue;
        }

        if (sis.bufsize != current_case->expected_size) {
            printf("Incorrect size in ModRM encoding: %s\n", current_case->name);
            printf("\tExpected: ");
            print_hex_buffer(current_case->expected_output,
                             current_case->expected_size);
            printf("\n");

            printf("\tActual: ");
            print_hex_buffer(sis.buffer, sis.bufsize);
            printf("\n");
            ++errors;
            continue;
        }

        if (memcmp(current_case->expected_output, sis.buffer, sis.bufsize) != 0) {
            printf("Incorrect bytes in ModRM encoding: %s\n", current_case->name);
            printf("\tExpected: ");
            print_hex_buffer(current_case->expected_output,
                             current_case->expected_size);
            printf("\n");

            printf("\tActual: ");
            print_hex_buffer(sis.buffer, sis.bufsize);
            printf("\n");
            ++errors;
            continue;
        }
    }

    return errors;
}

// Public API
// ----------

int amd64_enc_test(void) {
    return
        test_loc_validation() +
        test_address_offset_encoding() +
        test_mod_rm_encoding() +
        test_add_like() +
        test_addsd_like() +
        test_comisd() +
        test_idiv() +
        test_imul() +
        test_inc() +
        test_jump() +
        test_lea() +
        test_mov() +
        test_movd() +
        test_movzx() +
        test_neg() +
        test_push() +
        test_pop();
}

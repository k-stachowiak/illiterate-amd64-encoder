/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The INC instruction
// ===================

bool amd64_enc_inc(struct AMD64EncContext *ec, struct AMD64Location x) {

    assert(ec);

    if (!amd64_loc_is_gp_rm(&x)) {
        ec->error_message = "Can only perform INC on a general purpose r/m location";
        return false;
    }

    enc_start(ec);
    enc_prefixes(ec, AMD64_LS_32_BIT, x.size, false, NULL, &x);
    enc_sized_opcode(ec, x.size, 0xFE, 0xFF);
    enc_modrm_1arg(ec, 0, &x);
    enc_finish(ec);
    return true;
}

// Test code
// =========

int test_inc(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_inc(&ec, DH);
    errors += expect_success_2(&ec, &sis, 0xFE, 0xC6, "INC DH");

    amd64_enc_inc(&ec, AL);
    errors += expect_success_2(&ec, &sis, 0xFE, 0xC0, "INC AL");

    amd64_enc_inc(&ec, BPL);
    errors += expect_success_3(&ec, &sis, 0x40, 0xFE, 0xC5, "INC BPL");

    amd64_enc_inc(&ec, WPTR32_EX(BX, 0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x67, 0x66, 0xFF, 0x83, 0x01, 0x23, 0x45, 0x67,
        "INC disp32[EBX]");

    amd64_enc_inc(&ec, DWPTR64_EX(SI, 0x01));
    errors += expect_success_3(&ec, &sis, 0xFF, 0x46, 0x01, "INC disp8[RSI]");

    amd64_enc_inc(&ec, QWOFFSET32(0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x48, 0xFF, 0x04, 0x25, 0x01, 0x23, 0x45, 0x67,
        "INC [offset32]");

    return errors;
}

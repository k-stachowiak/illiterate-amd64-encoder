/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The LEA instruction
// ===================

bool amd64_enc_lea(struct AMD64EncContext *ec,
                   struct AMD64Location src,
                   struct AMD64Location dst) {

    assert(ec);

    if (dst.type != AMD64_LT_REGISTER_GP) {
        ec->error_message = "Can only LEA into a general purpose register";
        return false;
    }

    if (dst.size == AMD64_LS_8_BIT) {
        ec->error_message = "Can only LEA into a non-8-bit register";
        return false;
    }

    if (!amd64_loc_is_memory(&src)) {
        ec->error_message = "Can only LEA from a memory location";
        return false;
    }

    // LEA mem -> reg: 8D /r
    enc_start(ec);
    enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &dst, &src);
    enc_sized_opcode(ec, dst.size, 0x00, 0x8D);
    enc_modrm_2arg(ec, modrm_gpreg(dst.gpreg.gpreg), &src);
    enc_finish(ec);
    return true;
}

// Test code
// =========

static int test_lea_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_lea(&ec, WPTR32(SI), WPTR32_EX(DX, 0x01));
    errors += expect_failure(&ec, "Disallow LEA into a memory location");

    amd64_enc_lea(&ec, BPTR32(BX), AL);
    errors += expect_failure(&ec, "Disallow LEA into an 8-bit register");

    amd64_enc_lea(&ec, AX, WPTR32(SI));
    errors += expect_failure(&ec, "Disallow LEA from a register location");

    return errors;
}

static int test_lea_mem(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_lea(&ec, BRIPOFF(0x01), ESI);
    errors += expect_success_6(
        &ec, &sis,
        0x8D, 0x35, 0x01, 0x00, 0x00, 0x00,
        "LEA disp8(RIP) -> ESI");

    amd64_enc_lea(&ec, WPTR32_EX(AX, 0x67452301), BX);
    errors += expect_success_8(
        &ec, &sis,
        0x67, 0x66, 0x8D, 0x98, 0x01, 0x23, 0x45, 0x67,
        "LEA disp32(EAX) -> BX");

    amd64_enc_lea(&ec, DWPTR32_EX(BP, 0x01), ESP);
    errors += expect_success_4(
        &ec, &sis,
        0x67, 0x8D, 0x65, 0x01,
        "LEA disp8(EBP) -> ESP");

    amd64_enc_lea(&ec, QWSIB64(4, DI, 15, 0), R9);
    errors += expect_success_4(
        &ec, &sis,
        0x4D, 0x8D, 0x0C, 0xBF,
        "LEA (4*RDI+R15) -> R9");

    return errors;
}

int test_lea(void) {
    return
        test_lea_invalid() +
        test_lea_mem();
}

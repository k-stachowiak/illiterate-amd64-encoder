/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The IDIV instruction
// ====================

bool amd64_enc_idiv(struct AMD64EncContext *ec, struct AMD64Location divisor) {

    assert(ec);

    if (!amd64_loc_is_gp_rm(&divisor)) {
        ec->error_message = "Can only perform IDIV on a general purpose r/m location";
        return false;
    }

    // IDIV r/m: F6/F7 /7
    enc_start(ec);
    enc_prefixes(ec, AMD64_LS_32_BIT, divisor.size, false, NULL, &divisor);
    enc_sized_opcode(ec, divisor.size, 0xF6, 0xF7);
    enc_modrm_1arg(ec, 7, &divisor);
    enc_finish(ec);
    return true;
}

// Test code
// =========

static int test_idiv_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_idiv(&ec, IMM8(0));
    errors += expect_failure(&ec, "Disallow IDIV by an immediate");

    amd64_enc_idiv(&ec, IMM64(0));
    errors += expect_failure(&ec, "Disallow IDIV by an immediate");

    return errors;
}

static int test_idiv_rm(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_idiv(&ec, BH);
    errors += expect_success_2(
        &ec, &sis,
        0xF6, 0xFF,
        "IDIV BH");

    amd64_enc_idiv(&ec, CL);
    errors += expect_success_2(
        &ec, &sis,
        0xF6, 0xF9,
        "IDIV CL");

    amd64_enc_idiv(&ec, DIL);
    errors += expect_success_3(
        &ec, &sis,
        0x40, 0xF6, 0xFF,
        "IDIV DIL");

    amd64_enc_idiv(&ec, WPTR32(SI));
    errors += expect_success_4(
        &ec, &sis,
        0x67, 0x66, 0xF7, 0x3E,
        "IDIV [ESI]");

    amd64_enc_idiv(&ec, DWPTR32_EX(DX, 0x67452301));
    errors += expect_success_7(
        &ec, &sis,
        0x67, 0xF7, 0xBA, 0x01, 0x23, 0x45, 0x67,
        "IDIV [EDX]+disp32");

    amd64_enc_idiv(&ec, QWSIB32(1, 8, DI, 0x01));
    errors += expect_success_6(
        &ec, &sis,
        0x67, 0x4A, 0xF7, 0x7C, 0x07, 0x01,
        "IDIV [1*R8D+EDI]+disp8");

    return errors;
}

int test_idiv(void) {
    return
        test_idiv_invalid() +
        test_idiv_rm();
}

/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The MOV instruction
// ===================

bool amd64_enc_mov(struct AMD64EncContext *ec,
                   struct AMD64Location src,
                   struct AMD64Location dst) {

    // This function can generate a subset of possible encodings of the MOV
    // instruction. Which one is chosen depends on the argument types. Some
    // argument combinations are illegal, while other have not been implemented
    // as they seemed not useful for modern targets. The illegal and
    // unimplemented cases will be signalled with the result's structure
    // success field set to false, and errmsg pointing to a brief error message.

    enum AMD64LocationSize size;

    assert(ec);

    // Assert operand sizes:
    if (!(src.size == dst.size) &&
        !(src.type == AMD64_LT_IMMEDIATE && src.size == AMD64_LS_32_BIT &&
          amd64_loc_is_gp_rm(&dst) && dst.size == AMD64_LS_64_BIT)) {
        ec->error_message = "Size mismatch between MOV operands";
        return false;
    }
    size = dst.size;

    // Prevent use of the "high" registers, when the REX prefix is in order
    if ((rex_prefix_required(&src) ||
         rex_prefix_required(&dst)) &&
        ((src.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(src.gpreg.gpreg)) ||
         (dst.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(dst.gpreg.gpreg)))) {
        ec->error_message = "High register not allowed when REX prefix used";
        return false;
    }

    // Prevent use of the "high" registers, when the REX prefix is in order
    if ((loc_in_funny_register(&src) ||
         loc_in_funny_register(&dst)) &&
        ((src.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(src.gpreg.gpreg)) ||
         (dst.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(dst.gpreg.gpreg)))) {
        ec->error_message = "High and funny register not allowed in one instruction";
        return false;
    }

    enc_start(ec);
    if (dst.type == AMD64_LT_REGISTER_GP &&
        dst.gpreg.gpreg == AMD64_GPREG_AX &&
        src.type == AMD64_LT_MEMORY_OFFSET_BIG) {

        // MOV moffset -> AX: 0xA0/0xA1 /i
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &dst, NULL);
        enc_sized_opcode(ec, size, 0xA0, 0xA1);
        ec_put_offset_big_le(ec, src.moffset_big.offset);
        enc_finish(ec);
        return true;

    } else if (src.type == AMD64_LT_REGISTER_GP &&
               src.gpreg.gpreg == AMD64_GPREG_AX &&
               dst.type == AMD64_LT_MEMORY_OFFSET_BIG) {

        // MOV AX -> moffset: 0xA2/0xA3
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &src, NULL);
        enc_sized_opcode(ec, size, 0xA2, 0xA3);
        ec_put_offset_big_le(ec, dst.moffset_big.offset);
        enc_finish(ec);
        return true;

    } else if (src.type == AMD64_LT_REGISTER_GP && amd64_loc_is_gp_rm(&dst)) {

        // MOV reg -> reg/mem: 0x88/0x89 /r
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &src, &dst);
        enc_sized_opcode(ec, size, 0x88, 0x89);
        enc_modrm_2arg(ec, modrm_gpreg(src.gpreg.gpreg), &dst);
        enc_finish(ec);
        return true;

    } else if (amd64_loc_is_gp_rm(&src) && dst.type == AMD64_LT_REGISTER_GP) {

        // MOV reg/mem -> reg: 0x8A/0x8B /r
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &dst, &src);
        enc_sized_opcode(ec, size, 0x8A, 0x8B);
        enc_modrm_2arg(ec, modrm_gpreg(dst.gpreg.gpreg), &src);
        enc_finish(ec);
        return true;

    } else if (src.type == AMD64_LT_IMMEDIATE &&
               dst.type == AMD64_LT_REGISTER_GP &&
               src.size == dst.size) {

        // MOV imm -> reg: 0xB0/0xB8 +r i
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, true, &dst, NULL);
        enc_incr_sized_opcode(ec, size, 0xB0, 0xB8, dst.gpreg.gpreg);
        ec_put_immediate_le(ec, src.size, &src.imm);
        enc_finish(ec);
        return true;

    } else if (src.type == AMD64_LT_IMMEDIATE && amd64_loc_is_gp_rm(&dst)) {

        // MOV imm -> reg/mem: C6/C7 /0 i

        // Note1: It's impossible to encode move of 64-bit immediate directly
        //        into memory!
        if (src.size == AMD64_LS_64_BIT) {
            ec->error_message = "Illegal move imm64 -> reg/mem";
            return false;
        }

        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, NULL, &dst);
        enc_sized_opcode(ec, size, 0xC6, 0xC7);
        enc_modrm_1arg(ec, 0, &dst);
        ec_put_immediate_le(ec, src.size, &src.imm);
        enc_finish(ec);
        return true;

    } else {
        ec->error_message = "Unhandled operands for MOV";
        return false;

    }
}

// Test code
// =========

// MOV instruction has been tested paifully thoroughly to verify not only
// encoding of the instruction but also its operands in different combinations.

static int test_mov_case_2(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_2(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            message);
}

static int test_mov_case_3(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           uint8_t expected_3,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_3(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            expected_3,
                            message);
}

static int test_mov_case_4(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           uint8_t expected_3,
                           uint8_t expected_4,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_4(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            expected_3,
                            expected_4,
                            message);
}

static int test_mov_case_5(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           uint8_t expected_3,
                           uint8_t expected_4,
                           uint8_t expected_5,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_5(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            expected_3,
                            expected_4,
                            expected_5,
                            message);
}

static int test_mov_case_6(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           uint8_t expected_3,
                           uint8_t expected_4,
                           uint8_t expected_5,
                           uint8_t expected_6,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_6(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            expected_3,
                            expected_4,
                            expected_5,
                            expected_6,
                            message);
}

static int test_mov_case_7(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           uint8_t expected_3,
                           uint8_t expected_4,
                           uint8_t expected_5,
                           uint8_t expected_6,
                           uint8_t expected_7,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_7(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            expected_3,
                            expected_4,
                            expected_5,
                            expected_6,
                            expected_7,
                            message);
}

static int test_mov_case_8(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           uint8_t expected_3,
                           uint8_t expected_4,
                           uint8_t expected_5,
                           uint8_t expected_6,
                           uint8_t expected_7,
                           uint8_t expected_8,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_8(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            expected_3,
                            expected_4,
                            expected_5,
                            expected_6,
                            expected_7,
                            expected_8,
                            message);
}

static int test_mov_case_9(struct AMD64Location x,
                           struct AMD64Location y,
                           uint8_t expected_1,
                           uint8_t expected_2,
                           uint8_t expected_3,
                           uint8_t expected_4,
                           uint8_t expected_5,
                           uint8_t expected_6,
                           uint8_t expected_7,
                           uint8_t expected_8,
                           uint8_t expected_9,
                           char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_9(&ec,
                            &sis,
                            expected_1,
                            expected_2,
                            expected_3,
                            expected_4,
                            expected_5,
                            expected_6,
                            expected_7,
                            expected_8,
                            expected_9,
                            message);
}

static int test_mov_case_10(struct AMD64Location x,
                            struct AMD64Location y,
                            uint8_t expected_1,
                            uint8_t expected_2,
                            uint8_t expected_3,
                            uint8_t expected_4,
                            uint8_t expected_5,
                            uint8_t expected_6,
                            uint8_t expected_7,
                            uint8_t expected_8,
                            uint8_t expected_9,
                            uint8_t expected_10,
                            char *message) {
    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, x, y);
    return expect_success_10(&ec,
                             &sis,
                             expected_1,
                             expected_2,
                             expected_3,
                             expected_4,
                             expected_5,
                             expected_6,
                             expected_7,
                             expected_8,
                             expected_9,
                             expected_10,
                             message);
}

static int test_mov_invalid(void) {

    // This function checks very simple error conditions for mismatched operand
    // types.

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_mov(&ec, IMM8(0), WOFFSET32(0));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV between locations of different sizes");

    amd64_enc_mov(&ec, RAX, AH);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, BH, R8);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, BSIB32(1, AX, 8, 0), CH);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, DH, BSIB32(1, 8, AX, 0));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, SPL, AH);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, BH, BPL);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, SIL, CH);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, DH, DIL);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV to/from \"high\" registers when REX prefix used");

    amd64_enc_mov(&ec, IMM8(0), IMM8(0));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV between two immediates");

    amd64_enc_mov(&ec, IMM64(0), WOFFSET32(0));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV between a 64-bit immediate and a memory location");

    return errors;
}

static int test_mov_reg_reg_dir_old(void) {

    int errors = 0;

    // MOV old register <-> old register
    //
    // This sequence of tests checks basic instruction encoding and accounts
    // for potential symmetry related issues by testing a subset of moves both
    // ways.
    //
    // This only tests the old registers, but refers to all of them.

    errors += test_mov_case_2(AH, BH, 0x88, 0xE7, "MOV AH -> BH");
    errors += test_mov_case_2(BH, AH, 0x88, 0xFC, "MOV BH -> AH");
    errors += test_mov_case_2(AH, BL, 0x88, 0xE3, "MOV AH -> BL");
    errors += test_mov_case_2(BL, AH, 0x88, 0xDC, "MOV BL -> AH");
    errors += test_mov_case_2(BH, AL, 0x88, 0xF8, "MOV BH -> AL");
    errors += test_mov_case_2(AL, BH, 0x88, 0xC7, "MOV AL -> BH");
    errors += test_mov_case_2(AL, BL, 0x88, 0xC3, "MOV AL -> BL");
    errors += test_mov_case_2(BL, AL, 0x88, 0xD8, "MOV BL -> AL");

    errors += test_mov_case_3(CX, DX, 0x66, 0x89, 0xCA, "MOV CX -> DX");
    errors += test_mov_case_3(DX, CX, 0x66, 0x89, 0xD1, "MOV DX -> CX");

    errors += test_mov_case_2(ESI, EDI, 0x89, 0xF7, "MOV ESI -> EDI");
    errors += test_mov_case_2(EDI, ESI, 0x89, 0xFE, "MOV EDI -> ESI");

    errors += test_mov_case_3(RBP, RSP, 0x48, 0x89, 0xEC, "MOV RBP -> RSP");
    errors += test_mov_case_3(RSP, RBP, 0x48, 0x89, 0xE5, "MOV RSP -> RBP");

    return errors;
}

static int test_mov_reg_reg_dir_cross(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // MOV old register <-> extended register
    //
    // This test verifies proper generation of MOV instructions that refer
    // to the old and the extended registers.
    //
    // It doesn't refer to all the registers, as there would be unnecesarily
    // many combinations to handle, but it focuses on endocing access to
    // different kinds of them.

    amd64_enc_mov(&ec, AH, R8B);
    errors += expect_failure(
        &ec,
        "Shouldn't allow use of the \"high\" registers when REX prefix present");

    amd64_enc_mov(&ec, R8B, AH);
    errors += expect_failure(
        &ec,
        "Shouldn't allow use of the \"high\" registers when REX prefix present");

    errors += test_mov_case_3(AL, R8B, 0x41, 0x88, 0xC0, "MOV AL -> R8B");
    errors += test_mov_case_3(R8B, AL, 0x44, 0x88, 0xC0, "MOV R8B -> AL");
    errors += test_mov_case_4(AX, R8W, 0x66, 0x41, 0x89, 0xC0, "MOV AX -> R8W");
    errors += test_mov_case_4(R8W, AX, 0x66, 0x44, 0x89, 0xC0, "MOV R8W -> AX");
    errors += test_mov_case_3(EDI, R15D, 0x41, 0x89, 0xFF, "MOV EDI -> R15D");
    errors += test_mov_case_3(R15D, EDI, 0x44, 0x89, 0xFF, "MOV R15D -> EDI");
    errors += test_mov_case_3(RDI, R15, 0x49, 0x89, 0xFF, "MOV RDI -> R15");
    errors += test_mov_case_3(R15, RDI, 0x4C, 0x89, 0xFF, "MOV R15 -> RDI");

    return errors;
}

static int test_mov_reg_reg_dir_funny(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    errors += test_mov_case_3(AL, SPL, 0x40, 0x88, 0xC4, "MOV AL -> SPL");
    errors += test_mov_case_3(SPL, AL, 0x40, 0x88, 0xE0, "MOV SPL -> AL");

    errors += test_mov_case_3(BL, BPL, 0x40, 0x88, 0xDD, "MOV BL -> BPL");
    errors += test_mov_case_3(BPL, BL, 0x40, 0x88, 0xEB, "MOV BPL -> BL");

    errors += test_mov_case_3(CL, SIL, 0x40, 0x88, 0xCE, "MOV CL -> SIL");
    errors += test_mov_case_3(SIL, CL, 0x40, 0x88, 0xF1, "MOV SIL -> CL");

    errors += test_mov_case_3(DL, DIL, 0x40, 0x88, 0xD7, "MOV DL -> DIL");
    errors += test_mov_case_3(DIL, DL, 0x40, 0x88, 0xFA, "MOV DIL -> DL");

    return errors;
}

static int test_mov_reg_reg_indir_default(void) {

    // This is going to be a long one, so sit tight!

    int errors = 0;

    // Zero displacement

    // MOV [EAX] <-> CL
    errors += test_mov_case_3(
        BPTR32(AX), CL, 0x67, 0x8A, 0x08, "MOV [EAX] -> CL");
    errors += test_mov_case_3(
        CL, BPTR32(AX), 0x67, 0x88, 0x08, "MOV CL -> [EAX]");

    // MOV [RBX] <-> DL
    errors += test_mov_case_2(
        BPTR64(BX), DL, 0x8A, 0x13, "MOV [RBX] -> DL");
    errors += test_mov_case_2(
        DL, BPTR64(BX), 0x88, 0x13, "MOV DL -> [RBX]");

    // MOV [ESI] <-> R8B
    errors += test_mov_case_4(
        BPTR32(SI), R8B, 0x67, 0x44, 0x8A, 0x06, "MOV [ESI] -> R8B");
    errors += test_mov_case_4(
        R8B, BPTR32(SI), 0x67, 0x44, 0x88, 0x06, "MOV R8B -> [ESI]");

    // MOV [RDI] <-> R15B
    errors += test_mov_case_3(
        BPTR64(DI), R15B, 0x44, 0x8A, 0x3F, "MOV [RDI] -> R15B");
    errors += test_mov_case_3(
        R15B, BPTR64(DI), 0x44, 0x88, 0x3F, "MOV R15B -> [RDI]");

    // MOV [RDI] <-> SPL
    errors += test_mov_case_3(
        BPTR64(DI), SPL, 0x40, 0x8A, 0x27, "MOV [RDI] -> SPL");
    errors += test_mov_case_3(
        SPL, BPTR64(DI), 0x40, 0x88, 0x27, "MOV SPL -> [RDI]");

    // MOV [ESI] <-> BPL
    errors += test_mov_case_4(
        BPTR32(SI), BPL, 0x67, 0x40, 0x8A, 0x2E, "MOV [ESI] -> BPL");
    errors += test_mov_case_4(
        BPL, BPTR32(SI), 0x67, 0x40, 0x88, 0x2E, "MOV BPL -> [ESI]");

    // MOV [RBX] <-> SIL
    errors += test_mov_case_3(
        BPTR64(BX), SIL, 0x40, 0x8A, 0x33, "MOV [RBX] -> SIL");
    errors += test_mov_case_3(
        SIL, BPTR64(BX), 0x40, 0x88, 0x33, "MOV SIL -> [RBX]");

    // MOV [EAX] <-> DIL
    errors += test_mov_case_4(
        BPTR32(AX), DIL, 0x67, 0x40, 0x8A, 0x38, "MOV [EAX] -> DIL");
    errors += test_mov_case_4(
        DIL, BPTR32(AX), 0x67, 0x40, 0x88, 0x38, "MOV DIL -> [EAX]");

    // MOV [EAX] <-> CX
    errors += test_mov_case_4(
        WPTR32(AX), CX, 0x67, 0x66, 0x8B, 0x08, "MOV [EAX] -> CX");
    errors += test_mov_case_4(
        CX, WPTR32(AX), 0x67, 0x66, 0x89, 0x08, "MOV CX -> [EAX]");

    // MOV [RBX] <-> DX
    errors += test_mov_case_3(
        WPTR64(BX), DX, 0x66, 0x8B, 0x13, "MOV [RBX] -> DX");
    errors += test_mov_case_3(
        DX, WPTR64(BX), 0x66, 0x89, 0x13, "MOV DX -> [RBX]");

    // MOV [ESI] <-> R8W
    errors += test_mov_case_5(
        WPTR32(SI), R8W, 0x67, 0x66, 0x44, 0x8B, 0x06, "MOV [ESI] -> R8W");
    errors += test_mov_case_5(
        R8W, WPTR32(SI), 0x67, 0x66, 0x44, 0x89, 0x06, "MOV R8W -> [ESI]");

    // MOV [RDI] <-> R15W
    errors += test_mov_case_4(
        WPTR64(DI), R15W, 0x66, 0x44, 0x8B, 0x3F, "MOV [RDI] -> R15W");
    errors += test_mov_case_4(
        R15W, WPTR64(DI), 0x66, 0x44, 0x89, 0x3F, "MOV R15W -> [RDI]");

    // MOV [EAX] <-> ECX
    errors += test_mov_case_3(
        DWPTR32(AX), ECX, 0x67, 0x8B, 0x08, "MOV [EAX] -> ECX");
    errors += test_mov_case_3(
        ECX, DWPTR32(AX), 0x67, 0x89, 0x08, "MOV ECX -> [EAX]");

    // MOV [RBX] <-> EDX
    errors += test_mov_case_2(
        DWPTR64(BX), EDX, 0x8B, 0x13, "MOV [RBX] -> EDX");
    errors += test_mov_case_2(
        EDX, DWPTR64(BX), 0x89, 0x13, "MOV EDX -> [RBX]");

    // MOV [ESI] <-> R8D
    errors += test_mov_case_4(
        DWPTR32(SI), R8D, 0x67, 0x44, 0x8B, 0x06, "MOV [ESI] -> R8D");
    errors += test_mov_case_4(
        R8D, DWPTR32(SI), 0x67, 0x44, 0x89, 0x06, "MOV R8D -> [ESI]");

    // MOV [RDI] <-> R15D
    errors += test_mov_case_3(
        DWPTR64(DI), R15D, 0x44, 0x8B, 0x3F, "MOV [RDI] -> R15D");
    errors += test_mov_case_3(
        R15D, DWPTR64(DI), 0x44, 0x89, 0x3F, "MOV R15D -> [RDI]");

    // MOV [EAX] <-> RCX
    errors += test_mov_case_4(
        QWPTR32(AX), RCX, 0x67, 0x48, 0x8B, 0x08, "MOV [EAX] -> RCX");
    errors += test_mov_case_4(
        RCX, QWPTR32(AX), 0x67, 0x48, 0x89, 0x08, "MOV RCX -> [EAX]");

    // MOV [RBX] <-> RDX
    errors += test_mov_case_3(
        QWPTR64(BX), RDX, 0x48, 0x8B, 0x13, "MOV [RBX] -> RDX");
    errors += test_mov_case_3(
        RDX, QWPTR64(BX), 0x48, 0x89, 0x13, "MOV RDX -> [RBX]");

    // MOV [ESI] <-> R8
    errors += test_mov_case_4(
        QWPTR32(SI), R8, 0x67, 0x4C, 0x8B, 0x06, "MOV [ESI] -> R8");
    errors += test_mov_case_4(
        R8, QWPTR32(SI), 0x67, 0x4C, 0x89, 0x06, "MOV R8 -> [ESI]");

    // MOV [RDI] <-> R15
    errors += test_mov_case_3(
        QWPTR64(DI), R15, 0x4C, 0x8B, 0x3F, "MOV [RDI] -> R15");
    errors += test_mov_case_3(
        R15, QWPTR64(DI), 0x4C, 0x89, 0x3F, "MOV R15 -> [RDI]");

    // 8-bit displacement

    // MOV [EAX]+disp8 <-> CL
    errors += test_mov_case_4(
        BPTR32_EX(AX, 0x08), CL, 0x67, 0x8A, 0x48, 0x08, "MOV [EAX]+disp8 -> CL");
    errors += test_mov_case_4(
        CL, BPTR32_EX(AX, 0x08), 0x67, 0x88, 0x48, 0x08, "MOV CL -> [EAX]+disp8");

    // MOV [EAX]-disp8 <-> CL
    errors += test_mov_case_4(
        BPTR32_EX(AX, -0x08), CL, 0x67, 0x8A, 0x48, -0x08, "MOV [EAX]-disp8 -> CL");
    errors += test_mov_case_4(
        CL, BPTR32_EX(AX, -0x08), 0x67, 0x88, 0x48, -0x08, "MOV CL -> [EAX]-disp8");

    // MOV [EAX]+disp8 <-> BPL
    errors += test_mov_case_5(
        BPTR32_EX(AX, 0x08), BPL, 0x67, 0x40, 0x8A, 0x68, 0x08, "MOV [EAX]+disp8 -> BPL");
    errors += test_mov_case_5(
        BPL, BPTR32_EX(AX, 0x08), 0x67, 0x40, 0x88, 0x68, 0x08, "MOV BPL -> [EAX]+disp8");

    // MOV [EAX]-disp8 <-> BPL
    errors += test_mov_case_5(
        BPTR32_EX(AX, -0x08), BPL, 0x67, 0x40, 0x8A, 0x68, -0x08, "MOV [EAX]-disp8 -> BPL");
    errors += test_mov_case_5(
        BPL, BPTR32_EX(AX, -0x08), 0x67, 0x40, 0x88, 0x68, -0x08, "MOV BPL -> [EAX]-disp8");

    // MOV [RBX]+disp8 <-> DX
    errors += test_mov_case_4(
        WPTR64_EX(BX, 0x08), DX,
        0x66, 0x8B, 0x53, 0x08,
        "MOV [RBX]+disp8 -> DX");
    errors += test_mov_case_4(
        DX, WPTR64_EX(BX, 0x08),
        0x66, 0x89, 0x53, 0x08,
        "MOV DX -> [RBX]+disp8");

    // MOV [RBX]-disp8 <-> DX
    errors += test_mov_case_4(
        WPTR64_EX(BX, -0x08), DX,
        0x66, 0x8B, 0x53, -0x08,
        "MOV [RBX]-disp8 -> DX");
    errors += test_mov_case_4(
        DX, WPTR64_EX(BX, -0x08),
        0x66, 0x89, 0x53, -0x08,
        "MOV DX -> [RBX]-disp8");

    // MOV [ESI]+disp8 <-> R8D
    errors += test_mov_case_5(
        DWPTR32_EX(SI, 0x08), R8D,
        0x67, 0x44, 0x8B, 0x46, 0x08,
        "MOV [ESI]+disp8 -> R8D");
    errors += test_mov_case_5(
        R8D, DWPTR32_EX(SI, 0x08),
        0x67, 0x44, 0x89, 0x46, 0x08,
        "MOV R8D -> [ESI]+disp8");

    // MOV [ESI]-disp8 <-> R8D
    errors += test_mov_case_5(
        DWPTR32_EX(SI, -0x08), R8D,
        0x67, 0x44, 0x8B, 0x46, -0x08,
        "MOV [ESI]-disp8 -> R8D");
    errors += test_mov_case_5(
        R8D, DWPTR32_EX(SI, -0x08),
        0x67, 0x44, 0x89, 0x46, -0x08,
        "MOV R8D -> [ESI]-disp8");

    // MOV [RDI]+disp8 <-> R8D
    errors += test_mov_case_4(
        QWPTR64_EX(DI, 0x08), R8,
        0x4C, 0x8B, 0x47, 0x08,
        "MOV [RDI]+disp8 -> R8");
    errors += test_mov_case_4(
        R8, QWPTR64_EX(DI, 0x08),
        0x4C, 0x89, 0x47, 0x08,
        "MOV R8 -> [RDI]+disp8");

    // MOV [RDI]-disp8 <-> R8D
    errors += test_mov_case_4(
        QWPTR64_EX(DI, -0x08), R8,
        0x4C, 0x8B, 0x47, -0x08,
        "MOV [RDI]-disp8 -> R8");
    errors += test_mov_case_4(
        R8, QWPTR64_EX(DI, -0x08),
        0x4C, 0x89, 0x47, -0x08,
        "MOV R8 -> [RDI]-disp8");

    // 32-bit displacement

    // MOV [RDI]+disp32 <-> R15B
    errors += test_mov_case_7(
        BPTR64_EX(DI, 0x01234567), R15B,
        0x44, 0x8A, 0xBF, 0x67, 0x45, 0x23, 0x01,
        "MOV [RDI]+disp32 -> R15B");
    errors += test_mov_case_7(
        R15B, BPTR64_EX(DI, 0x01234567),
        0x44, 0x88, 0xBF, 0x67, 0x45, 0x23, 0x01,
        "MOV R15B -> [RDI]+disp32");

    // MOV [RDI]-disp32 <-> R15B
    errors += test_mov_case_7(
        BPTR64_EX(DI, -0x01234567), R15B,
        0x44, 0x8A, 0xBF, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV [RDI]-disp32 -> R15B");
    errors += test_mov_case_7(
        R15B, BPTR64_EX(DI, -0x01234567),
        0x44, 0x88, 0xBF, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV R15B -> [RDI]-disp32");

    // MOV [RDI]+disp32 <-> SIL
    errors += test_mov_case_7(
        BPTR64_EX(DI, 0x01234567), SIL,
        0x40, 0x8A, 0xB7, 0x67, 0x45, 0x23, 0x01,
        "MOV [RDI]+disp32 -> SIL");
    errors += test_mov_case_7(
        SIL, BPTR64_EX(DI, 0x01234567),
        0x40, 0x88, 0xB7, 0x67, 0x45, 0x23, 0x01,
        "MOV SIL -> [RDI]+disp32");

    // MOV [RDI]-disp32 <-> SIL
    errors += test_mov_case_7(
        BPTR64_EX(DI, -0x01234567), SIL,
        0x40, 0x8A, 0xB7, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV [RDI]-disp32 -> SIL");
    errors += test_mov_case_7(
        SIL, BPTR64_EX(DI, -0x01234567),
        0x40, 0x88, 0xB7, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV SIL -> [RDI]-disp32");

    // MOV [ESI]+disp32 <-> R8W
    errors += test_mov_case_9(
        WPTR32_EX(SI, 0x01234567), R8W,
        0x67, 0x66, 0x44, 0x8B, 0x86, 0x67, 0x45, 0x23, 0x01,
        "MOV [ESI]+disp32 -> R8W");
    errors += test_mov_case_9(
        R8W, WPTR32_EX(SI, 0x01234567),
        0x67, 0x66, 0x44, 0x89, 0x86, 0x67, 0x45, 0x23, 0x01,
        "MOV R8W -> [ESI]+disp32");

    // MOV [ESI]-disp32 <-> R8W
    errors += test_mov_case_9(
        WPTR32_EX(SI, -0x01234567), R8W,
        0x67, 0x66, 0x44, 0x8B, 0x86, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV [ESI]-disp32 -> R8W");
    errors += test_mov_case_9(
        R8W, WPTR32_EX(SI, -0x01234567),
        0x67, 0x66, 0x44, 0x89, 0x86, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV R8W -> [ESI]-disp32");

    // MOV [RBX]+disp32 <-> EDX
    errors += test_mov_case_6(
        DWPTR64_EX(BX, 0x01234567), EDX,
        0x8B, 0x93, 0x67, 0x45, 0x23, 0x01,
        "MOV [RBX]+disp32 -> EDX");
    errors += test_mov_case_6(
        EDX, DWPTR64_EX(BX, 0x01234567),
        0x89, 0x93, 0x67, 0x45, 0x23, 0x01,
        "MOV EDX -> [RBX]+disp32");

    // MOV [RBX]-disp32 <-> EDX
    errors += test_mov_case_6(
        DWPTR64_EX(BX, -0x01234567), EDX,
        0x8B, 0x93, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV [RBX]-disp32 -> EDX");
    errors += test_mov_case_6(
        EDX, DWPTR64_EX(BX, -0x01234567),
        0x89, 0x93, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV EDX -> [RBX]-disp32");

    // MOV [EAX]+disp32 <-> RCX
    errors += test_mov_case_8(
        QWPTR32_EX(AX, 0x01234567), RCX,
        0x67, 0x48, 0x8B, 0x88, 0x67, 0x45, 0x23, 0x01,
        "MOV [EAX]+disp32 -> RCX");
    errors += test_mov_case_8(
        RCX, QWPTR32_EX(AX, 0x01234567),
        0x67, 0x48, 0x89, 0x88, 0x67, 0x45, 0x23, 0x01,
        "MOV RCX -> [EAX]+disp32");

    // MOV [EAX]-disp32 <-> RCX
    errors += test_mov_case_8(
        QWPTR32_EX(AX, -0x01234567), RCX,
        0x67, 0x48, 0x8B, 0x88, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV [EAX]-disp32 -> RCX");
    errors += test_mov_case_8(
        RCX, QWPTR32_EX(AX, -0x01234567),
        0x67, 0x48, 0x89, 0x88, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV RCX -> [EAX]-disp32");

    return errors;
}

static int test_mov_reg_reg_indir_rbp(void) {

    int errors = 0;

    // 8-bit operands
    errors += test_mov_case_4(
        AL, BPTR32(BP),
        0x67, 0x88, 0x45, 0x00,
        "MOV AL -> [EBP]");
    errors += test_mov_case_4(
        BPTR32(BP), AL,
        0x67, 0x8A, 0x45, 0x00,
        "MOV [EBP] -> AL");

    errors += test_mov_case_3(
        AL, BPTR64_EX(BP, 0x01),
        0x88, 0x45, 0x01,
        "MOV AL -> [RBP]+disp8");
    errors += test_mov_case_3(
        BPTR64_EX(BP, 0x01), AL,
        0x8A, 0x45, 0x01,
        "MOV [RBP]+disp8 -> AL");

    errors += test_mov_case_5(
        DIL, BPTR32(BP),
        0x67, 0x40, 0x88, 0x7D, 0x00,
        "MOV DIL -> [EBP]");
    errors += test_mov_case_5(
        BPTR32(BP), DIL,
        0x67, 0x40, 0x8A, 0x7D, 0x00,
        "MOV [EBP] -> DIL");

    errors += test_mov_case_4(
        DIL, BPTR64_EX(BP, 0x01),
        0x40, 0x88, 0x7D, 0x01,
        "MOV DIL -> [RBP]+disp8");
    errors += test_mov_case_4(
        BPTR64_EX(BP, 0x01), DIL,
        0x40, 0x8A, 0x7D, 0x01,
        "MOV [RBP]+disp8 -> DIL");

    // 16-bit operands
    errors += test_mov_case_6(
        BX, WPTR32_EX(13, 0x01),
        0x67, 0x66, 0x41, 0x89, 0x5D, 0x01,
        "MOV BX -> [R13D]+disp8");
    errors += test_mov_case_6(
        WPTR32_EX(13, 0x01), BX,
        0x67, 0x66, 0x41, 0x8B, 0x5D, 0x01,
        "MOV [R13D]+disp8 -> BX");

    // 32-bit operands
    errors += test_mov_case_8(
        R8D, DWPTR32_EX(BP, 0x67452301),
        0x67, 0x44, 0x89, 0x85, 0x01, 0x23, 0x45, 0x67,
        "MOV R8D -> [EBP]+disp32");
    errors += test_mov_case_8(
        DWPTR32_EX(BP, 0x67452301), R8D,
        0x67, 0x44, 0x8B, 0x85, 0x01, 0x23, 0x45, 0x67,
        "MOV [EBP]+disp32 -> R8D");

    errors += test_mov_case_7(
        R8D, DWPTR64_EX(13, 0x67452301),
        0x45, 0x89, 0x85, 0x01, 0x23, 0x45, 0x67,
        "MOV R8D -> [R13]+disp32");

    errors += test_mov_case_7(
        DWPTR64_EX(13, 0x67452301), R8D,
        0x45, 0x8B, 0x85, 0x01, 0x23, 0x45, 0x67,
        "MOV [R13]+disp32 -> R8D");

    // 64-bit operands
    errors += test_mov_case_4(
        R15, QWPTR64(13),
        0x4D, 0x89, 0x7D, 0x00,
        "MOV R15 -> [R13]");
    errors += test_mov_case_4(
        QWPTR64(13), R15,
        0x4D, 0x8B, 0x7D, 0x00,
        "MOV [R13] -> R15");

    return errors;
}

static int test_mov_reg_reg_indir_rsp(void) {

    int errors = 0;

    // 8-bit operands
    errors += test_mov_case_5(
        CL, BPTR32(12),
        0x67, 0x41, 0x88, 0x0C, 0x24,
        "MOV CL -> [R12D]");
    errors += test_mov_case_5(
        BPTR32(12), CL,
        0x67, 0x41, 0x8A, 0x0C, 0x24,
        "MOV [R12D] -> CL");

    errors += test_mov_case_5(
        CL, BPTR64_EX(12, 0x01),
        0x41, 0x88, 0x4C, 0x24, 0x01,
        "MOV CL -> [R12]+disp8");
    errors += test_mov_case_5(
        BPTR64_EX(12, 0x01), CL,
        0x41, 0x8A, 0x4C, 0x24, 0x01,
        "MOV [R12]+disp8 -> CL");

    errors += test_mov_case_5(
        SIL, BPTR32(12),
        0x67, 0x41, 0x88, 0x34, 0x24,
        "MOV SIL -> [R12D]");
    errors += test_mov_case_5(
        BPTR32(12), SIL,
        0x67, 0x41, 0x8A, 0x34, 0x24,
        "MOV [R12D] -> SIL");

    errors += test_mov_case_5(
        SIL, BPTR64_EX(12, 0x01),
        0x41, 0x88, 0x74, 0x24, 0x01,
        "MOV SIL -> [R12]+disp8");
    errors += test_mov_case_5(
        BPTR64_EX(12, 0x01), SIL,
        0x41, 0x8A, 0x74, 0x24, 0x01,
        "MOV [R12]+disp8 -> SIL");

    // 16-bit operands
    errors += test_mov_case_6(
        DX, WPTR32_EX(SP, 0x01),
        0x67, 0x66, 0x89, 0x54, 0x24, 0x01,
        "MOV DX -> [ESP]+disp8");
    errors += test_mov_case_6(
        WPTR32_EX(SP, 0x01), DX,
        0x67, 0x66, 0x8B, 0x54, 0x24, 0x01,
        "MOV [ESP]+disp8 -> DX");

    // 32-bit operands
    errors += test_mov_case_9(
        R9D, DWPTR32_EX(12, 0x67452301),
        0x67, 0x45, 0x89, 0x8C, 0x24, 0x01, 0x23, 0x45, 0x67,
        "MOV R9D -> [R12D]+disp32");
    errors += test_mov_case_9(
        DWPTR32_EX(12, 0x67452301), R9D,
        0x67, 0x45, 0x8B, 0x8C, 0x24, 0x01, 0x23, 0x45, 0x67,
        "MOV [R12D]+disp32 -> R9D");

    errors += test_mov_case_8(
        R9D, DWPTR64_EX(SP, 0x67452301),
        0x44, 0x89, 0x8C, 0x24, 0x01, 0x23, 0x45, 0x67,
        "MOV R9D -> [RSP]+disp32");
    errors += test_mov_case_8(
        DWPTR64_EX(SP, 0x67452301), R9D,
        0x44, 0x8B, 0x8C, 0x24, 0x01, 0x23, 0x45, 0x67,
        "MOV [RSP]+disp32 -> R9D");

    // 64-bit operands
    errors += test_mov_case_4(
        R14, QWPTR64(SP),
        0x4C, 0x89, 0x34, 0x24,
        "MOV R14 -> [RSP]");
    errors += test_mov_case_4(
        QWPTR64(SP), R14,
        0x4C, 0x8B, 0x34, 0x24,
        "MOV R14 -> [RSP]");

    return errors;
}

static int test_mov_reg_offset_small(void) {

    // Here we test MOV between a value at a 32-bit memory offset and different
    // registers.

    int errors = 0;

    errors += test_mov_case_7(
        AL, BOFFSET32(0x67452301),
        0x88, 0x04, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV AL -> [offset32]");
    errors += test_mov_case_7(
        BOFFSET32(0x67452301), AL,
        0x8A, 0x04, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV [offset32] -> AL");

    errors += test_mov_case_8(
        SPL, BOFFSET32(0x67452301),
        0x40, 0x88, 0x24, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV SPL -> [offset32]");
    errors += test_mov_case_8(
        BOFFSET32(0x67452301), SPL,
        0x40, 0x8A, 0x24, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV [offset32] -> SPL");

    errors += test_mov_case_8(
        BX, WOFFSET32(0x67452301),
        0x66, 0x89, 0x1C, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV BX -> [offset32]");
    errors += test_mov_case_8(
        WOFFSET32(0x67452301), BX,
        0x66, 0x8B, 0x1C, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV [offset32] -> BX");

    errors += test_mov_case_8(
        R9D, DWOFFSET32(0x67452301),
        0x44, 0x89, 0x0C, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV R9D -> [offset32]");
    errors += test_mov_case_8(
        DWOFFSET32(0x67452301), R9D,
        0x44, 0x8B, 0x0C, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV [offset32] -> R9D");

    errors += test_mov_case_8(
        R14, QWOFFSET32(0x67452301),
        0x4C, 0x89, 0x34, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV R14 -> [offset32]");
    errors += test_mov_case_8(
        QWOFFSET32(0x67452301), R14,
        0x4C, 0x8B, 0x34, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOV [offset32] -> R14");

    return errors;
}

static int test_mov_reg_offset_big(void) {

    // Here we test MOV between rAX and an arbitrary memory location given by a
    // 64-bit literal address.

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // Check illegal arguments

    amd64_enc_mov(&ec, BL, BOFFSET64(0xEFCDAB8967452301));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV from a 64-bit address location to non rAX register");

    amd64_enc_mov(&ec, BOFFSET64(0xEFCDAB8967452301), BL);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV from a non rAX register to a 64-bit address location");

    amd64_enc_mov(&ec, R15, QWOFFSET64(0xEFCDAB8967452301));
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV from a 64-bit address location to non rAX register");

    amd64_enc_mov(&ec, QWOFFSET64(0xEFCDAB8967452301), R15);
    errors += expect_failure(
        &ec,
        "Shouldn't allow MOV from a non rAX register to a 64-bit address location");

    // rAX -> moffset
    //
    // Here we test the capability of direct storing data in memory.

    errors += test_mov_case_9(
        AL, BOFFSET64(0xEFCDAB8967452301),
        0xA2, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV AL -> 64-bit address");

    errors += test_mov_case_10(
        AX, WOFFSET64(0xEFCDAB8967452301),
        0x66, 0xA3, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV AX -> 64-bit address");

    errors += test_mov_case_9(
        EAX, DWOFFSET64(0xEFCDAB8967452301),
        0xA3, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV EAX -> 64-bit address");

    errors += test_mov_case_10(
        RAX, QWOFFSET64(0xEFCDAB8967452301),
        0x48, 0xA3, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV RAX -> 64-bit address");

    // moffset -> rAX
    //
    // Here we test the capability of direct loding data from memory.

    errors += test_mov_case_9(
        BOFFSET64(0xEFCDAB8967452301), AL,
        0xA0, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV 64-bit address -> AL");

    errors += test_mov_case_10(
        WOFFSET64(0xEFCDAB8967452301), AX,
        0x66, 0xA1, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV 64-bit address -> AX");

    errors += test_mov_case_9(
        DWOFFSET64(0xEFCDAB8967452301), EAX,
        0xA1, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV 64-bit address -> EAX");

    errors += test_mov_case_10(
        QWOFFSET64(0xEFCDAB8967452301), RAX,
        0x48, 0xA1, 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
        "Failed generating MOV 64-bit address -> RAX");

    return errors;
}

static int test_mov_reg_rip_offset(void) {

    // Here we test MOV between a RIP-based offset location and different
    // registers.

    int errors = 0;

    errors += test_mov_case_6(
        AL, BRIPOFF(0x67452301),
        0x88, 0x05, 0x01, 0x23, 0x45, 0x67,
        "MOV AL -> [RIP+offset32]");
    errors += test_mov_case_6(
        BRIPOFF(0x67452301), AL,
        0x8A, 0x05, 0x01, 0x23, 0x45, 0x67,
        "MOV [RIP+offset32] -> AL");

    errors += test_mov_case_7(
        BPL, BRIPOFF(0x67452301),
        0x40, 0x88, 0x2D, 0x01, 0x23, 0x45, 0x67,
        "MOV BPL -> [RIP+offset32]");
    errors += test_mov_case_7(
        BRIPOFF(0x67452301), BPL,
        0x40, 0x8A, 0x2D, 0x01, 0x23, 0x45, 0x67,
        "MOV [RIP+offset32] -> BPL");

    errors += test_mov_case_7(
        BX, WRIPOFF(-0x01234567),
        0x66, 0x89, 0x1D, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV BX -> [RIP-offset32]");
    errors += test_mov_case_7(
        WRIPOFF(-0x01234567), BX,
        0x66, 0x8B, 0x1D, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV [RIP-offset32] -> BX");

    errors += test_mov_case_7(
        R9D, DWRIPOFF(0x67452301),
        0x44, 0x89, 0x0D, 0x01, 0x23, 0x45, 0x67,
        "MOV R9D -> [RIP+offset32]");
    errors += test_mov_case_7(
        DWRIPOFF(0x67452301), R9D,
        0x44, 0x8B, 0x0D, 0x01, 0x23, 0x45, 0x67,
        "MOV [RIP+offset32] -> R9D");

    errors += test_mov_case_7(
        R14, QWRIPOFF(-0x01234567),
        0x4C, 0x89, 0x35, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV R14 -> [RIP-offset32]");
    errors += test_mov_case_7(
        QWRIPOFF(-0x01234567), R14,
        0x4C, 0x8B, 0x35, 0x99, 0xBA, 0xDC, 0xFE,
        "MOV [RIP-offset32] -> R14");

    return errors;
}

static int test_mov_reg_sib(void) {

    int errors = 0;

    // 8-bit operands

    errors += test_mov_case_8(
        AH, BSIB32(1, BX, CX, 0x67452301),
        0x67, 0x88, 0xA4, 0x19, 0x01, 0x23, 0x45, 0x67,
        "MOV AH -> [ECX+EBX*1+disp32]");
    errors += test_mov_case_7(
        BSIB64(1, BX, CX, 0x67452301), AH,
        0x8A, 0xA4, 0x19, 0x01, 0x23, 0x45, 0x67,
        "MOV [RCX+RBX*1+disp32] -> AH");

    errors += test_mov_case_8(
        AL, BSIB64(1, AX, 15, 0x67452301),
        0x41, 0x88, 0x84, 0x07, 0x01, 0x23, 0x45, 0x67,
        "MOV AL -> [R15+RAX*1+disp32]");
    errors += test_mov_case_8(
        BSIB64(1, AX, 15, 0x67452301), AL,
        0x41, 0x8A, 0x84, 0x07, 0x01, 0x23, 0x45, 0x67,
        "MOV [R15+RAX*1+disp32] -> AL");

    errors += test_mov_case_5(
        AL, BSIB64(4, BP, 10, 0x01),
        0x41, 0x88, 0x44, 0xAA, 0x01,
        "MOV AL -> [R10+RBP*4+disp8]");
    errors += test_mov_case_5(
        BSIB64(4, BP, 10, 0x01), AL,
        0x41, 0x8A, 0x44, 0xAA, 0x01,
        "MOV [R10+RBP*4+disp8] -> AL");

    errors += test_mov_case_8(
        SIL, BSIB64(1, AX, 15, 0x67452301),
        0x41, 0x88, 0xB4, 0x07, 0x01, 0x23, 0x45, 0x67,
        "MOV SIL -> [R15+RAX*1+disp32]");
    errors += test_mov_case_8(
        BSIB64(1, AX, 15, 0x67452301), SIL,
        0x41, 0x8A, 0xB4, 0x07, 0x01, 0x23, 0x45, 0x67,
        "MOV [R15+RAX*1+disp32] -> SIL");

    // 16-bit operands

    errors += test_mov_case_6(
        CX, WSIB64(1, BX, 14, 0x01),
        0x66, 0x41, 0x89, 0x4C, 0x1E, 0x01,
        "MOV CX -> [R14+RBX*1+disp8]");
    errors += test_mov_case_6(
        WSIB64(1, BX, 14, 0x01), CX,
        0x66, 0x41, 0x8B, 0x4C, 0x1E, 0x01,
        "MOV [R14+RBX*1+disp8] -> CX");

    errors += test_mov_case_5(
        CX, WSIB64(4, SI, 9, 0),
        0x66, 0x41, 0x89, 0x0C, 0xB1,
        "MOV CX -> [R9+RSI*4]");
    errors += test_mov_case_5(
        WSIB64(4, SI, 9, 0), CX,
        0x66, 0x41, 0x8B, 0x0C, 0xB1,
        "MOV [R9+RSI*4] -> CX");

    // 32-bit operands

    errors += test_mov_case_5(
        R10D, DWSIB32(2, CX, 12, 0),
        0x67, 0x45, 0x89, 0x14, 0x4C,
        "MOV R10D -> [R12D+ECX*2]");
    errors += test_mov_case_5(
        DWSIB32(2, CX, 12, 0), R10D,
        0x67, 0x45, 0x8B, 0x14, 0x4C,
        "MOV [R12D+ECX*2] -> R10D");

    errors += test_mov_case_9(
        R10D, DWSIB32(8, DI, 8, 0X67452301),
        0x67, 0x45, 0x89, 0x94, 0xF8, 0x01, 0x23, 0x45, 0x67,
        "MOV R10D -> [R8D+EDI*8+disp32]");
    errors += test_mov_case_9(
        DWSIB32(8, DI, 8, 0X67452301), R10D,
        0x67, 0x45, 0x8B, 0x94, 0xF8, 0x01, 0x23, 0x45, 0x67,
        "MOV [R8D+EDI*8+disp32] -> R10D");

    // 64-bit operands:

    errors += test_mov_case_9(
        R13, QWSIB32(2, DX, 11, 0x67452301),
        0x67, 0x4D, 0x89, 0xAC, 0x53, 0x01, 0x23, 0x45, 0x67,
        "MOV R13 -> [R11D+EDX*2+disp32]");
    errors += test_mov_case_9(
        QWSIB32(2, DX, 11, 0x67452301), R13,
        0x67, 0x4D, 0x8B, 0xAC, 0x53, 0x01, 0x23, 0x45, 0x67,
        "MOV [R11D+EDX*2+disp32] -> R13");


    errors += test_mov_case_6(
        R13, QWSIB32(8, 8, DI, 0x01),
        0x67, 0x4E, 0x89, 0x6C, 0xC7, 0x01,
        "MOV R13 -> [EDI+R8D*8+disp8]");
    errors += test_mov_case_6(
        QWSIB32(8, 8, DI, 0x01), R13,
        0x67, 0x4E, 0x8B, 0x6C, 0xC7, 0x01,
        "MOV [EDI+R8D*8+disp8] -> R13");

    return errors;
}

static int test_mov_reg_rm(void) {
    return
        test_mov_reg_reg_dir_old() +
        test_mov_reg_reg_dir_cross() +
        test_mov_reg_reg_dir_funny() +
        test_mov_reg_reg_indir_default() +
        test_mov_reg_reg_indir_rbp() +
        test_mov_reg_reg_indir_rsp() +
        test_mov_reg_offset_small() +
        test_mov_reg_offset_big() +
        test_mov_reg_rip_offset() +
        test_mov_reg_sib();
}

static int test_mov_reg_imm(void) {

    int errors = 0;

    errors += test_mov_case_2(
        IMM8(0x01), AL,
        0xB0, 0x01,
        "MOV imm8+ -> AL");
    errors += test_mov_case_2(
        IMM8(-0x01), AL,
        0xB0, 0xFF,
        "MOV imm8- -> AL");

    errors += test_mov_case_4(
        IMM16(0x2301), DI,
        0x66, 0xBF, 0x01, 0x23,
        "MOV imm16+ -> DI");
    errors += test_mov_case_4(
        IMM16(-0x2301), DI,
        0x66, 0xBF, 0xFF, 0xDC,
        "MOV imm16- -> DI");

    errors += test_mov_case_6(
        IMM32(0x67452301), R8D,
        0x41, 0xB8, 0x01, 0x23, 0x45, 0x67,
        "MOV imm32+ -> R8D");
    errors += test_mov_case_6(
        IMM32(-0x67452301), R8D,
        0x41, 0xB8, 0xFF, 0xDC, 0xBA, 0x98,
        "MOV imm32- -> R8D");

    errors += test_mov_case_10(
        IMM64(0x0123456789ABCDEF), R15,
        0x49, 0xBF, 0xEF, 0xCD, 0xAB, 0x89, 0x67, 0x45, 0x23, 0x01,
        "MOV imm64+ -> R15");
    errors += test_mov_case_10(
        IMM64(-0x0123456789ABCDEF), R15,
        0x49, 0xBF, 0x11, 0x32, 0x54, 0x76, 0x98, 0xBA, 0xDC, 0xFE,
        "MOV imm64- -> R15");

    // Special case of MOV imm32->r/m64

    errors += test_mov_case_7(
        IMM32(0x67452301), RBP,
        0x48, 0xC7, 0xC5, 0x01, 0x23, 0x45, 0x67,
        "MOV imm32+ -> RBP");
    errors += test_mov_case_7(
        IMM32(-0x67452301), RBP,
        0x48, 0xC7, 0xC5, 0xFF, 0xDC, 0xBA, 0x98,
        "MOV imm32- -> RBP");

    return errors;
}

static int test_mov_rm_imm(void) {

    int errors = 0;

    // 8-bit operands:

    errors += test_mov_case_4(
        IMM8(0x01), BPTR32(BX),
        0x67, 0xC6, 0x03, 0x01,
        "MOV imm8 -> [EBX]");

    errors += test_mov_case_4(
        IMM8(0x01), BPTR64_EX(SI, 0x01),
        0xC6, 0x46, 0x01, 0x01,
        "MOV imm8 -> [RSI]+disp8");

    // 16-bit operands

    errors += test_mov_case_10(
        IMM16(0x2301), WOFFSET32(0x67452301),
        0x66, 0xC7, 0x04, 0x25, 0x01, 0x23, 0x45, 0x67, 0x01, 0x23,
        "MOV imm16 -> [offset32]");

    // 32-bit operands

    errors += test_mov_case_9(
        IMM32(0x67452301), DWSIB32(2, CX, 12, 0),
        0x67, 0x41, 0xC7, 0x04, 0x4C, 0x01, 0x23, 0x45, 0x67,
        "MOV imm32 -> [2*ECX+R12D]");

    // Special case of MOV imm32->r/m64

    errors += test_mov_case_8(
        IMM32(0x67452301), QWPTR64(SP),
        0x48, 0xC7, 0x04, 0x24, 0x01, 0x23, 0x45, 0x67,
        "MOV imm32 -> [RSP]");

    return errors;
}

int test_mov(void) {
    return
        test_mov_invalid() +
        test_mov_reg_rm() +
        test_mov_reg_imm() +
        test_mov_rm_imm();
}

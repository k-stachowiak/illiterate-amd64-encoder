/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The common ADD like instruction encoding
// ========================================

// There is a subset of the AMD 64 instructions that share the entire encoding
// scheme and high level semantics. Therefore their encoding may be implemented
// using a single algorithm, with a few instruction specific parameters.
// They are given in the following structure.

struct AddLikeParameters {
    char *name;
    uint8_t reg_modifier;
    uint8_t imm_a_opcode_8bit;
    uint8_t imm_a_opcode_default;
    uint8_t imm_rm_opcode_8bit;
    uint8_t imm_rm_opcode_default;
    // uint8_t imm8_rm_opcode_8bit; // This is handled by 8-bit imm_rm
    uint8_t imm8_rm_opcode_default;
    uint8_t reg_rm_opcode_8bit;
    uint8_t reg_rm_opcode_default;
    uint8_t rm_reg_opcode_8bit;
    uint8_t rm_reg_opcode_default;
};

static bool enc_add_like(struct AMD64EncContext *ec,
                         struct AMD64Location src,
                         struct AMD64Location dst,
                         struct AddLikeParameters *params) {
    enum AMD64LocationSize size;

    // 1. Assert input:

    // 1.1. Assert operand sizes:
    if (// In general disallow size mismach...
        (src.size != dst.size) &&

        // ...except when the source is an 8-bit immediate...
        !(src.type == AMD64_LT_IMMEDIATE &&
          src.size == AMD64_LS_8_BIT) &&

        // ...and when the source is a 32-bit immediate and destination is a 64-bit r/m
        !(src.type == AMD64_LT_IMMEDIATE &&
          src.size == AMD64_LS_32_BIT &&
          amd64_loc_is_gp_rm(&dst) &&
          dst.size == AMD64_LS_64_BIT)) {
        ec->error_message = "Size mismatch between ADD-LIKE operands";
        return false;
    }
    size = dst.size; // NOTE: we take dst size, as it denotes the basic size.
                      //       There is a case, where src size may be smaller.

    // 1.2. Prevent use of the "high" registers, when the REX prefix is in order:
    if ((rex_prefix_required(&src) ||
         rex_prefix_required(&dst)) &&
        ((src.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(src.gpreg.gpreg)) ||
         (dst.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(dst.gpreg.gpreg)))) {
        ec->error_message = "High register not allowed when REX prefix used";
        return false;
    }

    // 1.3. Prevent use of the "funny" and "high" registers in the same instruction:
    if ((loc_in_funny_register(&src) ||
         loc_in_funny_register(&dst)) &&
        ((src.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(src.gpreg.gpreg)) ||
         (dst.type == AMD64_LT_REGISTER_GP && amd64_gpreg_is_high(dst.gpreg.gpreg)))) {
        ec->error_message = "High register cannot be encoded together with the funny ones";
        return false;
    }

    // 1.4. Prevent use of a 64-bit immediate:
    if (src.type == AMD64_LT_IMMEDIATE && src.size == AMD64_LS_64_BIT) {
        ec->error_message =
            "Cannot perform an ADD-LIKE operation with a 64-bit immediate";
        return false;
    }

    // 2. Main dispatch of encodable cases:
    enc_start(ec);
    if (src.type == AMD64_LT_IMMEDIATE &&
        dst.type == AMD64_LT_REGISTER_GP &&
        dst.gpreg.gpreg == AMD64_GPREG_AX) {

        // 2.a) ADD-LIKE immX -> AX: OP /iX
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, NULL, &dst);
        enc_sized_opcode(ec,
                         size,
                         params->imm_a_opcode_8bit,
                         params->imm_a_opcode_default);
        ec_put_immediate_le(ec, src.size, &src.imm);
        enc_finish(ec);
        return true;

    } else if (src.type == AMD64_LT_IMMEDIATE &&
               amd64_loc_is_gp_rm(&dst) &&
               (src.size == dst.size ||
                (src.size == AMD64_LS_32_BIT && dst.size == AMD64_LS_64_BIT))) {

        // 2.b) ADD-LIKE immX -> r/m: OP' /MOD /iX
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, NULL, &dst);
        enc_sized_opcode(ec,
                         size,
                         params->imm_rm_opcode_8bit,
                         params->imm_rm_opcode_default);
        enc_modrm_1arg(ec, params->reg_modifier, &dst);
        ec_put_immediate_le(ec, src.size, &src.imm);
        enc_finish(ec);
        return true;

    } else if (src.type == AMD64_LT_IMMEDIATE &&
               src.size == AMD64_LS_8_BIT &&
               amd64_loc_is_gp_rm(&dst) &&
               (dst.size == AMD64_LS_16_BIT ||
                dst.size == AMD64_LS_32_BIT ||
                dst.size == AMD64_LS_64_BIT)) {

        // 2.c) ADD-LIKE imm8 -> r/m: OP" /MOD /ib

        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, NULL, &dst);

        // Note, that for the 8-bit case here, we borrow an opcode from another
        // case as for this one, there is no specific one; it's semantically
        // identical.
        enc_sized_opcode(ec,
                         size,
                         params->imm_rm_opcode_8bit,
                         params->imm8_rm_opcode_default);
        enc_modrm_1arg(ec, params->reg_modifier, &dst);
        ec_put_immediate_le(ec, src.size, &src.imm);
        enc_finish(ec);
        return true;

    } else if (src.type == AMD64_LT_REGISTER_GP && amd64_loc_is_gp_rm(&dst)) {

        // 2.d) ADD-LIKE reg -> r/m: OP /r
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &src, &dst);
        enc_sized_opcode(ec,
                         size,
                         params->reg_rm_opcode_8bit,
                         params->reg_rm_opcode_default);
        enc_modrm_2arg(ec, modrm_gpreg(src.gpreg.gpreg), &dst);
        enc_finish(ec);
        return true;

    } else if (amd64_loc_is_gp_rm(&src) && dst.type == AMD64_LT_REGISTER_GP) {

        // 2.e) ADD-LIKE r/m -> reg: OP /r
        enc_prefixes(ec, AMD64_LS_32_BIT, dst.size, false, &dst, &src);
        enc_sized_opcode(ec,
                         size,
                         params->rm_reg_opcode_8bit,
                         params->rm_reg_opcode_default);
        enc_modrm_2arg(ec, modrm_gpreg(dst.gpreg.gpreg), &src);
        enc_finish(ec);
        return true;

    } else {

        // 2.f)
        ec->error_message = "Unhandled operands for MOV";
        return false;

    }
}

// Particular ADD-like instructions encoding API
// =============================================

bool amd64_enc_add(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst) {
    static struct AddLikeParameters alp = {
        .name = "ADD",
        .reg_modifier = 0,
        .imm_a_opcode_8bit      = 0x04,
        .imm_a_opcode_default   = 0x05,
        .imm_rm_opcode_8bit     = 0x80,
        .imm_rm_opcode_default  = 0x81,
        .imm8_rm_opcode_default = 0x83,
        .reg_rm_opcode_8bit     = 0x00,
        .reg_rm_opcode_default  = 0x01,
        .rm_reg_opcode_8bit     = 0x02,
        .rm_reg_opcode_default  = 0x03
    };
    assert(ec);
    return enc_add_like(ec, src, dst, &alp);
}

bool amd64_enc_sub(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst) {
    static struct AddLikeParameters alp = {
        .name = "SUB",
        .reg_modifier = 5,
        .imm_a_opcode_8bit      = 0x2C,
        .imm_a_opcode_default   = 0x2D,
        .imm_rm_opcode_8bit     = 0x80,
        .imm_rm_opcode_default  = 0x81,
        .imm8_rm_opcode_default = 0x83,
        .reg_rm_opcode_8bit     = 0x28,
        .reg_rm_opcode_default  = 0x29,
        .rm_reg_opcode_8bit     = 0x2A,
        .rm_reg_opcode_default  = 0x2B
    };
    assert(ec);
    return enc_add_like(ec, src, dst, &alp);
}

bool amd64_enc_cmp(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst) {
    static struct AddLikeParameters alp = {
        .name = "CMP",
        .reg_modifier = 7,
        .imm_a_opcode_8bit      = 0x3C,
        .imm_a_opcode_default   = 0x3D,
        .imm_rm_opcode_8bit     = 0x80,
        .imm_rm_opcode_default  = 0x81,
        .imm8_rm_opcode_default = 0x83,
        .reg_rm_opcode_8bit     = 0x38,
        .reg_rm_opcode_default  = 0x39,
        .rm_reg_opcode_8bit     = 0x3A,
        .rm_reg_opcode_default  = 0x3B
    };
    assert(ec);
    return enc_add_like(ec, src, dst, &alp);
}

bool amd64_enc_xor(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst) {
    static struct AddLikeParameters alp = {
        .name = "XOR",
        .reg_modifier = 6,
        .imm_a_opcode_8bit      = 0x34,
        .imm_a_opcode_default   = 0x35,
        .imm_rm_opcode_8bit     = 0x80,
        .imm_rm_opcode_default  = 0x81,
        .imm8_rm_opcode_default = 0x83,
        .reg_rm_opcode_8bit     = 0x30,
        .reg_rm_opcode_default  = 0x31,
        .rm_reg_opcode_8bit     = 0x32,
        .rm_reg_opcode_default  = 0x33
    };
    assert(ec);
    return enc_add_like(ec, src, dst, &alp);
}

// Test code
// =========

static int test_add_like_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    enc_add_like(&ec, AL, AX, NULL);
    errors += expect_failure(
        &ec,
        "Should not allow operands of different sizes");

    enc_add_like(&ec, IMM64(0xEFCDAB8967452301), RAX, NULL);
    errors += expect_failure(
        &ec,
        "Should not allow ADD-LIKE from 64-bit immediate");

    enc_add_like(&ec, AL, IMM8(0x01), NULL);
    errors += expect_failure(
        &ec,
        "Should not allow ADD-LIKE to an immediate");

    enc_add_like(&ec, AH, R8B, NULL);
    errors += expect_failure(
        &ec,
        "Should not allow ADD-LIKE with a \"high\" register, when REX prefix needed");

    enc_add_like(&ec, AH, SPL, NULL);
    errors += expect_failure(
        &ec,
        "Should not allow ADD-LIKE with a \"high\" register, and a \"funny\" register");

    return errors;
}

static int test_add_like_imm_a(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_add(&ec, IMM8(0x01), AL);
    errors += expect_success_2(
        &ec, &sis,
        0x04, 0x01,
        "ADD imm8 -> AL");

    amd64_enc_sub(&ec, IMM16(0x2301), AX);
    errors += expect_success_4(
        &ec, &sis,
        0x66, 0x2D, 0x01, 0x23,
        "SUB imm16 -> AX");

    amd64_enc_cmp(&ec, IMM32(0x67452301), EAX);
    errors += expect_success_5(
        &ec, &sis,
        0x3D, 0x01, 0x23, 0x45, 0x67,
        "CMP imm32 -> EAX");

    amd64_enc_xor(&ec, IMM32(0x67452301), RAX);
    errors += expect_success_6(
        &ec, &sis,
        0x48, 0x35, 0x01, 0x23, 0x45, 0x67,
        "XOR imm32 -> RAX");

    return errors;
}

static int test_add_like_imm_reg(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_add(&ec, IMM8(0x01), BH);
    errors += expect_success_3(
        &ec, &sis,
        0x80, 0xC7, 0x01,
        "ADD imm8 -> BH");

    amd64_enc_sub(&ec, IMM8(0x01), BL);
    errors += expect_success_3(
        &ec, &sis,
        0x80, 0xEB, 0x01,
        "SUB imm8 -> BL");

    amd64_enc_sub(&ec, IMM8(0x01), SPL);
    errors += expect_success_4(
        &ec, &sis,
        0x40, 0x80, 0xEC, 0x01,
        "SUB imm8 -> SPL");

    amd64_enc_cmp(&ec, IMM16(0x2301), SI);
    errors += expect_success_5(
        &ec, &sis,
        0x66, 0x81, 0xFE, 0x01, 0x23,
        "CMP imm16 -> SI");

    amd64_enc_xor(&ec, IMM32(0x67452301), R8D);
    errors += expect_success_7(
        &ec, &sis,
        0x41, 0x81, 0xF0, 0x01, 0x23, 0x45, 0x67,
        "XOR imm32 -> R8D");

    amd64_enc_xor(&ec, IMM32(0x67452301), R15);
    errors += expect_success_7(
        &ec, &sis,
        0x49, 0x81, 0xF7, 0x01, 0x23, 0x45, 0x67,
        "XOR imm32 -> R15");

    return errors;
}

static int test_add_like_imm_rm(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // imm8 -> r/mX

    amd64_enc_add(&ec, IMM8(0x01), BPTR64(SP));
    errors += expect_success_4(
        &ec, &sis,
        0x80, 0x04, 0x24, 0x01,
        "ADD imm8 -> [RSP]");

    // immX -> r/mX

    amd64_enc_sub(&ec, IMM8(0x01), BPTR32_EX(8, 0x01));
    errors += expect_success_6(
        &ec, &sis,
        0x67, 0x41, 0x80, 0x68, 0x01, 0x01,
        "SUB imm8 -> [R8D]+disp8");

    amd64_enc_cmp(&ec, IMM16(0x2301), WPTR64_EX(15, 0x67452301));
    errors += expect_success_10(
        &ec, &sis,
        0x66, 0x41, 0x81, 0xBF, 0x01, 0x23, 0x45, 0x67, 0x01, 0x23,
        "CMP imm16 -> [R15]+disp32");

    amd64_enc_xor(&ec, IMM32(0x67452301), DWRIPOFF(0x67452301));
    errors += expect_success_10(
        &ec, &sis,
        0x81, 0x35, 0x01, 0x23, 0x45, 0x67, 0x01, 0x23, 0x45, 0x67,
        "XOR imm32 -> [RIP]+disp32");

    amd64_enc_add(&ec, IMM32(0x67452301), DWSIB32(4, 15, DI, 0x01));
    errors += expect_success_10(
        &ec, &sis,
        0x67, 0x42, 0x81, 0x44, 0xBF, 0x01, 0x01, 0x23, 0x45, 0x67,
        "ADD imm32 -> [4*R15D+EDI]+disp8");

    return errors;
}

static int test_add_like_reg_rm(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_add(&ec, CH, BPTR64(BP));
    errors += expect_success_3(
        &ec, &sis,
        0x00, 0x6D, 0x00,
        "ADD CH -> [RBP]");

    amd64_enc_xor(&ec, DL, BPTR32_EX(9, 0x01));
    errors += expect_success_5(
        &ec, &sis,
        0x67, 0x41, 0x30, 0x51, 0x01,
        "XOR DL -> [R9D]+disp8");

    amd64_enc_xor(&ec, SIL, BPTR32_EX(9, 0x01));
    errors += expect_success_5(
        &ec, &sis,
        0x67, 0x41, 0x30, 0x71, 0x01,
        "XOR SIL -> [R9D]+disp8");

    amd64_enc_cmp(&ec, DI, WPTR64_EX(13, 0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x66, 0x41, 0x39, 0xBD, 0x01, 0x23, 0x45, 0x67,
        "CMP DI -> [R13]+disp32");

    amd64_enc_sub(&ec, R10D, DWRIPOFF(0x67452301));
    errors += expect_success_7(
        &ec, &sis,
        0x44, 0x29, 0x15, 0x01, 0x23, 0x45, 0x67,
        "SUB r10d -> [RIP]+disp32");

    amd64_enc_add(&ec, R14, QWSIB32(8, 11, AX, 0x01));
    errors += expect_success_6(
        &ec, &sis,
        0x67, 0x4E, 0x01, 0x74, 0xD8, 0x01,
        "ADD r14 -> [8*R15D+EAX]+disp8");

    return errors;
}

int test_add_like(void) {
    return
        test_add_like_invalid() +
        test_add_like_imm_a() +
        test_add_like_imm_reg() +
        test_add_like_imm_rm() +
        test_add_like_reg_rm();
}

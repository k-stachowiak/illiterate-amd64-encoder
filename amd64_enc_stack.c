/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The PUSH instruction
// ====================

bool amd64_enc_push(struct AMD64EncContext *ec, struct AMD64Location x) {

    assert(ec);

    if ((x.size == AMD64_LS_8_BIT || x.size == AMD64_LS_32_BIT) &&
        x.type != AMD64_LT_IMMEDIATE) {
        ec->error_message = "Only 16- and 16-bit r/m operands allowed for PUSH in 64-bit mode";
        return false;
    }

    if (x.size == AMD64_LS_64_BIT && x.type == AMD64_LT_IMMEDIATE) {
        ec->error_message = "Cannog PUSH a 64-bit immediate";
        return false;
    }

    enc_start(ec);

    if (x.type == AMD64_LT_IMMEDIATE) {

        // PUSH immX: 0x6A/0x68 immX
        enc_prefixes(ec, AMD64_LS_64_BIT, x.size, false, NULL, NULL);
        if (x.size == AMD64_LS_8_BIT)
            ec_put(ec, 0x6A);
        else
            ec_put(ec, 0x68);
        ec_put_immediate_le(ec, x.size, &x.imm);

    } else if (x.type == AMD64_LT_REGISTER_GP) {
        // PUSH reg: 0x50 +rX
        enc_prefixes(ec, AMD64_LS_64_BIT, x.size, true, &x, NULL);
        enc_incr_sized_opcode(ec, x.size, 0x00, 0x50, x.gpreg.gpreg);

    } else {
        // PUSH r/m: 0xFF /6
        enc_prefixes(ec, AMD64_LS_64_BIT, x.size, false, NULL, &x);
        ec_put(ec, 0xFF);
        enc_modrm_1arg(ec, 6, &x);

    }

    enc_finish(ec);
    return true;
}

// The POP instruction
// ===================

bool amd64_enc_pop(struct AMD64EncContext *ec, struct AMD64Location x) {

    if (!amd64_loc_is_gp_rm(&x)) {
        ec->error_message = "Only general purpose r/m operands allowed for POP";
        return false;
    }

    if (x.size != AMD64_LS_16_BIT && x.size != AMD64_LS_64_BIT) {
        ec->error_message = "Only 16- and 64-bit operands allowed for POP in 64-bit mode";
        return false;
    }

    enc_start(ec);

    if (x.type == AMD64_LT_REGISTER_GP) {
        // POP reg: 0x58 +rX
        enc_prefixes(ec, AMD64_LS_64_BIT, x.size, true, &x, NULL);
        enc_incr_sized_opcode(ec, x.size, 0x00, 0x58, x.gpreg.gpreg);

    } else {
        // POP r/m: 0x8F /0
        enc_prefixes(ec, AMD64_LS_64_BIT, x.size, false, NULL, &x);
        ec_put(ec, 0x8F);
        enc_modrm_1arg(ec, 0, &x);

    }

    enc_finish(ec);
    return true;
}

// Test code
// =========

// Specific suites
// ---------------

static int test_push_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_push(&ec, AH);
    errors += expect_failure(&ec, "PUSH should not take a 8-bit r/m operand");

    amd64_enc_push(&ec, AL);
    errors += expect_failure(&ec, "PUSH should not take a 8-bit r/m operand");

    amd64_enc_push(&ec, EAX);
    errors += expect_failure(&ec, "PUSH should not take a 32-bit r/m operand");

    amd64_enc_push(&ec, IMM64(0));
    errors += expect_failure(&ec, "PUSH should not take a 64-bit immediate");

    return errors;
}

static int test_push_valid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // PUSH imm

    amd64_enc_push(&ec, IMM8(0x01));
    errors += expect_success_2(&ec, &sis, 0x6A, 0x01, "PUSH imm8");

    amd64_enc_push(&ec, IMM16(0x2301));
    errors += expect_success_4(&ec, &sis, 0x66, 0x68, 0x01, 0x23, "PUSH imm16");

    amd64_enc_push(&ec, IMM32(0x67452301));
    errors += expect_success_5(&ec, &sis, 0x68, 0x01, 0x23, 0x45, 0x67, "PUSH imm32");

    // PUSH reg

    amd64_enc_push(&ec, AX);
    errors += expect_success_2(&ec, &sis, 0x66, 0x50, "PUSH AX");

    amd64_enc_push(&ec, R8W);
    errors += expect_success_3(&ec, &sis, 0x66, 0x41, 0x50, "PUSH R8W");

    amd64_enc_push(&ec, R15);
    errors += expect_success_2(&ec, &sis, 0x41, 0x57, "PUSH R15");

    // PUSH mod/rm

    amd64_enc_push(&ec, WOFFSET32(0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x66, 0xFF, 0x34, 0x25, 0x01, 0x23, 0x45, 0x67,
        "PUSH (offset32)");

    amd64_enc_push(&ec, WPTR32(DI));
    errors += expect_success_4(
        &ec, &sis,
        0x67, 0x66, 0xFF, 0x37,
        "PUSH (EDI)");

    amd64_enc_push(&ec, QWPTR64_EX(SI, 0x01));
    errors += expect_success_3(
        &ec, &sis,
        0xFF, 0x76, 0x01,
        "PUSH disp8(RSI)");

    amd64_enc_push(&ec, QWSIB32(2, CX, 12, 0X67452301));
    errors += expect_success_9(
        &ec, &sis,
        0x67, 0x41, 0xFF, 0xB4, 0x4C, 0x01, 0x23, 0x45, 0x67,
        "PUSH disp32(2*ECX+R12D)");

    return errors;
}

static int test_pop_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_pop(&ec, AH);
    errors += expect_failure(&ec, "POP should not take a 8-bit r/m operand");

    amd64_enc_pop(&ec, AL);
    errors += expect_failure(&ec, "POP should not take a 8-bit r/m operand");

    amd64_enc_pop(&ec, EAX);
    errors += expect_failure(&ec, "POP should not take a 32-bit r/m operand");

    amd64_enc_pop(&ec, IMM8(0));
    errors += expect_failure(&ec, "POP should not take an immediate");

    amd64_enc_pop(&ec, IMM16(0));
    errors += expect_failure(&ec, "POP should not take an immediate");

    amd64_enc_pop(&ec, IMM32(0));
    errors += expect_failure(&ec, "POP should not take an immediate");

    amd64_enc_pop(&ec, IMM64(0));
    errors += expect_failure(&ec, "POP should not take an immediate");

    return errors;
}

static int test_pop_valid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // POP reg:

    amd64_enc_pop(&ec, BX);
    errors += expect_success_2(&ec, &sis, 0x66, 0x5B, "POP BX");

    amd64_enc_pop(&ec, R9W);
    errors += expect_success_3(&ec, &sis, 0x66, 0x41, 0x59, "POP R9W");

    amd64_enc_pop(&ec, R14);
    errors += expect_success_2(&ec, &sis, 0x41, 0x5E, "POP R14");

    // POP r/m:

    amd64_enc_pop(&ec, WOFFSET32(0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x66, 0x8F, 0x04, 0x25, 0x01, 0x23, 0x45, 0x67,
        "POP (offset32)");

    amd64_enc_pop(&ec, WPTR32(SI));
    errors += expect_success_4(
        &ec, &sis,
        0x67, 0x66, 0x8F, 0x06,
        "POP (ESI)");

    amd64_enc_pop(&ec, QWPTR64_EX(DI, 0x01));
    errors += expect_success_3(
        &ec, &sis,
        0x8F, 0x47, 0x01,
        "POP offset8(RDI)");

    amd64_enc_pop(&ec, QWSIB32(4, AX, 14, 0x67452301));
    errors += expect_success_9(
        &ec, &sis,
        0x67, 0x41, 0x8F, 0x84, 0x86, 0x01, 0x23, 0x45, 0x67,
        "POP disp32(2*EAX+R14D)");

    return errors;
}

// Public API
// ----------

int test_push(void) {
    return
        test_push_invalid() +
        test_push_valid();
}

int test_pop(void) {
    return
        test_pop_invalid() +
        test_pop_valid();
}

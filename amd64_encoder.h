/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMD64_ENCODER_H
#define AMD64_ENCODER_H

#include <stdbool.h>
#include <stdint.h>

// Architecture representation
// ===========================

// General purpose registers
// -------------------------

enum AMD64GPRegister {
    AMD64_GPREG_AH,
    AMD64_GPREG_BH,
    AMD64_GPREG_CH,
    AMD64_GPREG_DH,

    AMD64_GPREG_AX,
    AMD64_GPREG_BX,
    AMD64_GPREG_CX,
    AMD64_GPREG_DX,
    AMD64_GPREG_SP,
    AMD64_GPREG_BP,
    AMD64_GPREG_SI,
    AMD64_GPREG_DI,

    AMD64_GPREG_8,
    AMD64_GPREG_9,
    AMD64_GPREG_10,
    AMD64_GPREG_11,
    AMD64_GPREG_12,
    AMD64_GPREG_13,
    AMD64_GPREG_14,
    AMD64_GPREG_15,
};

// SSE Registers
// -------------

enum AMD64SSERegister {
    AMD64_SSEREG_XMM0,
    AMD64_SSEREG_XMM1,
    AMD64_SSEREG_XMM2,
    AMD64_SSEREG_XMM3,
    AMD64_SSEREG_XMM4,
    AMD64_SSEREG_XMM5,
    AMD64_SSEREG_XMM6,
    AMD64_SSEREG_XMM7
};

// Relative offset for jumps
// -------------------------
//
// The way we define offsets for

enum AMD64RelOffsetSize {
    AMD64_ROS_8,
    AMD64_ROS_32
};

struct AMD64RelOffset {
    enum AMD64RelOffsetSize size;
    union {
        int8_t value8;
        int32_t value32;
    };
};

// Commonly used displacement definition
// -------------------------------------
//
// This can be applied to certain locations. E.g. when using register in an
// indirect context like [RAX] (meaning at the address stored in RAX) instead
// of just RAX, we can optionally apply an offset to it, turning it into:
// [RAX]+displacement

struct AMD64Displacement {
    int32_t value;
};

struct AMD64Displacement amd64_displacement_make(int32_t value);

// Immediate location
// ------------------
//
// This doesn't mean a physical location, but rather data put directly in the
// program code. Certain instructions allow us to glue literal values to them
// rather than only looking for operands in registers or memory.

struct AMD64ImmediateLocation {
    int64_t value;
};

// Direct register location
// ------------------------
//
// This expresses data residing directly in a register.
// The definition of the direct register location is straightforward.

struct AMD64GPRegisterLocation {
    enum AMD64GPRegister gpreg;
};

// SSE register location
// ---------------------

struct AMD64SSERegisterLocation {
    enum AMD64SSERegister ssereg;
};

// Indirect register location
// --------------------------

// The inidrect register based location is composed of a few things. First,
// we point to the register holding the address. Since only a handful of the
// registers can be used in this context, we define a separate enumeration
// rather than reuse the common one.

enum AMD64IndirectRegister {
    AMD64_IREG_AX,
    AMD64_IREG_BX,
    AMD64_IREG_CX,
    AMD64_IREG_DX,
    AMD64_IREG_SP,
    AMD64_IREG_BP,
    AMD64_IREG_SI,
    AMD64_IREG_DI,

    AMD64_IREG_8,
    AMD64_IREG_9,
    AMD64_IREG_10,
    AMD64_IREG_11,
    AMD64_IREG_12,
    AMD64_IREG_13,
    AMD64_IREG_14,
    AMD64_IREG_15
};

// It is possible to only take the lower 32 bits of a register as the address
// base rather than the default 64.

enum AMD64IndirectionAddressSize {
    AMD64_IAS_32,
    AMD64_IAS_64
};

// The structure defining the indirect addressing also contains a displacement
// field. If non zero, it will be encoded together with the accompanying
// instruction.

struct AMD64IndirectRegisterLocation {
    enum AMD64IndirectRegister ireg;
    enum AMD64IndirectionAddressSize addr_size;
    struct AMD64Displacement disp;
};

// Offset based memory locations
// -----------------------------

struct AMD64SmallOffsetLocation {
    uint32_t offset;
};

struct AMD64BigOffsetLocation {
    uint64_t offset;
};

struct AMD64RipOffsetLocation {
    int32_t offset;
};

struct AMD64RipOffsetRecord {
    int offset;
    int size;
    int base;
};

struct AMD64DeferredRipOffsetLocation {
    struct AMD64RipOffsetRecord *rec;
};

// SIB Memory location
// -------------------
//
// This case enables encoding a memory location other than the indirect register
// location. There are two variants: raw and SIB based.

enum AMD64SIBScale {
    AMD64_SS_1,
    AMD64_SS_2,
    AMD64_SS_4,
    AMD64_SS_8
};

enum AMD64SIBRegister {
    AMD64_SREG_AX,
    AMD64_SREG_BX,
    AMD64_SREG_CX,
    AMD64_SREG_DX,
    AMD64_SREG_SP,
    AMD64_SREG_BP,
    AMD64_SREG_SI,
    AMD64_SREG_DI,

    AMD64_SREG_8,
    AMD64_SREG_9,
    AMD64_SREG_10,
    AMD64_SREG_11,
    AMD64_SREG_12,
    AMD64_SREG_13,
    AMD64_SREG_14,
    AMD64_SREG_15,
};

struct AMD64SIBMemoryLocation {
    enum AMD64SIBScale scale;
    enum AMD64SIBRegister index;
    enum AMD64SIBRegister base;
    enum AMD64IndirectionAddressSize addr_size;
    struct AMD64Displacement disp;
};

// Core location definition
// ------------------------

enum AMD64LocationType {
    AMD64_LT_IMMEDIATE,
    AMD64_LT_REGISTER_GP,
    AMD64_LT_REGISTER_SSE,
    AMD64_LT_REGISTER_INDIRECT,
    AMD64_LT_MEMORY_OFFSET_SMALL,
    AMD64_LT_MEMORY_OFFSET_BIG,
    AMD64_LT_MEMORY_RIP_OFFSET,
    AMD64_LT_MEMORY_SIB,
    AMD64_LT_DEFERRED_MEMORY_RIP_OFFSET
};

enum AMD64LocationSize {
    AMD64_LS_8_BIT,
    AMD64_LS_16_BIT,
    AMD64_LS_32_BIT,
    AMD64_LS_64_BIT
};

struct AMD64Location {
    enum AMD64LocationType type;
    enum AMD64LocationSize size;
    union {
        struct AMD64ImmediateLocation imm;
        struct AMD64GPRegisterLocation gpreg;
        struct AMD64SSERegisterLocation ssereg;
        struct AMD64IndirectRegisterLocation ireg;
        struct AMD64SmallOffsetLocation moffset_small;
        struct AMD64BigOffsetLocation moffset_big;
        struct AMD64RipOffsetLocation mrip;
        struct AMD64SIBMemoryLocation msib;
        struct AMD64DeferredRipOffsetLocation def_mrip;
    };
};

// Location constructors
// ---------------------

bool amd64_loc_try_make_register_gp(enum AMD64LocationSize size,
                                    enum AMD64GPRegister gpreg,
                                    struct AMD64Location *result);

bool amd64_loc_try_make_memory_sib(enum AMD64LocationSize size,
                                   enum AMD64SIBScale scale,
                                   enum AMD64SIBRegister index,
                                   enum AMD64SIBRegister base,
                                   enum AMD64IndirectionAddressSize ias,
                                   struct AMD64Displacement disp,
                                   struct AMD64Location *result);

struct AMD64Location amd64_loc_make_immediate(enum AMD64LocationSize loc_size,
                                              int64_t value);

struct AMD64Location amd64_loc_make_register_gp(enum AMD64LocationSize size,
                                                enum AMD64GPRegister gpreg);

struct AMD64Location amd64_loc_make_register_sse(enum AMD64SSERegister ssereg);

struct AMD64Location amd64_loc_make_register_indirect(
        enum AMD64LocationSize size,
        enum AMD64IndirectRegister ireg,
        enum AMD64IndirectionAddressSize ias,
        struct AMD64Displacement disp);

struct AMD64Location amd64_loc_make_memory_offset_small(enum AMD64LocationSize size,
                                                        int32_t offset);

struct AMD64Location amd64_loc_make_memory_offset_big(enum AMD64LocationSize size,
                                                      int64_t offset);

struct AMD64Location amd64_loc_make_memory_rip_offset(enum AMD64LocationSize size,
                                                      int32_t offset);

struct AMD64Location amd64_loc_make_memory_sib(enum AMD64LocationSize size,
                                               enum AMD64SIBScale scale,
                                               enum AMD64SIBRegister index,
                                               enum AMD64SIBRegister base,
                                               enum AMD64IndirectionAddressSize ias,
                                               struct AMD64Displacement disp);

struct AMD64Location amd64_loc_make_deferred_memory_rip_offset(
        enum AMD64LocationSize size,
        struct AMD64RipOffsetRecord *loc_rec);

// Location operations
// -------------------

bool amd64_loc_equal(struct AMD64Location *x, struct AMD64Location *y);
void amd64_loc_swap(struct AMD64Location *x, struct AMD64Location *y);

// Location compatibility
// ----------------------

enum AMD64LocationSize amd64_loc_size_from_bytes(uint8_t bytes);
bool amd64_try_conv_gpreg_to_ireg(enum AMD64GPRegister gpreg,
                                  enum AMD64IndirectRegister *ireg);

// Location predicates
// -------------------

bool loc_in_funny_register(struct AMD64Location *loc);

bool amd64_gpreg_is_high(enum AMD64GPRegister gpreg);
bool amd64_gpreg_is_extended(enum AMD64GPRegister gpreg);
bool amd64_disp_is_byte(struct AMD64Displacement *disp);
bool amd64_ireg_is_extended(enum AMD64IndirectRegister ireg);
bool amd64_sreg_is_extended(enum AMD64SIBRegister sreg);
bool amd64_loc_is_memory(struct AMD64Location *loc);
bool amd64_loc_is_gp_rm(struct AMD64Location *loc);
bool amd64_loc_is_sse_rm(struct AMD64Location *loc);

// Encoding API
// ============

#define AMD64_MAX_INSTRUCTION_SIZE 15

struct AMD64SingleInstrSink {
    uint8_t buffer[AMD64_MAX_INSTRUCTION_SIZE];
    int bufsize;
};

void amd64_sis_init(struct AMD64SingleInstrSink *sis);
void amd64_sis_reset(struct AMD64SingleInstrSink *sis);

struct AMD64AddressEntry {
    int offset;
    int size;
};

struct AMD64EncContext {
    char *error_message;
    void *sink_ctx;
    int  (*sink_offset)(void *);
    void (*sink_put)(void *, uint8_t);
    struct AMD64AddressEntry last_address_entry;
    int *base_set_request;
};

void amd64_ec_init_generic(struct AMD64EncContext *ec,
                           void *ctx,
                           int (*offset_func)(void *),
                           void (*put_func)(void *, uint8_t));

void amd64_ec_init_single_instr(struct AMD64EncContext *ec,
                                struct AMD64SingleInstrSink *sis);

bool amd64_enc_add(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_sub(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_cmp(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_xor(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_comisd(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_idiv(struct AMD64EncContext *aec, struct AMD64Location divisor);
bool amd64_enc_imul(struct AMD64EncContext *aec, int argc, ...);
bool amd64_enc_addsd(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_subsd(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_mulsd(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_divsd(struct AMD64EncContext *ec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_inc(struct AMD64EncContext *aec, struct AMD64Location x);
bool amd64_enc_jnae(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jnb(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_je(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jne(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jna(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jnbe(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jl(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jnl(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jng(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jg(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jmp_rel(struct AMD64EncContext *aec, struct AMD64RelOffset offset);
bool amd64_enc_jmp_rm(struct AMD64EncContext *aec, struct AMD64Location x);
bool amd64_enc_lea(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_mov(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_movd(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_movzx(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_movsd(struct AMD64EncContext *aec, struct AMD64Location src, struct AMD64Location dst);
bool amd64_enc_neg(struct AMD64EncContext *aec, struct AMD64Location x);
bool amd64_enc_push(struct AMD64EncContext *aec, struct AMD64Location x);
bool amd64_enc_pop(struct AMD64EncContext *aec, struct AMD64Location x);
bool amd64_enc_ret(struct AMD64EncContext *aec);
bool amd64_enc_ret_pop(struct AMD64EncContext *aec, uint16_t pop);
bool amd64_enc_syscall(struct AMD64EncContext *aec);

// Testing API
// ===========

int amd64_enc_test(void);

#endif

/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

// The common COMISD like instruction encoding
// ===========================================

bool amd64_enc_comisd(struct AMD64EncContext *ec,
                      struct AMD64Location src,
                      struct AMD64Location dst) {

    // Assert operand sizes:
    if (src.size != dst.size || src.size != AMD64_LS_64_BIT) {
        ec->error_message = "Both operands of COMISD must be 64-bit locations";
        return false;
    }

    if (amd64_loc_is_sse_rm(&src) &&
        dst.type == AMD64_LT_REGISTER_SSE) {

        // ADDSD-like xmm2/mem64 -> xmm1: F2 0F <opcode> /r

        enc_start(ec);

        if ((src.type == AMD64_LT_REGISTER_INDIRECT &&
             src.ireg.addr_size == AMD64_IAS_32) ||
            (src.type == AMD64_LT_MEMORY_SIB &&
             src.msib.addr_size == AMD64_IAS_32))
            ec_put(ec, 0x67);

        ec_put(ec, 0x66);
        enc_rex_prefix(ec, AMD64_LS_64_BIT, AMD64_LS_64_BIT, false, &dst, &src);
        ec_put(ec, 0x0F);
        ec_put(ec, 0x2F);
        enc_modrm_2arg(ec, modrm_ssereg(dst.ssereg.ssereg), &src);
        enc_finish(ec);
        return true;

    } else {
        ec->error_message = "Invalid operands for this instruction";
        return false;

    }
}

// Test code
// =========

static int test_comisd_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_addsd(&ec, AH, XMM0);
    errors += expect_failure(&ec, "Disallow non-64-bit operands");

    amd64_enc_subsd(&ec, RDI, XMM0);
    errors += expect_failure(&ec, "Disallow non-XMM register");

    amd64_enc_divsd(&ec, QWOFFSET64(0x67452301), QWSIB32(2, DX, 11, 0));
    errors += expect_failure(&ec, "Disallow memory-memory operands");

    return errors;
}

static int test_comisd_valid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_comisd(&ec, XMM1, XMM6);
    errors += expect_success_4(
        &ec, &sis,
        0x66, 0x0F, 0x2F, 0xF1,
        "COMISD XMM1 -> XMM6");

    amd64_enc_comisd(&ec, QWOFFSET32(0x67452301), XMM1);
    errors += expect_success_9(
        &ec, &sis,
        0x66, 0x0F, 0x2F, 0x0C, 0x25, 0x01, 0x23, 0x45, 0x67,
        "COMISD (mem32) -> XMM1");

    amd64_enc_comisd(&ec, QWPTR64(BP), XMM6);
    errors += expect_success_5(
        &ec, &sis,
        0x66, 0x0F, 0x2F, 0x75, 0x00,
        "COMISD (RBP) -> XMM6");

    amd64_enc_comisd(&ec, QWPTR32(13), XMM1);
    errors += expect_success_7(
        &ec, &sis,
        0x67, 0x66, 0x41, 0x0F, 0x2F, 0x4d, 0x00,
        "COMISD (R13D) -> XMM1");

    amd64_enc_comisd(&ec, QWSIB32(1, CX, 10, 0), XMM6);
    errors += expect_success_7(
        &ec, &sis,
        0x67, 0x66, 0x41, 0x0F, 0x2F, 0x34, 0x0A,
        "COMISD (1*ECX+R10D) -> XMM6");

    return errors;
}

int test_comisd(void) {
    return
        test_comisd_invalid() +
        test_comisd_valid();
}

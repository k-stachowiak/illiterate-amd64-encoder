/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The conditional jumps
// =====================

static bool enc_jump_common(struct AMD64EncContext *ec,
                            uint8_t opcode_8bit,
                            uint8_t opcode_32bit_hi,
                            struct AMD64RelOffset offset) {
    assert(ec);

    enc_start(ec);
    if (offset.size == AMD64_ROS_8) {
        ec_put(ec, opcode_8bit);

    } else {
        ec_put(ec, 0x0F);
        ec_put(ec, opcode_32bit_hi);
    }
    ec_put_rel_offest_le(ec, &offset);
    enc_finish(ec);
    return true;
}

bool amd64_enc_jnae(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x72, 0x82, offset);
}

bool amd64_enc_jnb(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x73, 0x83, offset);
}

bool amd64_enc_je(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x74, 0x84, offset);
}

bool amd64_enc_jne(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x75, 0x85, offset);
}

bool amd64_enc_jna(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x76, 0x86, offset);
}

bool amd64_enc_jnbe(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x77, 0x87, offset);
}

bool amd64_enc_jl(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x7C, 0x8C, offset);
}

bool amd64_enc_jnl(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x7D, 0x8D, offset);
}

bool amd64_enc_jng(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x7E, 0x8E, offset);
}

bool amd64_enc_jg(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {
    return enc_jump_common(ec, 0x7F, 0x8F, offset);
}

// The unconditional jumps
// =======================
//
// The jump (here only the near variants are implemented) belongs to a group of
// exceptional instructions, in which the operand size defaults to 64-bit in the
// 64-bit mode. It therefore requires special encoding. Also, due to this, it
// can only be encoded in a limited number of ways in this mode.

bool amd64_enc_jmp_rel(struct AMD64EncContext *ec, struct AMD64RelOffset offset) {

    // The relative offset jump can only be encoded for a 8- and 32-bit offsets,
    // hence we use a special type as its argument.

    assert(ec);

    enc_start(ec);
    if (offset.size == AMD64_ROS_8)
        // An 8-bit offset: 0xEB offset
        ec_put(ec, 0xEB);
    else
        // A 32-bit offset: 0xE9 offset
        ec_put(ec, 0xE9);
    ec_put_rel_offest_le(ec, &offset);
    enc_finish(ec);
    return true;
}

bool amd64_enc_jmp_rm(struct AMD64EncContext *ec, struct AMD64Location x) {

    assert(ec);

    if (!amd64_loc_is_gp_rm(&x)) {
        ec->error_message = "JMP(loc) requires a general purpose r/m location";
        return false;
    }

    if (x.size != AMD64_LS_16_BIT && x.size != AMD64_LS_64_BIT) {
        ec->error_message = "JMP(loc) is only encodable for 16- and 64-bit operands";
        return false;
    }

    // JMP r/m: 0xFF /4
    enc_start(ec);
    enc_prefixes(ec, AMD64_LS_64_BIT, x.size, false, NULL, &x);
    ec_put(ec, 0xFF);
    enc_modrm_1arg(ec, 4, &x);
    enc_finish(ec);
    return true;
}

// Test code
// =========

static int test_jcc(void) {

    struct AMD64RelOffset offset8 = { .size = AMD64_ROS_8, .value8 = 0x01 };
    struct AMD64RelOffset offset32 = { .size = AMD64_ROS_32, .value32 = 0x67452301 };

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_je(&ec, offset8);
    errors += expect_success_2(&ec, &sis, 0x74, 0x01, "JE rel8");

    amd64_enc_jne(&ec, offset32);
    errors += expect_success_6(&ec, &sis, 0x0F, 0x85, 0x01, 0x23, 0x45, 0x67, "JNE rel32");

    amd64_enc_jl(&ec, offset8);
    errors += expect_success_2(&ec, &sis, 0x7C, 0x01, "JL rel8");

    amd64_enc_jnl(&ec, offset32);
    errors += expect_success_6(&ec, &sis, 0x0F, 0x8D, 0x01, 0x23, 0x45, 0x67, "JNL rel32");

    amd64_enc_jng(&ec, offset8);
    errors += expect_success_2(&ec, &sis, 0x7E, 0x01, "JNG rel8");

    amd64_enc_jg(&ec, offset32);
    errors += expect_success_6(&ec, &sis, 0x0F, 0x8F, 0x01, 0x23, 0x45, 0x67, "JG rel32");

    return errors;
}

static int test_jmp_rel(void) {

    struct AMD64RelOffset offset8 = { .size = AMD64_ROS_8, .value8 = 0x01 };
    struct AMD64RelOffset offset32 = { .size = AMD64_ROS_32, .value32 = 0x67452301 };

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_jmp_rel(&ec, offset8);
    errors += expect_success_2(&ec, &sis, 0xEB, 0x01, "JMP rel8");

    amd64_enc_jmp_rel(&ec, offset32);
    errors += expect_success_5(&ec, &sis, 0xE9, 0x01, 0x23, 0x45, 0x67, "JMP rel8");

    return errors;
}

static int test_jmp_rm(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_jmp_rm(&ec, AX);
    errors += expect_success_3(&ec, &sis, 0x66, 0xFF, 0xE0, "JMP [AX]");

    amd64_enc_jmp_rm(&ec, WPTR32_EX(BX, 0x01));
    errors += expect_success_5(
        &ec, &sis,
        0x67, 0x66, 0xFF, 0x63, 0x01,
        "JMP 0x01[EBX]");

    amd64_enc_jmp_rm(&ec, QWSIB32(2, CX, DX, 0));
    errors += expect_success_4(
        &ec, &sis,
        0x67, 0xFF, 0x24, 0x4A,
        "JMP [2*ECX+EDX]");

    amd64_enc_jmp_rm(&ec, QWOFFSET32(0x67452301));
    errors += expect_success_7(
        &ec, &sis,
        0xFF, 0x24, 0x25, 0x01, 0x23, 0x45, 0x67,
        "JMP [offset64]");

    return errors;
}

int test_jump(void) {
    return
        test_jcc() +
        test_jmp_rel() +
        test_jmp_rm();
}

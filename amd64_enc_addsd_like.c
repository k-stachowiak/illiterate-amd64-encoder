/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The common ADDSD like instruction encoding
// ==========================================

static bool enc_addsd_like(struct AMD64EncContext *ec,
                           struct AMD64Location *src,
                           struct AMD64Location *dst,
                           uint8_t opcode) {

    // Assert operand sizes:
    if (src->size != dst->size || src->size != AMD64_LS_64_BIT) {
        ec->error_message = "Both operands of this instruction must be 64-bit locations";
        return false;
    }

    if (amd64_loc_is_sse_rm(src) && dst->type == AMD64_LT_REGISTER_SSE) {

        // ADDSD-like xmm2/mem64 -> xmm1: F2 0F <opcode> /r
        enc_start(ec);
        enc_legacy_prefixes(ec, AMD64_LS_64_BIT, AMD64_LS_64_BIT, src);
        ec_put(ec, 0xF2);
        enc_rex_prefix(ec, AMD64_LS_64_BIT, AMD64_LS_64_BIT, false, dst, src);
        ec_put(ec, 0x0F);
        ec_put(ec, opcode);
        enc_modrm_2arg(ec, modrm_ssereg(dst->ssereg.ssereg), src);
        enc_finish(ec);
        return true;

    } else {
        ec->error_message = "Invalid operands for this instruction";
        return false;

    }

}

// The encoding of specific instructions API
// =========================================

bool amd64_enc_addsd(struct AMD64EncContext *ec,
                     struct AMD64Location src,
                     struct AMD64Location dst) {
    assert(ec);
    return enc_addsd_like(ec, &src, &dst, 0x58);
}

bool amd64_enc_subsd(struct AMD64EncContext *ec,
                     struct AMD64Location src,
                     struct AMD64Location dst) {
    assert(ec);
    return enc_addsd_like(ec, &src, &dst, 0x5C);
}

bool amd64_enc_mulsd(struct AMD64EncContext *ec,
                     struct AMD64Location src,
                     struct AMD64Location dst) {
    assert(ec);
    return enc_addsd_like(ec, &src, &dst, 0x59);
}

bool amd64_enc_divsd(struct AMD64EncContext *ec,
                     struct AMD64Location src,
                     struct AMD64Location dst) {
    assert(ec);
    return enc_addsd_like(ec, &src, &dst, 0x5E);
}

bool amd64_enc_movsd(struct AMD64EncContext *ec,
                     struct AMD64Location src,
                     struct AMD64Location dst) {
    assert(ec);
    if (amd64_loc_is_sse_rm(&src) &&
        dst.type == AMD64_LT_REGISTER_SSE)
        return enc_addsd_like(ec, &src, &dst, 0x10);
    else
        return enc_addsd_like(ec, &dst, &src, 0x11);
}

// Test code
// =========

static int test_addsd_like_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_addsd(&ec, AH, XMM0);
    errors += expect_failure(&ec, "Disallow non-64-bit operands");

    amd64_enc_subsd(&ec, RDI, XMM0);
    errors += expect_failure(&ec, "Disallow non-XMM register");

    amd64_enc_divsd(&ec, QWOFFSET64(0x67452301), QWSIB32(2, DX, 11, 0));
    errors += expect_failure(&ec, "Disallow memory-memory operands");

    return errors;
}

static int test_addsd_like_valid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_addsd(&ec, XMM0, XMM7);
    errors += expect_success_4(
        &ec, &sis,
        0xF2, 0x0F, 0x58, 0xF8,
        "ADDSD XMM0 -> XMM7");

    amd64_enc_addsd(&ec, QWPTR64(AX), XMM0);
    errors += expect_success_4(
        &ec, &sis,
        0xF2, 0x0F, 0x58, 0x00,
        "ADDSD (RAX) -> XMM0");

    amd64_enc_subsd(&ec, QWOFFSET32(0x67452301), XMM0);
    errors += expect_success_9(
        &ec, &sis,
        0xF2, 0x0F, 0x5C, 0x04, 0x25, 0x01, 0x23, 0x45, 0x67,
        "SUBSD (disp32) -> XMM0");

    amd64_enc_movsd(&ec, QWOFFSET32(0x67452301), XMM0);
    errors += expect_success_9(
        &ec, &sis,
        0xF2, 0x0F, 0x10, 0x04, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOVSD (disp32) -> XMM0");

    amd64_enc_movsd(&ec, XMM7, QWSIB32(2, DX, 11, 0));
    errors += expect_success_7(
        &ec, &sis,
        0x67, 0xF2, 0x41, 0x0F, 0x11, 0x3C, 0x53,
        "MOVSD XMM7 -> (2*EDX+R11D)");

    amd64_enc_movsd(&ec, QWSIB32(2, DX, 11, 0), XMM7);
    errors += expect_success_7(
        &ec, &sis,
        0x67, 0xF2, 0x41, 0x0F, 0x10, 0x3C, 0x53,
        "MULSD (2*EDX+R11D) -> XMM7");

    amd64_enc_mulsd(&ec, QWPTR32(8), XMM7);
    errors += expect_success_6(
        &ec, &sis,
        0x67, 0xF2, 0x41, 0x0F, 0x59, 0x38,
        "MULSD (R8D) -> XMM7");

    amd64_enc_divsd(&ec, QWSIB64(8, BX, 15, 0), XMM7);
    errors += expect_success_6(
        &ec, &sis,
        0xF2, 0x41, 0x0F, 0x5E, 0x3C, 0xDF,
        "DIVSD (8*RBX+R15) -> XMM7");

    amd64_enc_divsd(&ec, XMM7, XMM0);
    errors += expect_success_4(
        &ec, &sis,
        0xF2, 0x0F, 0x5E, 0xC7,
        "DIVSD XMM7 -> XMM0");

    return errors;
}

int test_addsd_like(void) {
    return
        test_addsd_like_invalid() +
        test_addsd_like_valid();
}

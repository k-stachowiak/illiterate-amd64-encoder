/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMD64_INTERNAL_H
#define AMD64_INTERNAL_H

#include "amd64_encoder.h"

#include <stdlib.h>
#include <stdio.h>

// Internal API
// ============

// Internal error handling
// -----------------------

#define INTERNAL_ERROR do {                                             \
    fprintf(stderr, "Internal error at %s:%d\n", __FILE__, __LINE__);   \
    exit(1); \
    } while (0)

// Bitshift helper macros
// ----------------------

#define OFFSET32_0(D) (((D) >> 0)  & 0xFF)
#define OFFSET32_1(D) (((D) >> 8)  & 0xFF)
#define OFFSET32_2(D) (((D) >> 16) & 0xFF)
#define OFFSET32_3(D) (((D) >> 24) & 0xFF)

#define DISPL_0(D) (((D).value >> 0)  & 0xFF)
#define DISPL_1(D) (((D).value >> 8)  & 0xFF)
#define DISPL_2(D) (((D).value >> 16) & 0xFF)
#define DISPL_3(D) (((D).value >> 24) & 0xFF)

// Special bytes encoding
// ----------------------

uint8_t modrm_gpreg(enum AMD64GPRegister gpreg);
uint8_t modrm_ssereg(enum AMD64SSERegister ssereg);
uint8_t modrm_ireg(enum AMD64IndirectRegister ireg);
uint8_t modrm_sreg(enum AMD64SIBRegister sreg);

// Byte emission
// -------------

int ec_offset(struct AMD64EncContext *ctx);

void ec_put(struct AMD64EncContext *ctx, uint8_t x);

void ec_put_rel_offest_le(struct AMD64EncContext *ec,
                          struct AMD64RelOffset *offset);

void ec_put_32disp_le(struct AMD64EncContext *ec, int32_t disp);

void ec_put_disp_le(struct AMD64EncContext *ec,
                    struct AMD64Displacement *disp);

void ec_put_immediate_le(struct AMD64EncContext *ec,
                         enum AMD64LocationSize loc_size,
                         struct AMD64ImmediateLocation *imm);

void ec_put_offset_big_le(struct AMD64EncContext *ec, int64_t offset);

void ec_put_rex_prefix(struct AMD64EncContext *ec,
                       bool rex_w,
                       bool rex_r,
                       bool rex_x,
                       bool rex_b);

void ec_maybe_put_rex_prefix(struct AMD64EncContext *ec,
                             bool rex_w,
                             bool rex_r,
                             bool rex_x,
                             bool rex_b);

// Encoding of common patterns
// ---------------------------

bool rex_prefix_required(struct AMD64Location *loc);

void enc_legacy_prefixes(struct AMD64EncContext *ec,
                         enum AMD64LocationSize default_operand_size,
                         enum AMD64LocationSize operand_size,
                         struct AMD64Location *rm_location);

void enc_rex_prefix(struct AMD64EncContext *ec,
                    enum AMD64LocationSize default_operand_size,
                    enum AMD64LocationSize operand_size,
                    bool reg_opcode,
                    struct AMD64Location *reg_loc,
                    struct AMD64Location *rm_loc);

void enc_prefixes(struct AMD64EncContext *ec,
                  enum AMD64LocationSize default_operand_size,
                  enum AMD64LocationSize operand_size,
                  bool reg_opcode,
                  struct AMD64Location *reg_loc,
                  struct AMD64Location *rm_loc);

void enc_sized_opcode(struct AMD64EncContext *ec,
                      enum AMD64LocationSize size,
                      uint8_t opcode_8bit,
                      uint8_t opcode_default);

void enc_incr_sized_opcode(struct AMD64EncContext *ec,
                           enum AMD64LocationSize size,
                           uint8_t opcode_8bit,
                           uint8_t opcode_default,
                           enum AMD64GPRegister incr_gpreg);

void enc_modrm_1arg(struct AMD64EncContext *ec,
                    uint8_t reg_code,
                    struct AMD64Location *rm_loc);

void enc_modrm_2arg(struct AMD64EncContext *ec,
                    uint8_t reg_code,
                    struct AMD64Location *rm_loc);

void enc_start(struct AMD64EncContext *ec);
void enc_finish(struct AMD64EncContext *ec);

// Tests
// =====

// Common test utilities
// ---------------------

int expect_failure(struct AMD64EncContext *ec, char *fail_msg);

int expect_success(struct AMD64EncContext *ec,
                   struct AMD64SingleInstrSink *sis,
                   uint8_t *expected,
                   int expected_len,
                   char *fail_msg);

int expect_success_2(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     char *fail_msg);

int expect_success_3(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     char *fail_msg);

int expect_success_4(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     char *fail_msg);

int expect_success_5(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     char *fail_msg);

int expect_success_6(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     char *fail_msg);

int expect_success_7(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     uint8_t expected_7,
                     char *fail_msg);

int expect_success_8(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     uint8_t expected_7,
                     uint8_t expected_8,
                     char *fail_msg);

int expect_success_9(struct AMD64EncContext *ec,
                     struct AMD64SingleInstrSink *sis,
                     uint8_t expected_1,
                     uint8_t expected_2,
                     uint8_t expected_3,
                     uint8_t expected_4,
                     uint8_t expected_5,
                     uint8_t expected_6,
                     uint8_t expected_7,
                     uint8_t expected_8,
                     uint8_t expected_9,
                     char *fail_msg);

int expect_success_10(struct AMD64EncContext *ec,
                      struct AMD64SingleInstrSink *sis,
                      uint8_t expected_1,
                      uint8_t expected_2,
                      uint8_t expected_3,
                      uint8_t expected_4,
                      uint8_t expected_5,
                      uint8_t expected_6,
                      uint8_t expected_7,
                      uint8_t expected_8,
                      uint8_t expected_9,
                      uint8_t expected_10,
                      char *fail_msg);

int expect_success_11(struct AMD64EncContext *ec,
                      struct AMD64SingleInstrSink *sis,
                      uint8_t expected_1,
                      uint8_t expected_2,
                      uint8_t expected_3,
                      uint8_t expected_4,
                      uint8_t expected_5,
                      uint8_t expected_6,
                      uint8_t expected_7,
                      uint8_t expected_8,
                      uint8_t expected_9,
                      uint8_t expected_10,
                      uint8_t expected_11,
                      char *fail_msg);

// Entry points for particular module tests
// ----------------------------------------

int test_add_like(void);
int test_addsd_like(void);
int test_comisd(void);
int test_idiv(void);
int test_imul(void);
int test_inc(void);
int test_jump(void);
int test_lea(void);
int test_mov(void);
int test_movd(void);
int test_movzx(void);
int test_neg(void);
int test_push(void);
int test_pop(void);

#endif

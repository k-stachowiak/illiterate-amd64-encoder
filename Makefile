# Copyright (C) 2020 Krzysztof Stachowiak
#
# This file is part of the Illiterate Assembler.
#
# Illiterate Assembler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Illiterate Assembler compiler is distributed in the hope that it
# will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the program.  If not, see <http://www.gnu.org/licenses/>.

# Flags for different configurations
# ==================================

CFLAGS = -Wall -Wextra -MD -MP
LDFLAGS = -lm

RELEASE-FLAGS = -O2
DEBUG-FLAGS = -O0 -g3

COV-FLAGS = -fprofile-arcs -ftest-coverage

SAN-FLAGS = -fsanitize=address -fsanitize=leak -fsanitize=undefined -static-libasan

# Object definitions
# ==================

LIB-SOURCES = $(wildcard amd64*.c)
LIB-OBJECTS = $(LIB-SOURCES:%.c=%.o)
LIB-DEPENDENCIES = $(LIB-SOURCES:%.c=%.d)

LIB-DEBUG-OBJECTS = $(LIB-SOURCES:%.c=%_d.o)
LIB-COV-OBJECTS = $(LIB-SOURCES:%.c=%_c.o)
LIB-SAN-OBJECTS = $(LIB-SOURCES:%.c=%_s.o)
LIB-SAN-DEBUG-OBJECTS = $(LIB-SOURCES:%.c=%_sd.o)

# Target definitions
# ==================

# Common
# ------

default: test libilasm.a
all: test       test_d       test_c       test_s       test_sd \
     libilasm.a libilasm_d.a libilasm_c.a libilasm_s.a libilasm_sd.a

# Release
# -------

test: test.o $(LIB-OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

libilasm.a: $(LIB-OBJECTS)
	$(AR) rcs $@ $^

# Debug
# -----

test_d: test_d.o $(LIB-DEBUG-OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

libilasm_d.a: $(LIB-DEBUG-OBJECTS)
	$(AR) rcs $@ $^

# Coverage
# --------

test_c: test_c.o $(LIB-COV-OBJECTS)
	$(CC) $(LDFLAGS) $(COV-FLAGS) -o $@ $^

libilasm_c.a: $(LIB-COV-OBJECTS)
	$(AR) rcs $@ $^

# Sanitizer
# ---------

test_s: test_s.o $(LIB-SAN-OBJECTS)
	$(CC) $(LDFLAGS) $(SAN-FLAGS) -o $@ $^

libilasm_s.a: $(LIB-SAN-OBJECTS)
	$(AR) rcs $@ $^

# Debug/sanitizer
# ---------------

test_sd: test_sd.o $(LIB-SAN-DEBUG-OBJECTS)
	$(CC) $(LDFLAGS) $(SAN-FLAGS) -o $@ $^

libilasm_sd.a: $(LIB-SAN-DEBUG-OBJECTS)
	$(AR) rcs $@ $^

# Compiler recepies
# =================

%.o: %.c
	$(CC) $(CFLAGS) $(RELEASE-FLAGS) -c $< -o $@

%_d.o: %.c
	$(CC) $(CFLAGS) $(DEBUG-FLAGS) -c $< -o $@

%_c.o: %.c
	$(CC) $(CFLAGS) $(DEBUG-FLAGS) $(COV-FLAGS) -c $< -o $@

%_s.o: %.c
	$(CC) $(CFLAGS) $(RELEASE-FLAGS) $(SAN-FLAGS) -c $< -o $@

%_sd.o: %.c
	$(CC) $(CFLAGS) $(DEBUG-FLAGS) $(SAN-FLAGS) -c $< -o $@

# Coverage report
# ---------------

coverage.html: test_c
	./test_c
	gcovr -r . --html --html-details -o coverage.html

# Phony targets
# =============

.PHONY: clean

clean:
	rm -f *.o *.d *.a *.gcda *.gcno *.html test test_sd test_s test_c test_d

-include $(LIB-DEPENDENCIES)

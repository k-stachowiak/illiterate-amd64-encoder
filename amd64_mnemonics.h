/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AMD64_MNEMONICS_H
#define AMD64_MNEMONICS_H

#include "amd64_encoder.h"

// Immediates
// ==========

#define IMM8(___X___) amd64_loc_make_immediate(AMD64_LS_8_BIT, ___X___)
#define IMM16(___X___) amd64_loc_make_immediate(AMD64_LS_16_BIT, ___X___)
#define IMM32(___X___) amd64_loc_make_immediate(AMD64_LS_32_BIT, ___X___)
#define IMM64(___X___) amd64_loc_make_immediate(AMD64_LS_64_BIT, ___X___)

// General purpose registers
// =========================

#define AH amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_AH)
#define BH amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_BH)
#define CH amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_CH)
#define DH amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_DH)

#define AL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_AX)
#define BL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_BX)
#define CL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_CX)
#define DL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_DX)
#define SPL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_SP)
#define BPL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_BP)
#define SIL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_SI)
#define DIL amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_DI)
#define R8B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_8)
#define R9B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_9)
#define R10B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_10)
#define R11B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_11)
#define R12B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_12)
#define R13B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_13)
#define R14B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_14)
#define R15B amd64_loc_make_register_gp(AMD64_LS_8_BIT, AMD64_GPREG_15)

#define AX amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_AX)
#define BX amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_BX)
#define CX amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_CX)
#define DX amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_DX)
#define SP amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_SP)
#define BP amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_BP)
#define SI amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_SI)
#define DI amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_DI)
#define R8W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_8)
#define R9W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_9)
#define R10W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_10)
#define R11W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_11)
#define R12W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_12)
#define R13W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_13)
#define R14W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_14)
#define R15W amd64_loc_make_register_gp(AMD64_LS_16_BIT, AMD64_GPREG_15)

#define EAX amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_AX)
#define EBX amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_BX)
#define ECX amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_CX)
#define EDX amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_DX)
#define ESP amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_SP)
#define EBP amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_BP)
#define ESI amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_SI)
#define EDI amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_DI)
#define R8D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_8)
#define R9D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_9)
#define R10D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_10)
#define R11D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_11)
#define R12D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_12)
#define R13D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_13)
#define R14D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_14)
#define R15D amd64_loc_make_register_gp(AMD64_LS_32_BIT, AMD64_GPREG_15)

#define RAX amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_AX)
#define RBX amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_BX)
#define RCX amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_CX)
#define RDX amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_DX)
#define RSP amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_SP)
#define RBP amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_BP)
#define RSI amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_SI)
#define RDI amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_DI)
#define R8 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_8)
#define R9 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_9)
#define R10 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_10)
#define R11 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_11)
#define R12 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_12)
#define R13 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_13)
#define R14 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_14)
#define R15 amd64_loc_make_register_gp(AMD64_LS_64_BIT, AMD64_GPREG_15)

// XMM registers
// =============

#define XMM0 amd64_loc_make_register_sse(AMD64_SSEREG_XMM0)
#define XMM1 amd64_loc_make_register_sse(AMD64_SSEREG_XMM1)
#define XMM2 amd64_loc_make_register_sse(AMD64_SSEREG_XMM2)
#define XMM3 amd64_loc_make_register_sse(AMD64_SSEREG_XMM3)
#define XMM4 amd64_loc_make_register_sse(AMD64_SSEREG_XMM4)
#define XMM5 amd64_loc_make_register_sse(AMD64_SSEREG_XMM5)
#define XMM6 amd64_loc_make_register_sse(AMD64_SSEREG_XMM6)
#define XMM7 amd64_loc_make_register_sse(AMD64_SSEREG_XMM7)

// Indirect register locations
// ===========================

#define BPTR32(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_8_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(0))
#define WPTR32(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_16_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(0))
#define DWPTR32(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_32_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(0))
#define QWPTR32(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_64_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(0))

#define BPTR64(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_8_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(0))
#define WPTR64(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_16_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(0))
#define DWPTR64(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_32_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(0))
#define QWPTR64(___R___) amd64_loc_make_register_indirect(\
        AMD64_LS_64_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(0))

#define BPTR32_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_8_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(___X___))
#define WPTR32_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_16_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(___X___))
#define DWPTR32_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_32_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(___X___))
#define QWPTR32_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_64_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_32,\
        amd64_displacement_make(___X___))

#define BPTR64_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_8_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(___X___))
#define WPTR64_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_16_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(___X___))
#define DWPTR64_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_32_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(___X___))
#define QWPTR64_EX(___R___, ___X___) amd64_loc_make_register_indirect(\
        AMD64_LS_64_BIT, AMD64_IREG_ ## ___R___, AMD64_IAS_64,\
        amd64_displacement_make(___X___))

// Memory offset
// =============

#define BOFFSET32(___X___) amd64_loc_make_memory_offset_small(\
        AMD64_LS_8_BIT, ___X___)
#define WOFFSET32(___X___) amd64_loc_make_memory_offset_small(\
        AMD64_LS_16_BIT, ___X___)
#define DWOFFSET32(___X___) amd64_loc_make_memory_offset_small(\
        AMD64_LS_32_BIT, ___X___)
#define QWOFFSET32(___X___) amd64_loc_make_memory_offset_small(\
        AMD64_LS_64_BIT, ___X___)

#define BOFFSET64(___X___) amd64_loc_make_memory_offset_big(\
        AMD64_LS_8_BIT, ___X___)
#define WOFFSET64(___X___) amd64_loc_make_memory_offset_big(\
        AMD64_LS_16_BIT, ___X___)
#define DWOFFSET64(___X___) amd64_loc_make_memory_offset_big(\
        AMD64_LS_32_BIT, ___X___)
#define QWOFFSET64(___X___) amd64_loc_make_memory_offset_big(\
        AMD64_LS_64_BIT, ___X___)

// RIP offset
// ==========

#define BRIPOFF(___X___) amd64_loc_make_memory_rip_offset(AMD64_LS_8_BIT, ___X___)
#define WRIPOFF(___X___) amd64_loc_make_memory_rip_offset(AMD64_LS_16_BIT, ___X___)
#define DWRIPOFF(___X___) amd64_loc_make_memory_rip_offset(AMD64_LS_32_BIT, ___X___)
#define QWRIPOFF(___X___) amd64_loc_make_memory_rip_offset(AMD64_LS_64_BIT, ___X___)

// SIB addressed
// =============

#define BSIB32(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_8_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_32, amd64_displacement_make(___X___))

#define WSIB32(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_16_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_32, amd64_displacement_make(___X___))

#define DWSIB32(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_32_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_32, amd64_displacement_make(___X___))

#define QWSIB32(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_64_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_32, amd64_displacement_make(___X___))

#define BSIB64(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_8_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_64, amd64_displacement_make(___X___))

#define WSIB64(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_16_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_64, amd64_displacement_make(___X___))

#define DWSIB64(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_32_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_64, amd64_displacement_make(___X___))

#define QWSIB64(___S___, ___I___, ___B___, ___X___) amd64_loc_make_memory_sib(\
    AMD64_LS_64_BIT, AMD64_SS_ ## ___S___, AMD64_SREG_ ## ___I___, AMD64_SREG_ ## ___B___,\
    AMD64_IAS_64, amd64_displacement_make(___X___))

#endif

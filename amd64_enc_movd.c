/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

// The MOVD instruction encoding
// =============================

bool amd64_enc_movd(struct AMD64EncContext *ec,
                    struct AMD64Location src,
                    struct AMD64Location dst) {

    struct AMD64Location *reg_loc, *rm_loc;
    uint8_t opcode;

    // We can perform MOVD in two ways:
    if (amd64_loc_is_gp_rm(&src) && dst.type == AMD64_LT_REGISTER_SSE) {
        // a) from a general purpose reg/mem location to an SSE register:
        rm_loc = &src;
        reg_loc = &dst;
        opcode = 0x6E;

    } else if (amd64_loc_is_gp_rm(&dst) && src.type == AMD64_LT_REGISTER_SSE) {
        // b) from an SSE register to a general purpose reg/mem location:
        rm_loc = &dst;
        reg_loc = &src;
        opcode = 0x7E;

    } else {
        ec->error_message = "Invalid operands for MOVD";
        return false;
    }

    // Now that we have recognized the reg and the r/m arguments, let's
    // validate the r/m operand's size:
    if (rm_loc->size != AMD64_LS_32_BIT && rm_loc->size != AMD64_LS_64_BIT) {
        ec->error_message = "Invalid r/m operand size for MOVD";
        return false;
    }

    enc_start(ec);
    enc_legacy_prefixes(ec, AMD64_LS_32_BIT, rm_loc->size, rm_loc);
    ec_put(ec, 0x66);
    enc_rex_prefix(ec, AMD64_LS_32_BIT, rm_loc->size, false, NULL, rm_loc);
    ec_put(ec, 0x0F);
    ec_put(ec, opcode);
    enc_modrm_2arg(ec, modrm_ssereg(reg_loc->ssereg.ssereg), rm_loc);
    enc_finish(ec);
    return true;
}

// Test code
// =========

static int test_movd_invalid(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_movd(&ec, AH, XMM0);
    errors += expect_failure(&ec, "Disallow 8-bit argument for MOVD");

    amd64_enc_movd(&ec, BL, XMM0);
    errors += expect_failure(&ec, "Disallow 8-bit argument for MOVD");

    amd64_enc_movd(&ec, CX, XMM0);
    errors += expect_failure(&ec, "Disallow 16-bit argument for MOVD");

    amd64_enc_movd(&ec, XMM7, XMM0);
    errors += expect_failure(&ec, "Disallow two SSE operands for MOVD");

    amd64_enc_movd(&ec, RSP, QWPTR64(DX));
    errors += expect_failure(&ec, "Disallow two non-SSE operands for MOVD");

    amd64_enc_movd(&ec, QWPTR32(AX), QWPTR64(DX));
    errors += expect_failure(&ec, "Disallow mem -> mem MOVD");

    return errors;
}

static int test_movd_reg_reg(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_movd(&ec, ESP, XMM7);
    errors += expect_success_4(&ec, &sis, 0x66, 0x0F, 0x6E, 0xFC, "MOVD ESP -> XMM7");

    amd64_enc_movd(&ec, XMM7, ESP);
    errors += expect_success_4(&ec, &sis, 0x66, 0x0F, 0x7E, 0xFC, "MOVD XMM7 -> ESP");

    amd64_enc_movd(&ec, RAX, XMM0);
    errors += expect_success_5(&ec, &sis, 0x66, 0x48, 0x0F, 0x6E, 0xC0, "MOVD RAX -> XMM0");

    amd64_enc_movd(&ec, XMM0, RAX);
    errors += expect_success_5(&ec, &sis, 0x66, 0x48, 0x0F, 0x7E, 0xC0, "MOVD XMM0 -> RAX");

    return errors;
}

static int test_movd_reg_rm(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    // XMM <-> indirect register
    amd64_enc_movd(&ec, QWPTR32(AX), XMM0);
    errors += expect_success_6(
        &ec, &sis,
        0x67, 0x66, 0x48, 0x0F, 0x6E, 0x00,
        "MOVD (EAX) -> XMM0");

    amd64_enc_movd(&ec, XMM0, QWPTR32(AX));
    errors += expect_success_6(
        &ec, &sis,
        0x67, 0x66, 0x48, 0x0F, 0x7E, 0x00,
        "MOVD XMM0 -> (EAX)");

    // XMM <-> indirect register + displacement
    amd64_enc_movd(&ec, DWPTR64_EX(BX, 0x01), XMM1);
    errors += expect_success_5(
        &ec, &sis,
        0x66, 0x0F, 0x6E, 0x4B, 0x01,
        "MOVD disp8(RBX) -> XMM1");

    amd64_enc_movd(&ec, XMM1, DWPTR64_EX(BX, 0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x66, 0x0F, 0x7E, 0x8b, 0x01, 0x23, 0x45, 0x67,
        "MOVD XMM1 -> disp32(REBX)");

    // XMM <-> displacement
    amd64_enc_movd(&ec, DWOFFSET32(0x01), XMM2);
    errors += expect_success_9(
        &ec, &sis,
        0x66, 0x0F, 0x6E, 0x14, 0x25, 0x01, 0x00, 0x00, 0x00,
        "MOVD disp8(RBX) -> XMM2");

    amd64_enc_movd(&ec, XMM2, DWOFFSET32(0x67452301));
    errors += expect_success_9(
        &ec, &sis,
        0x66, 0x0F, 0x7E, 0x14, 0x25, 0x01, 0x23, 0x45, 0x67,
        "MOVD XMM2 -> disp32(RBX)");

    // XMM <-> RIP offset
    amd64_enc_movd(&ec, QWRIPOFF(0x01), XMM3);
    errors += expect_success_9(
        &ec, &sis,
        0x66, 0x48, 0x0F, 0x6E, 0x1D, 0x01, 0x00, 0x00, 0x00,
        "MOVD disp8(RIP) -> XMM3");

    amd64_enc_movd(&ec, XMM3, QWRIPOFF(0x67452301));
    errors += expect_success_9(
        &ec, &sis,
        0x66, 0x48, 0x0F, 0x7E, 0x1D, 0x01, 0x23, 0x45, 0x67,
        "MOVD XMM3 -> disp32(RIP)");

    // XMM <-> RIP offset
    amd64_enc_movd(&ec, DWSIB64(4, CX, DX, 0), XMM4);
    errors += expect_success_5(
        &ec, &sis,
        0x66, 0x0F, 0x6E, 0x24, 0x8A,
        "MOVD (4*RCX+RDX) -> XMM4");

    amd64_enc_movd(&ec, XMM4, DWSIB64(4, CX, DX, 0x01));
    errors += expect_success_6(
        &ec, &sis,
        0x66, 0x0F, 0x7E, 0x64, 0x8A, 0x01,
        "MOVD XMM4 -> disp8(4*RCX+RDX)");

    return errors;
}

int test_movd(void) {
    return
        test_movd_invalid() +
        test_movd_reg_reg() +
        test_movd_reg_rm();
}

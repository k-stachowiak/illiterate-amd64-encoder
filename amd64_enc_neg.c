/*
 * Copyright (C) 2020 Krzysztof Stachowiak
 *
 * This file is part of the Illiterate Assembler.
 *
 * Illiterate Assembler is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Illiterate Assembler compiler is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "amd64_encoder.h"
#include "amd64_internal.h"
#include "amd64_mnemonics.h"

#include <assert.h>

// The NEG instruction
// ===================

bool amd64_enc_neg(struct AMD64EncContext *ec, struct AMD64Location x) {

    assert(ec);

    if (!amd64_loc_is_gp_rm(&x)) {
        ec->error_message = "Can only perform NEG on a general purpose r/m location";
        return false;
    }

    enc_start(ec);
    enc_prefixes(ec, AMD64_LS_32_BIT, x.size, false, NULL, &x);
    enc_sized_opcode(ec, x.size, 0xF6, 0xF7);
    enc_modrm_1arg(ec, 3, &x);
    enc_finish(ec);
    return true;
}

// Test code
// =========

int test_neg(void) {

    struct AMD64SingleInstrSink sis;
    struct AMD64EncContext ec;
    int errors = 0;

    amd64_sis_init(&sis);
    amd64_ec_init_single_instr(&ec, &sis);

    amd64_enc_neg(&ec, AH);
    errors += expect_success_2(&ec, &sis, 0xF6, 0xDC, "NEG AH");

    amd64_enc_neg(&ec, BL);
    errors += expect_success_2(&ec, &sis, 0xF6, 0xDB, "NEG BL");

    amd64_enc_neg(&ec, SIL);
    errors += expect_success_3(&ec, &sis, 0x40, 0xF6, 0xDE, "NEG SIL");

    amd64_enc_neg(&ec, WPTR32_EX(CX, 0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x67, 0x66, 0xF7, 0x99, 0x01, 0x23, 0x45, 0x67,
        "NEG disp32[EBX]");

    amd64_enc_neg(&ec, DWPTR64_EX(DX, 0x01));
    errors += expect_success_3(&ec, &sis, 0xF7, 0x5A, 0x01, "NEG disp8[RDX]");

    amd64_enc_neg(&ec, QWOFFSET32(0x67452301));
    errors += expect_success_8(
        &ec, &sis,
        0x48, 0xF7, 0x1C, 0x25, 0x01, 0x23, 0x45, 0x67,
        "NEG [offset32]");

    return errors;
}
